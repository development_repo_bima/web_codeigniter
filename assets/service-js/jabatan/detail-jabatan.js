var main_url = site_url + 'manages/jabatan/';
$(document).ready(function(){
	get_detail(id);
	
	$('#edit-button').click(function(){
		$('#detail-view').fadeOut();
		$('#edit-view').fadeIn();
		
	})
	
	$('#save-edit').click(function(){
		update();
		return false;
	}) 
	
});
function get_detail(id){
	if(!id){
		alert('gagal');
	}
	$.ajax({
		url : main_url + 'getDetail',
		data : {'id' : id},
		dataType : 'json',
		type : 'get',
		success : function(result){
			console.log(result.data);
			$('#nama-jabatan').html(result.data.nama_jabatan);
			$('#ket-jabatan').html(result.data.deskripsi);

			$('#jabform-nama').val(result.data.nama_jabatan);
			$('#jabform-ket').val(result.data.deskripsi);
			
		},
		error : function(x,h,r){
			console.log(r);
		}
	})
}
function update(){
	$.ajax({
		url : main_url + 'updateData',dataType : 'json',type : 'patch',
		data : {'id':id,'jabatan':$('#jabform-nama').val(),'keterangan':$('#jabform-ket').val()},
		success : function(result){
			if(result.status){
				//alert(result.msg);
				swal({
					title :"Berhasil",
					text : result.msg,
					type : "success",
					showCancelButton : false,
					closeOnConfirm : false,
					showLoaderOnConfirm : true,
				}, function(){
					setTimeout( function(){
						cancel();
					}, 2000)
				});
			}else{
				swal("Gagal",result.msg,"error");
			}
		}
	})
}
function cancel(){
	location.replace(main_url);
}