var main_url = site_url + 'manages/jabatan/';
$(document).ready(function(){
	
	//event handler for disabled add
	$('.add-new input').keyup(function(){
		disableAddForm();
	})
	
	//when add data submitted
	$('#submitted').click(function(){
		save_data();
		return false;
	})
	
});
//function for disabled add form
function disableAddForm(){
	var empty =true;
	$('.add-new input').each(function(){
		if($(this).val().length!==0)
			empty = false;
	})
	if(empty==false){
		$('#submitted').attr('disabled', false);
	}else{
		$('#submitted').attr('disabled', true);
	}
}
//ajax
function save_data(){
	
	var url = main_url + 'addData'
	
	$.ajax({
		
		url : url,
		dataType : 'json',
		type : 'post',
		data : $('#form-jabatan').serialize(),
		success: function(result){
			if(result.status){
				swal({
					title :"Berhasil",
					text : result.msg,
					type : "success",
					showCancelButton : false,
					closeOnConfirm : false,
					showLoaderOnConfirm : true,
				}, function(){
					setTimeout( function(){
						window.location.replace(main_url);
					}, 2000)
				});
			}else{
				//alert(result.msg);
				swal("Gagal", result.msg, "error");
			}
		},
		error: function(x,h,r){
			console.log(r);
		}
	})
}