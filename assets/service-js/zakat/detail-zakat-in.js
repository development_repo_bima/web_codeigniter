
$(document).ready(function(){
	get_detail(id);

	(role == 3)?$('#edit-button').hide():$('#edit-button').show();
	
	$('#edit-button').click(function(){
		$('#detail-view').fadeOut();
		$('#edit-view').fadeIn();
		$('#header-tuts').html('Edit Info Pemberi Zakat');
		get_jabatan();
	})
	
	$('#save-edit').click(function(){
		update();
		return false;
	}) 
	
});
function get_detail(id){
	if(!id){
		//alert('gagal');
		swal("Error","Disallowed Action","error");
		notFound();
	}
	$.ajax({
		url : site_url + 'zakat/penerimaan/getDetail',
		data : {'id' : id},
		dataType : 'json',
		type : 'get',
		success : function(result){

			(result.data.metode_bayar == 2)?$('#bank').hide():$('#bank').show();
			$('#bb').attr({'href':site_url+'assets/files/'+result.data.bukti_bayar,'target':'_blank'});
			//for detail val
			/*user info*/
			$('#nama-donatur').html(result.data.nama_donatur);
			$('#notelp').html(result.data.no_telp);
			$('#emailuser').html(result.data.email);
			$('#alamat-donatur').html(result.data.alamat);

			/*transaction info*/
			$('#metode-bayar').html(result.data.metode);
			$('#jenis').html(result.data.jenis);
			$('#data-bank').html('Bank '+result.data.nama_bank+' : '+result.data.no_rek+' '+result.data.atas_nama);
			$('#jml-dana').html(currency_replace(result.data.jumlah_dana));
			$('#tgl-transaksi').html(result.data.created_at);

			//for edit val
			$('#namadonatur-edit').val(result.data.nama_donatur);
			$('#notelp-edit').val(result.data.no_telp);
			$('#email-edit').val(result.data.email);
			$('#alamat-donatur-edit').html(result.data.alamat);
		},
		error : function(x,h,r){
			console.log(r);
		}
	})
}
function update(){
	$.ajax({
		url : site_url + 'zakat/penerimaan/updateData',dataType : 'json',type : 'patch',
		data : $('#form-edit').serialize()+'&id='+id+'&tipe_user='+$('#select-form').val(),
		success : function(result){
			if(result.status){
				//alert(result.msg);
				swal({
					title :"Berhasil",
					text : result.msg,
					type : "success",
					showCancelButton : false,
					closeOnConfirm : false,
					showLoaderOnConfirm : true,
				}, function(){
					setTimeout( function(){
						cancel();
					}, 2000)
				});
			}
		}
	})
}
function cancel(){
	location.replace(site_url+'zakat/penerimaan');
}
function disabledform(){
	if($('#change-pass').is(':checked')){
		$('#password-edit').attr('disabled', false);
	}else{
		$('#password-edit').attr('disabled', true);
	}
}
function disabledselect(){
	if($('#change-role').is(':checked')){
		$('#select-form').attr('disabled', false);
	}else{
		$('#select-form').attr('disabled', true);
	}
}
function get_jabatan(){
	//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
	$.ajax({
		url : site_url + 'user/getJabatan',dataType:'json',type:'get',
		success:function(result){
			$('#fuchs').html(result.data);
			$('#select-form').attr('disabled', true);
		}
	})
}
function notFound(){
	location.replace(site_url+'testing/notFound');
}

function currency_replace(int){
	var foreign_number = int.toLocaleString();
	var idn_number = foreign_number.replace(',','.');

	return 'Rp.'+idn_number;
}