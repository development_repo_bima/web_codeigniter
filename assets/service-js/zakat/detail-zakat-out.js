
$(document).ready(function(){
	get_detail(id);

	(role == 3)?$('#edit-button').hide():$('#edit-button').show();
	
	$('#edit-button').click(function(){
		$('#detail-view').fadeOut();
		$('#edit-view').fadeIn();
		$('#header-tuts').html('Ubah Data Penyaluran Zakat');
		get_jabatan();
	})
	
	$('#save-edit').click(function(){
		update();
		return false;
	}) 
	
});
function get_detail(id){
	if(!id){
		//alert('gagal');
		swal("Error","Disallowed Action","error");
		notFound();
	}
	$.ajax({
		url : site_url + 'zakat/pengeluaran/getDetail',
		data : {'id' : id},
		dataType : 'json',
		type : 'get',
		success : function(result){
			//console.log(result.data);
			//$('.data-detail tbody').html(result.data);
			//for detail val
			$('#jenis-penerima').html(result.data.nama_pilihan);
			$('#nama-penerima').html(result.data.nama_penerima);
			$('#notelp').html(result.data.no_telp);
			$('#emailuser').html(result.data.email);
			$('#alamatPenerima').html(result.data.alamat_penerima);

			$('#tipe').html(result.data.nama_pilihan);
			$('#jml-dana').html(currency_replace(result.data.jumlah_diterima));
			$('#tgl-transaksi').html(result.data.created_at);
			//for edit val
			$('#nama-penerima-edit').val(result.data.nama_penerima);
			$('#email-edit').val(result.data.email);
			$('#notelp-edit').val(result.data.no_telp);
			$('#alamat-penerima-edit').val(result.data.alamat_penerima);
		},
		error : function(x,h,r){
			console.log(r);
		}
	})
}
function update(){
	$.ajax({
		url : site_url + 'zakat/pengeluaran/updateData',dataType : 'json',type : 'patch',
		data : $('#form-edit').serialize()+'&id='+id+'&tipe_user='+$('#select-form').val(),
		success : function(result){
			if(result.status){
				//alert(result.msg);
				swal({
					title :"Berhasil",
					text : result.msg,
					type : "success",
					showCancelButton : false,
					closeOnConfirm : false,
					showLoaderOnConfirm : true,
				}, function(){
					setTimeout( function(){
						cancel();
					}, 2000)
				});
			}
		}
	})
}
function cancel(){
	location.replace(site_url+'zakat/pengeluaran');
}
function disabledform(){
	if($('#change-pass').is(':checked')){
		$('#password-edit').attr('disabled', false);
	}else{
		$('#password-edit').attr('disabled', true);
	}
}
function disabledselect(){
	if($('#change-role').is(':checked')){
		$('#select-form').attr('disabled', false);
	}else{
		$('#select-form').attr('disabled', true);
	}
}
function get_jabatan(){
	//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
	$.ajax({
		url : site_url + 'user/getJabatan',dataType:'json',type:'get',
		success:function(result){
			$('#fuchs').html(result.data);
			$('#select-form').attr('disabled', true);
		}
	})
}
function notFound(){
	location.replace(site_url+'testing/notFound');
}
function currency_replace(int){
	var foreign_number = int.toLocaleString();
	var idn_number = foreign_number.replace(',','.');

	return 'Rp.'+idn_number;
}