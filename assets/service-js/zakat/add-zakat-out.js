$(document).ready(function(){
	get_pilihan();

	//event handler for disabled add
	$('.add-new input').keyup(function(){
		disableAddForm();
	})
	
	//when clicked for password check submitted
	$(document).on('click','#check-pass',function(){
		var empty = $('#chk-form').find("input").filter(function(){
			return this.value === "";
		});
		if(empty.length){
			swal("Info","Periksa Isian Anda", "info");
		}else{
			check_data();
		}
		return false;
	})
	
	//when add data submitted
	$('#do-upload').click(function(){
		var empty = $('#add-form').find("input").filter(function(){
			return this.value === "";
		});
		if(empty.length){
			swal("Info","Periksa Isian Anda", "info");
		}else{
			save_data();
		}
		return false;
	})
	
});
//function for disabled cek form
function disableCekForm(){
	var empty =true;
	$('.checks input').each(function(){
		if($(this).val().length!==0)
			empty = false;
	})
	if(empty==false){
		$('#check-pass').attr('disabled', false);
	}else{
		$('#check-pass').attr('disabled', true);
	}
}
//function for disabled add form
function disableAddForm(){
	var empty =true;
	$('.add-new input').each(function(){
		if($(this).val().length!==0)
			empty = false;
	})
	if(empty==false){
		$('#do-upload').attr('disabled', false);
	}else{
		$('#do-upload').attr('disabled', true);
	}
}
//ajax
function save_data(){
	
	var url = site_url + 'zakat/pengeluaran/addData'
	
	$.ajax({
		
		url : url,
		dataType : 'json',
		type : 'post',
		data : $('#form-zakat-out').serialize()+'&jenis_penerima='+$('#select-form').val(),
		success: function(result){
			if(result.status){
				//alert(result.msg);
				swal({
						title :"Berhasil",
						text : result.msg,
						type : "success",
						showCancelButton : false,
						closeOnConfirm : false,
						showLoaderOnConfirm : true,
					}, function(){
						setTimeout( function(){
							window.location.replace(site_url + 'zakat/pengeluaran');
						}, 2000)
					});
			}else{
				swal("Gagal",result.msg,"error");
				//alert(result.msg);
			}
		},
		error: function(x,h,r){
			console.log(r);
		}
	})
}

function jenis_pilihan(){
	
	if($('#select-form').val()==='6'){
		$('#penerima-span').fadeIn(); 
		$('#label-nama').html('Nama Yayasan Penerima');
	}else if($('#select-form').val()==='7'){
		$('#penerima-span').fadeIn();
		$('#label-nama').html('Nama Penerima');
	}else{
		$('#penerima-span').fadeOut();
	}
}

function get_pilihan(){
		//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
		$.ajax({
			url : site_url + 'zakat/pengeluaran/getPilihan',dataType:'json',type:'get',
			success:function(result){
				$('#bantuan-id').html(result.data);
			}
		})
	}