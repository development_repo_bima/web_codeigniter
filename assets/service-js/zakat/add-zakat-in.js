$(document).ready(function(){
	get_pilihan();
	
	//event handler for disabled add
	$('.add-new input').keyup(function(){
		disableAddForm();
	})

	
	var url = site_url + 'zakat/penerimaan/addData'

	$('#form-zakat-in').ajaxForm({
		
		url : url,
		//delegation : true,
		type : 'post',
		data : $('#form-zakat-in').serialize(),
		dataType : 'json',
		resetForm : true,
		success: function(result){
			if(result.status){
				//alert(result.msg);
				swal({
						title :"Berhasil",
						text : result.msg,
						type : "success",
						showCancelButton : false,
						closeOnConfirm : false,
						showLoaderOnConfirm : true,
					}, function(){
						setTimeout( function(){
							window.location.replace(site_url + 'zakat/penerimaan');
						}, 2000)
					});
			}else{
				swal("Gagal",result.msg,"error");
				//alert(result.msg);
			}
		},
		error: function(x,h,r){
			console.log(r);
		}
		
	});
	return false;

});

function jenis_pilihan(){
	
	if($('#select-form').val()==='1'){
		$('#trf-info').fadeIn(); 
		$('#chg-label').html('Jumlah Transfer');
		$('#no-rek').attr('required', true);
		$('#bank').attr('required', true);
		$('#userfile').attr('required', true);
	}else{
		$('#trf-info').fadeOut();
		$('#chg-label').html('Jumlah yang dibayarkan');
		$('#no-rek').attr('required', false);
		$('#bank').attr('required', false);
		$('#userfile').attr('required', false);
	}
}

//function for disabled add form
function disableAddForm(){
	var empty =true;
	$('.add-new input').each(function(){
		if($(this).val().length!==0)
			empty = false;
	})
	if(empty==false){
		$('#do-upload').attr('disabled', false);
	}else{
		$('#do-upload').attr('disabled', true);
	}
}

function get_pilihan(){
	//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
	$.ajax({
		url : site_url + 'zakat/penerimaan/getPilihan',dataType:'json',type:'get',
		success:function(result){
			$('#metode-bayar').html(result.data);
		}
	})
}
