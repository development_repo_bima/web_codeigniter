$(document).ready(function(){
	//init
	/*get_data();*/
	/* 
	*	check value search input,
	*	if value null not needed to reload page,
	*	for better perform :) 
	*	change as you wish if needed
	*/
	$('#clear').on('click', function(){
		if($('#input-search').val()){
			$('#input-search').val('');
			get_data();
			$('#cari').show();
			$(this).hide();
			return false;
		}
		return false;
	})
	/*
	*	search process and validation, cheers..
	*/
	$('#cari').on('click', function(){
		search();
		return false;
	})
	
	$('#search').submit( function(){
		search();
		return false;
	});

	$('#with-date').click(function(){
		if($(this).is(':checked')){
			$('#show-date').fadeIn();
			$('#default-date').fadeOut();
		}else{
			$('#show-date').fadeOut();
			$('#default-date').fadeIn();
		}
	})


      $('#range-date').daterangepicker({
        locale: {
            format: 'YYYY/MM/DD'
        }}, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
      });
});

function search(){
	var search = $('#input-search').val();
	if(!search){
		//alert('Silahkan masukkan keyword yang di cari');
		swal("Oops..","Could you type something?", "warning");
	}else{
		get_data(null, search);
		$('#cari').hide();
		$('#clear').show();
		return false;
	}
	return false;
}

function post_data(){
	
	
	$.ajax({
		
		url : site_url +'zakat/laporan/viewData',
		dataType : 'json',
		type : 'post',
		data : $('#form-laporan').serialize(),
		success: function(result){
			if(result.status){
				console.log(result);
				$(".table tbody").html(result.data);
			}else{
				swal("Gagal",result.msg,"error");
				//alert(result.msg);
			}
		},
		error: function(x,h,r){
			console.log(r);
		}
	})
}
function delete_record(id){
	swal({
			title : "Anda Yakin ?",
			text : "Anda akan menghapus record ini",
			type : "warning",
			showCancelButton : true,
			confirmButton : "#DD6855",
			confirmButtonText : "Ya",
			cancelButtonText : "Tidak",
			closeOnConfirm : false,
			closeOnCancel : false
		},
		function(isConfirm){
			if(isConfirm){
				$.ajax({
					url : site_url + 'zakat/pengeluaran/delete',
					data : {'id' : id},
					type : 'delete',
					dataType : 'json',
					success : function(result){
						if(result.status){
							swal("Berhasil",result.msg, "success");
							get_data();
						}else{
							swal("Gagal",result.msg, "error");
							get_data();
						}
					},
					error : function(x,h,r){
						console.log(r);
					}
				})
			}else{
				swal("Gagal","Anda Batal Menghapus User ini", "error");
			}
	});
	
}
function approval(id,status){
	if(status == '1'){
		msg = 'Setujui record ini?';
	}else{
		msg = 'Tolak record ini?';
	}
	swal({
			title : "Anda Yakin ?",
			text : msg,
			type : "warning",
			showCancelButton : true,
			confirmButton : "#DD6855",
			confirmButtonText : "Ya",
			cancelButtonText : "Tidak",
			closeOnConfirm : false,
			closeOnCancel : false
		},
		function(isConfirm){
			if(isConfirm){
				
				$.ajax({
					url : site_url + 'zakat/pengeluaran/approval',
					data : {id:id,status:status},
					type : 'PUT',
					dataType : 'json',
					success : function(result){
						if(result.status){
							swal("Berhasil",result.msg, "success");
							get_data();
						}else{
							swal("Gagal",result.msg, "error");
							get_data();
						}
					},
					error : function(x,h,r){
						console.log(r);
					}
				})

			}else{
				swal("Cancel","Anda Membatalkan Request ini", "error");
			}
	});
}
$(function(){
	$(document).on('click',"ul.pagination>li>a",function(){
	
		var href = $(this).attr('href');
		get_data(href);
		
		return false;
	});
});