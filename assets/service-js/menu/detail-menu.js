var main_url = site_url + 'manages/menu/';
get_parent();
$(document).ready(function(){
	get_detail(id);
	
	$('#edit-button').click(function(){
		$('#detail-view').fadeOut();
		$('#edit-view').fadeIn();
	})
	
	$('#save-edit').click(function(){
		update();
		return false;
	}) 
	
});
function get_detail(id){
	if(!id){
		//alert('gagal');
		swal("Error","Disallowed Action","error");
		notFound();
	}
	$.ajax({
		url : main_url + 'getDetail',
		data : {'id' : id},
		dataType : 'json',
		type : 'get',
		success : function(result){
			//console.log(result.data);
			if(result.status){

				//for detail
				$('#init-menu').html(result.data.const);
				$('#menu-name').html(result.data.menu);
				$('#icon-menu').html(result.data.icon);
				$('#url-menu').html(result.data.url);
				$('#parent-id').html((!result.data.parents) ? 'null' : result.data.parents);
				$('#is-parent').html((result.data.induk==0)?'no' : 'yes');
				//for edit
				$('#menuform-menu').val(result.data.menu);
				$('#menuform-icon').val(result.data.icon);
				$('#menuform-url').val(result.data.url);
				$('#menuform-parent').val(result.data.parent);
			}else{
				notFound();
			}
		},
		error : function(x,h,r){
			console.log(r);
		}
	})
}
function update(){
	//console.log($('#form-edit').serialize());
	var id_init = '&id='+id;
	$.ajax({
		url : main_url + 'updateData',dataType : 'json',type : 'patch',
		data : $('#form-edit').serialize()+id_init,//{'id':id,'username':$('#edit-username').val(),'email':$('#edit-email').val()},
		success : function(result){
				if(result.status){
				//alert(result.msg);
				swal({
					title :"Berhasil",
					text : result.msg,
					type : "success",
					showCancelButton : false,
					closeOnConfirm : false,
					showLoaderOnConfirm : true,
				}, function(){
					setTimeout( function(){
						cancel();
					}, 2000)
				});
			}else{
				swal("Gagal",result.msg,"error");
			}
		}
	})
}
function get_parent(){
	//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
	$.ajax({
		url : main_url + 'getParent',dataType:'json',type:'get',
		success:function(result){
			$('#parent-menu').html(result.data);
			
		}
	})
}
function asParentForm(){
	if($('#is-parents').is(':checked')){
		$('#select-parent-form').attr('disabled', true);
	}else{
		$('#select-parent-form').attr('disabled', false);
	}
}
function cancel(){
	location.replace(main_url);
}
function notFound(){
	location.replace(site_url+'testing/notFound');
}