var main_url = site_url + 'manages/menu/';
$(document).ready(function(){
	
	get_parent();
	
	$('#parent-status').on('click', function(){
		if($('#parent-status').is(':checked')){
			$('#child').fadeOut();
			$('#select-parent-form').val(null);
		}else{
			$('#child').fadeIn();
		};
		
	})
	
	//when add data submitted
	$('#submitted').click(function(){
		var empty = $('#form-menu').find("input").filter(function(){
			return this.value === "";
		});
		if(empty.length){
			swal("Info","Periksa Isian Anda", "info");
		}else{
			save_data();
		}
		return false;
	})
	
});
//ajax
function save_data(){
	
	var url = main_url+'addData'
	
	$.ajax({
		
		url : url,
		dataType : 'json',
		type : 'post',
		data : $('#form-menu').serialize(),
		success: function(result){
			if(result.status){
				//alert(result.msg);
				swal({
						title :"Berhasil",
						text : result.msg,
						type : "success",
						showCancelButton : false,
						closeOnConfirm : false,
						showLoaderOnConfirm : true,
					}, function(){
						setTimeout( function(){
							window.location.replace(main_url);
						}, 2000)
					});
			}else{
				swal("Gagal",result.msg,"error");
				//alert(result.msg);
			}
		},
		error: function(x,h,r){
			console.log(r);
		}
	})
}
function cancel(){
	location.replace(main_url);
}
function get_parent(){
	//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
	$.ajax({
		url : main_url + 'getParent',dataType:'json',type:'get',
		success:function(result){
			$('#child').html(result.data);
		}
	})
}