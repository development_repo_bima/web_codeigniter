
$(document).ready(function(){
	get_detail(id);
	
	$('#edit-button').click(function(){
		$('#detail-view').fadeOut();
		$('#edit-view').fadeIn();
		$('#header-tuts').html('Edit User');
		get_jabatan();
	})
	
	$('#save-edit').click(function(){
		update();
		return false;
	}) 
	
});
function get_detail(id){
	if(!id){
		//alert('gagal');
		swal("Error","Disallowed Action","error");
		notFound();
	}
	$.ajax({
		url : site_url + 'user/getDetail',
		data : {'id' : id},
		dataType : 'json',
		type : 'get',
		success : function(result){
			//console.log(result.data);
			//$('.data-detail tbody').html(result.data);
			//for detail val
			$('#nameuser').html(result.data.username);
			$('#emailuser').html(result.data.email);
			$('#jabatannama').html(result.data.nama_jabatan);
			//for edit val
			$('#username-edit').val(result.data.username);
			$('#email-edit').val(result.data.email);
		},
		error : function(x,h,r){
			console.log(r);
		}
	})
}
function update(){
	$.ajax({
		url : site_url + 'user/updateData',dataType : 'json',type : 'patch',
		data : $('#form-edit').serialize()+'&id='+id+'&tipe_user='+$('#select-form').val(),
		success : function(result){
			if(result.status){
				//alert(result.msg);
				swal({
					title :"Berhasil",
					text : result.msg,
					type : "success",
					showCancelButton : false,
					closeOnConfirm : false,
					showLoaderOnConfirm : true,
				}, function(){
					setTimeout( function(){
						cancel();
					}, 2000)
				});
			}
		}
	})
}
function cancel(){
	location.replace(site_url+'user');
}
function disabledform(){
	if($('#change-pass').is(':checked')){
		$('#password-edit').attr('disabled', false);
	}else{
		$('#password-edit').attr('disabled', true);
	}
}
function disabledselect(){
	if($('#change-role').is(':checked')){
		$('#select-form').attr('disabled', false);
	}else{
		$('#select-form').attr('disabled', true);
	}
}
function get_jabatan(){
	//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
	$.ajax({
		url : site_url + 'user/getJabatan',dataType:'json',type:'get',
		success:function(result){
			$('#fuchs').html(result.data);
			$('#select-form').attr('disabled', true);
		}
	})
}
function notFound(){
	location.replace(site_url+'testing/notFound');
}