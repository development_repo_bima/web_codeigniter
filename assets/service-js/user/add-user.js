$(document).ready(function(){
	get_jabatan();
	//when pass and conf-pass not match fadeIn alert and disabled submit button
	$('#password-conf').on('change', function(){
		var conf = $(this).val(); raw = $('#password').val();
		if(conf != raw){
			$('#warns').fadeIn();
			$('#warns strong').html('Konfirmasi password tidak sesuai');
			$('#submitted').attr('disabled', true);
		}else{
			$('#warns').fadeOut();
			$('#submitted').attr('disabled', false);
		}
	})
	// event handler for disabled cek
	$('.checks input').keyup(function(){
		disableCekForm();
	})
	//event handler for disabled add
	$('.add-new input').keyup(function(){
		disableAddForm();
	})
	
	//swap interface
	$('#checkbest').click(function(){
		$('#chk-form').fadeIn();
		$('#add-form').fadeOut();
	});
	//swap over
	$(document).on('click','#back-to-form',function(){
		$('#chk-form').fadeOut();
		$('#add-form').fadeIn();
	})
	
	//when clicked for password check submitted
	$(document).on('click','#check-pass',function(){
		var empty = $('#chk-form').find("input").filter(function(){
			return this.value === "";
		});
		if(empty.length){
			swal("Info","Periksa Isian Anda", "info");
		}else{
			check_data();
		}
		return false;
	})
	
	//when add data submitted
	$('#submitted').click(function(){
		var empty = $('#add-form').find("input").filter(function(){
			return this.value === "";
		});
		if(empty.length){
			swal("Info","Periksa Isian Anda", "info");
		}else{
			save_data();
		}
		return false;
	})
	
});
//function for disabled cek form
function disableCekForm(){
	var empty =true;
	$('.checks input').each(function(){
		if($(this).val().length!==0)
			empty = false;
	})
	if(empty==false){
		$('#check-pass').attr('disabled', false);
	}else{
		$('#check-pass').attr('disabled', true);
	}
}
//function for disabled add form
function disableAddForm(){
	var empty =true;
	$('.add-new input').each(function(){
		if($(this).val().length!==0)
			empty = false;
	})
	if(empty==false){
		$('#submitted').attr('disabled', false);
	}else{
		$('#submitted').attr('disabled', true);
	}
}
//ajax
function save_data(){
	
	var url = site_url + 'user/addData'
	
	$.ajax({
		
		url : url,
		dataType : 'json',
		type : 'post',
		data : $('#form-user').serialize()+'&tipe_user='+$('#select-form').val(),
		success: function(result){
			if(result.status){
				//alert(result.msg);
				swal({
						title :"Berhasil",
						text : result.msg,
						type : "success",
						showCancelButton : false,
						closeOnConfirm : false,
						showLoaderOnConfirm : true,
					}, function(){
						setTimeout( function(){
							window.location.replace(site_url + 'user');
						}, 2000)
					});
			}else{
				swal("Gagal",result.msg,"error");
				//alert(result.msg);
			}
		},
		error: function(x,h,r){
			console.log(r);
		}
	})
}

function check_data(url){
	
	//alert(url);
	if(!url){
		url = site_url+'user/check';
	}
	
	$.ajax({
		
		url:url,dataType:'json',type:'post',data: $('#form-check').serialize(),
		success: function(result){
			if(result.status){
				swal("Info!",result.msg,"info");
				//alert(result.msg);
			}else{
				swal("Info!",result.msg,"info");
				//alert(result.msg);
			}
		},
		error: function(x,h,r){
			console.log(r);
		}
	})
	
}
function get_jabatan(){
		//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
		$.ajax({
			url : site_url + '/user/getJabatan',dataType:'json',type:'get',
			success:function(result){
				$('#fuchs').html(result.data);
			}
		})
	}