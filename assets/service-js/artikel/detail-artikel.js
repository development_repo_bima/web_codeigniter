$(document).ready(function(){
	get_detail(id);
	$('#uid').val(id);
	$('.summernote').summernote({
        height: 200,
        tabsize: 4,
        styleWithSpan: true
      });
	
	$('#edit-button').click(function(){
		$('#detail-view').fadeOut();
		$('#edit-view').fadeIn();
		$('#header-tuts').html('Edit User');
	})
	
	/* $('#save-edit').click(function(){
		//console.log($('.summernote').summernote("code"));
		update();
		return false;
	})  */
	//console.log($('#uid').val());
	$('#form-edit').ajaxForm({
		
		url : site_url + 'artikel/updateData',
		//delegation : true,
		type : 'post',
		data : $('#form-edit').serialize(),
		dataType : 'json',
		resetForm : true,
		success : function(result){
			if(result.status){
				//alert(result.msg);
				swal({
						title :"Berhasil",
						text : result.msg,
						type : "success",
						showCancelButton : false,
						closeOnConfirm : false,
						showLoaderOnConfirm : true,
					}, function(){
						setTimeout( function(){
							window.location.replace(site_url + 'artikel');
						}, 2000)
					});
			}else{
				swal("Gagal",result.msg,"error");
				//alert(result.msg);
			}
		},
		error : function(x,h,r){
			console.log(r);
		}
		
	});
	return false;
	
});
function get_detail(id){
	if(!id){
		//alert('gagal');
		swal("Error","Disallowed Action","error");
		notFound();
	}
	$.ajax({
		url : site_url + 'artikel/getDetail',
		data : {'id' : id},
		dataType : 'json',
		type : 'get',
		success : function(result){
			//console.log(result.data);
			//$('.data-detail tbody').html(result.data);
			//for detail val
			$('#title-artikel').html(result.data.title);
			$('#content-artikel').html(result.data.content);
			$('#images-content').html('<img src="'+site_url+'assets/files/'+result.data.image+'" alt="pics" width="100%" class="img-rounded profile_img">')
			//for edit val
			$('#title-edit').val(result.data.title);
			$('.summernote').summernote("code",result.data.content);
		},
		error : function(x,h,r){
			console.log(r);
		}
	})
}
function update(){
	$.ajax({
		url : site_url + 'artikel/updateData',dataType : 'json',type : 'patch',
		data : $('#form-edit').serialize()+'&id='+id,
		success : function(result){
			if(result.status){
				//alert(result.msg);
				swal({
					title :"Berhasil",
					text : result.msg,
					type : "success",
					showCancelButton : false,
					closeOnConfirm : false,
					showLoaderOnConfirm : true,
				}, function(){
					setTimeout( function(){
						cancel();
					}, 2000)
				});
			}
		}
	})
}
function cancel(){
	location.replace(site_url+'artikel');
}
function disabledform(){
	if($('#change-pass').is(':checked')){
		$('#password-edit').attr('disabled', false);
	}else{
		$('#password-edit').attr('disabled', true);
	}
}
function disabledselect(){
	if($('#change-role').is(':checked')){
		$('#select-form').attr('disabled', false);
	}else{
		$('#select-form').attr('disabled', true);
	}
}
function notFound(){
	location.replace(site_url+'testing/notFound');
}