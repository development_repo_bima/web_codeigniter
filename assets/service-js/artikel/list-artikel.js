$(document).ready(function(){
	//init
	get_data();
	/* 
	*	check value search input,
	*	if value null not needed to reload page,
	*	for better perform :) 
	*	change as you wish if needed
	*/
	$('#clear').on('click', function(){
		if($('#input-search').val()){
			$('#input-search').val('');
			$('#cari').show();
			$(this).hide();
			get_data();
			return false;
		}
		
		return false;
	})
	/*responsive table*/
	$('.table-responsive').on('show.bs.dropdown', function(){
		$('.table-responsive').css('overflow', 'inherit');
	})
	$('.table-responsive').on('hide.bs.dropdown', function(){
		$('.table-responsive').css('overflow', 'auto');
	})
	/*
	*	search process and validation, cheers..
	*/
	$('#cari').on('click', function(){
		search();
		return false;
	})

	$('#search').submit( function(){
		search();
		return false;
	})
	
});

function search(){
	var search = $('#input-search').val();
	if(!search){
		//alert('Silahkan masukkan keyword yang di cari');
		swal("Oops..","Could you type something?", "warning");
	}else{
		get_data(null,search);
		$('#cari').hide();
		$('#clear').show();
		return false;
	}
	return false;
}

/*
*	hit this function to get data with or without search
*/
function get_data(url, param){
	if(!url)
		url = site_url +'artikel/getData'
	
	$.ajax({
			url : url,
			/*data: $('#search').serialize(),*/
			data: {search:param},
			dataType : 'json',
			type : 'get',
			success : function(result){
				$(".table tbody").html(result.data);
				$("ul.pagination").html(result.paging);
			},
			error: function(x,h,r){
				console.log(r);
			}
		})
}
function delete_artikel(id){
	swal({
			title : "Anda Yakin ?",
			text : "Anda akan menghapus artikel ini",
			type : "warning",
			showCancelButton : true,
			confirmButton : "#DD6855",
			confirmButtonText : "Ya",
			cancelButtonText : "Tidak",
			closeOnConfirm : false,
			closeOnCancel : false
		},
		function(isConfirm){
			if(isConfirm){
				$.ajax({
					url : site_url + 'artikel/deleteArtikel',
					data : {'id' : id},
					type : 'delete',
					dataType : 'json',
					success : function(result){
						if(result.status){
							swal("Berhasil",result.msg, "success");
							get_data();
						}else{
							swal("Gagal",result.msg, "error");
							get_data();
						}
					},
					error : function(x,h,r){
						console.log(r);
					}
				})
			}else{
				swal("Gagal","Anda Batal Menghapus User ini", "error");
			}
	});
	
}
function activated_artikel(id,status){
	//console.log(id+' '+status);
	//var msg = '';
	if(status == '1'){
		msg = 'Anda akan menayangkan artikel ini?';
	}else{
		msg = 'Anda akan menyembunyikan artikel ini?';
	}
	swal({
			title : "Anda Yakin ?",
			text : msg,
			type : "warning",
			showCancelButton : true,
			confirmButton : "#DD6855",
			confirmButtonText : "Ya",
			cancelButtonText : "Tidak",
			closeOnConfirm : false,
			closeOnCancel : false
		},
		function(isConfirm){
			if(isConfirm){
				
				$.ajax({
					url : site_url + 'artikel/activatedArtikel',
					data : {id:id,status:status},
					type : 'PUT',
					dataType : 'json',
					success : function(result){
						if(result.status){
							swal("Berhasil",result.msg, "success");
							get_data();
						}else{
							swal("Gagal",result.msg, "error");
							get_data();
						}
					},
					error : function(x,h,r){
						console.log(r);
					}
				})

			}else{
				swal("Cancel","Anda Membatalkan Request ini", "error");
			}
	});
}
$(function(){
	$(document).on('click',"ul.pagination>li>a",function(){
	
		var href = $(this).attr('href');
		get_data(href);
		
		return false;
	});
});