$(document).ready(function(){
	get_category();
	//when pass and conf-pass not match fadeIn alert and disabled submit button
	
	$('.summernote').summernote({
        height: 200,
        tabsize: 2,
        styleWithSpan: true
      });
	$('.note-editable').attr('contenteditable',false);
	
	//animate for summernote
	$('#switch-editor').click(function(){
		if($(this).is(':checked')){
			$('.note-editable').attr('contenteditable',true); //summernote disabled
			$('#switch-label').attr('title','Slide Off this button to save your work on editor');//title for checkbox button
			$('#container-editor').val(''); //saved editor container
			$('#submitted').attr('disabled',true); //save button disabled
			//console.log('checked');
		}else{
			//chcek comment on if above, change val for above code
			$('.note-editable').attr('contenteditable',false);
			$('#switch-label').attr('title','Slide It to add contents');
			$('#container-editor').val($('.summernote').val());
			$('#submitted').attr('disabled',false);
			console.log($('#container-editor').val());
		}
	})
	
	//ajax upload
	$('#form-artikel').ajaxForm({
		
		url : site_url + 'artikel/addData',
		//delegation : true,
		type : 'post',
		data : $('#form-artikel').serialize(),
		dataType : 'json',
		resetForm : true,
		success : function(result){
			if(result.status){
				//alert(result.msg);
				swal({
						title :"Berhasil",
						text : result.msg,
						type : "success",
						showCancelButton : false,
						closeOnConfirm : false,
						showLoaderOnConfirm : true,
					}, function(){
						setTimeout( function(){
							window.location.replace(site_url + 'artikel');
						}, 2000)
					});
			}else{
				swal("Gagal",result.msg,"error");
				//alert(result.msg);
			}
		},
		error : function(x,h,r){
			console.log(r);
		}
		
	});
	return false;
	
});


function get_category(){
		//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
		$.ajax({
			url : site_url + '/artikel/getCategory',dataType:'json',type:'get',
			success:function(result){
				$('#fuchs').html(result.data);
			}
		})
}