<?php
/**
 * Backend Controller parent
 * 
 * @author Bima S
 *
 */
class My_Back extends MY_Controller {

	public $access_arr = ['view','add','edit','remove'];

	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('loggedIn'))
			redirect('front/login');
		
	}
	
	public function _nampil($view, $data = array()){
		
		$this->load->view('layout/header', $data);
		$this->load->view('layout/sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('layout/footer');
		
	}
	
	/* public function response($res=array()){
		
		echo json_encode($res);
	} */
	
	public function _paging($url, $total, $limit){
		$config = array(
			'base_url'		=> site_url().$url,
			'total_rows'	=> $total,
			'per_page'		=> $limit,
			'uri_segment'	=> 3
		);
		//bootstrap config pagination
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
	    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	    $config['next_tag_open'] = "<li>";
	    $config['next_tagl_close'] = "</li>";
	    $config['prev_tag_open'] = "<li>";
	    $config['prev_tagl_close'] = "</li>";
	    $config['first_tag_open'] = "<li>";
	    $config['first_tagl_close'] = "</li>";
	    $config['last_tag_open'] = "<li>";
	    $config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
		
		return $this->pagination->create_links();
	}

}