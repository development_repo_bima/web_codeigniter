<?php

/**
 * Extending The Controller
 * 
 * @author Bima S
 *
 */
/* load the MX_Loader class */
require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {
    
    public function __construct() {
        
       parent::__construct();
    }
	
	public function response($res=array(), $isNumeric = true){
		/* if you won't check numeric in json, set $isNumeric false*/
		if($isNumeric){
			echo json_encode($res, JSON_NUMERIC_CHECK);
		}else{
			echo json_encode($res);
		}
	}
}