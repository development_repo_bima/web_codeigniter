<?php 

/**
 * Frontend Controller parent
 * 
 * @author Bima S
 *
 */

class My_Front extends MY_Controller{

	public function __construct(){
		
		parent::__construct();
		
		if($this->session->userdata('loggedIn'))
			redirect('home');
		//print("saya Bapaknya");
	}
	
	public function _nampil($view, $data = array()){
		
		$this->load->view('front_layout/front_header', $data);
		$this->load->view('front_layout/front_menu', $data);
		$this->load->view($view, $data);
		$this->load->view('front_layout/front_footer');
		
	}
	
	public function _render($view, $data = array()){
		
		$this->load->view('front_layout/new_header', $data);
		$this->load->view('front_layout/new_menu', $data);
		$this->load->view($view, $data);
		$this->load->view('front_layout/new_footer');
		$this->load->view('front_layout/style_switcher');
		//$this->load->view('front_layout/js_loader');
		
	}

	public function _render_home($view, $data = []){
		$this->load->view('front_layout/home/new_up', $data);
		$this->load->view($view, $data);
		$this->load->view('front_layout/home/new_down');
	}

	/* public function response($res=array()){
		
		echo json_encode($res);
	} */
	
	public function _front_paging($url, $total, $limit){
		$config = array(
			'base_url'		=> site_url().$url,
			'total_rows'	=> $total,
			'per_page'		=> $limit,
			'uri_segment'	=> 3
		);
		//bootstrap config pagination
	    $config['display_pages'] = FALSE;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['next_link'] = 'Older &rarr;';
		$config['prev_link'] = '&larr; Newer';
	    $config['cur_tag_open'] = "<li class='disabled'><li class='active_page'><a href='#'>";
	    $config['cur_tag_close'] = "<span></span></a></li>";
	    $config['next_tag_open'] = "<li class ='next'>";
	    $config['next_tag_close'] = "</li>";
	    $config['prev_tag_open'] = "<li class ='previous'>";
	    $config['prev_tag_close'] = "</li>";
		
		$this->pagination->initialize($config);
		
		return $this->pagination->create_links();
	}
	
}