<!--=== Header v8 ===-->
	<div class="header-v8 header-sticky">
		<!-- Topbar blog -->
		<div class="blog-topbar">
			<div class="topbar-search-block">
				<div class="container">
					<form action="#">
						<input type="text" class="form-control" placeholder="Search">
						<div class="search-close"><i class="icon-close"></i></div>
					</form>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-xs-8">
						<div class="topbar-time"><?php echo date('l').', '.date('M jS Y'); ?></div>
						<div class="topbar-toggler"><span class="fa fa-angle-down"></span></div>
						<ul class="topbar-list topbar-menu">
							<li><a href="#">Contact</a></li>
							<li><a href="#">Forums</a></li>
							<li class=" hidden-sm hidden-md hidden-lg"><strong><a class="" href="<?php echo site_url('auth'); ?>">Login or Register</a></strong></li>
							<!-- <li class="cd-log_reg hidden-sm hidden-md hidden-lg"><strong><a class="cd-signup" href="javascript:void(0);">Register</a></strong></li> -->
						</ul>
					</div>
					<div class="col-sm-4 col-xs-4 clearfix">
						<i class="fa fa-search search-btn pull-right"></i>
						<ul class="topbar-list topbar-log_reg pull-right visible-sm-block visible-md-block visible-lg-block">
							<li class="home"><a class="" href="<?php echo site_url('auth'); ?>">Login or Register</a></li>
							<!-- <li class="cd-log_reg"><a class="cd-signup" href="javascript:void(0);">Register</a></li> -->
						</ul>
					</div>
				</div><!--/end row-->
			</div><!--/end container-->
		</div>
		<!-- End Topbar blog -->

		<!-- Navbar -->
		<div class="navbar mega-menu" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="res-container">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<div class="navbar-brand">
						<a href="index.html">
							<img src="<?php echo base_url(); ?>public/assets/img/themes/logo-news-dark-brown.png" alt="Logo">
						</a>
					</div>
				</div><!--/end responsive container-->

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-responsive-collapse">
					<div class="res-container">
						<ul class="nav navbar-nav">
							<?php echo $this->modul->get_home_menu(); ?>
							<!-- Home -->
						</ul>
					</div><!--/responsive container-->
				</div><!--/navbar-collapse-->
			</div><!--/end contaoner-->
		</div>
		<!-- End Navbar -->
	</div>
	<!--=== End Header v8 ===-->

	<!--== generate Breadcrumbs ==-->
	<?php echo $this->modul->breadcrumbs(); ?>