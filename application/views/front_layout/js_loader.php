<!-- JS Global Compulsory -->
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script src="<?php echo base_url(); ?>public/assets/plugins/back-to-top.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/smoothScroll.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/counter/waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/counter/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/master-slider/masterslider/masterslider.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/master-slider/masterslider/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/modernizr.js"></script>
<script src="<?php echo base_url(); ?>public/assets/plugins/login-signup-modal-window/js/main.js"></script> <!-- Gem jQuery -->
<!-- JS Customization -->
<script src="<?php echo base_url(); ?>public/assets/js/custom.js"></script>
<!-- JS Page Level -->
<script src="<?php echo base_url(); ?>public/assets/js/app.js"></script>
<script src="<?php echo base_url(); ?>public/assets/js/plugins/fancy-box.js"></script>
<script src="<?php echo base_url(); ?>public/assets/js/plugins/owl-carousel.js"></script>
<script src="<?php echo base_url(); ?>public/assets/js/plugins/master-slider-showcase2.js"></script>
<script src="<?php echo base_url(); ?>public/assets/js/plugins/style-switcher.js"></script>
<script>
	jQuery(document).ready(function() {
		App.init();
		App.initCounter();
		FancyBox.initFancybox();
		OwlCarousel.initOwlCarousel();
		OwlCarousel.initOwlCarousel2();
		StyleSwitcher.initStyleSwitcher();
		MasterSliderShowcase2.initMasterSliderShowcase2();
	});
</script>
<!--[if lt IE 9]>
	<script src="assets/plugins/respond.js"></script>
	<script src="assets/plugins/html5shiv.js"></script>
	<script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.9.7/Blog-Magazine/blog_page_layouts1.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Jan 2017 12:29:54 GMT -->
</html>