<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- Mirrored from htmlstream.com/preview/unify-v1.9.7/Blog-Magazine/blog_post_layouts_fw.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Jan 2017 12:30:06 GMT -->
<head>
	<title>Blog Medium | Unify - Responsive Website Template</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/blog.style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/headers/header-v8.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/footers/footer-v8.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/animate.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/fancybox/source/jquery.fancybox.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/master-slider/masterslider/style/masterslider.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/master-slider/masterslider/skins/default/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/login-signup-modal-window/css/style.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/theme-colors/brown.css" id="style_color">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/theme-skins/dark.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/custom.css">
	<?php if(isset($custom_css)){foreach($custom_css as $css){ echo "<link href='".base_url().$css."'rel='stylesheet' />"; };} ?>

	<!-- JS Global Compulsory -->
	<script src="<?php echo base_url(); ?>public/assets/plugins/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- JS Implementing Plugins -->
	<script src="<?php echo base_url(); ?>public/assets/plugins/back-to-top.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/smoothScroll.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/counter/waypoints.min.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/counter/jquery.counterup.min.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/master-slider/masterslider/masterslider.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/master-slider/masterslider/jquery.easing.min.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/modernizr.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/plugins/login-signup-modal-window/js/main.js"></script> <!-- Gem jQuery -->
	<!-- JS Customization -->
	<script src="<?php echo base_url(); ?>public/assets/js/custom.js"></script>
	<script>var site_url = '<?php echo site_url(); ?>';</script>
	<!-- JS Page Level -->
	<script src="<?php echo base_url(); ?>public/assets/js/app.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/js/plugins/fancy-box.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/js/plugins/owl-carousel.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/js/plugins/master-slider-showcase2.js"></script>
	<script src="<?php echo base_url(); ?>public/assets/js/plugins/style-switcher.js"></script>
	<?php if(isset($custom_js)){foreach($custom_js as $js){ echo "<script src='".base_url().$js."'></script>"; };} ?>
	<script>
		jQuery(document).ready(function() {
			App.init();
			App.initCounter();
			FancyBox.initFancybox();
			OwlCarousel.initOwlCarousel();
			OwlCarousel.initOwlCarousel2();
			StyleSwitcher.initStyleSwitcher();
			MasterSliderShowcase2.initMasterSliderShowcase2();
		});
	</script>
</head>

<body class="header-fixed header-fixed-space-v2">
<div class="wrapper">