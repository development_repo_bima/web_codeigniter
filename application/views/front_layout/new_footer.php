<!--=== Footer v8 ===-->
	<div class="footer-v8">
		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 column-one md-margin-bottom-50">
						<a href="index.html"><img class="footer-logo" src="assets/img/themes/logo-news-light-brown.png" alt=""></a>
						<p class="margin-bottom-20">Unify is an ultra fully responsive template with modern and smart design.</p>
						<span>Headquarters:</span>
						<p>795 Folsom Ave, Suite 600, San Francisco, CA 94107</p>
						<hr>
						<span>Phone:</span>
						<p>(+123) 456 7890</p>
						<p>(+123) 456 7891</p>
						<hr>
						<span>Email Address:</span>
						<a href="#">support@htmlstream.com</a>
					</div>

					<div class="col-md-6 col-sm-6">
						<h2>Newsletter</h2>
						<p><strong>Subscribe</strong> to our newsletter and stay up to date with the latest news and deals!</p><br>

						<!-- Form Group -->
						<div class="input-group margin-bottom-50">
							<input class="form-control" type="email" placeholder="Enter email">
							<div class="input-group-btn">
								<button type="button" class="btn-u input-btn">Subscribe</button>
							</div>
						</div>
						<!-- End Form Group -->

						<h2>Social Network</h2>
						<p><strong>Follow Us</strong> If you want to be kept up to date about what’s going on, minute by minute, then search for Grant and give us a follow!</p><br>

						<!-- Social Icons -->
						<ul class="social-icon-list margin-bottom-20">
							<li><a href="#"><i class="rounded-x fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="rounded-x fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="rounded-x fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="rounded-x fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="rounded-x fa fa-dribbble"></i></a></li>
						</ul>
						<!-- End Social Icons -->
					</div>
				</div><!--/end row-->
			</div><!--/end container-->
		</footer>

		<footer class="copyright">
			<div class="container">
				<ul class="list-inline terms-menu">
					<li>2015 &copy; All Rights Reserved.</li>
					<li class="home"><a href="#">Terms of Use</a></li>
					<li><a href="#">Privacy and Policy</a></li>
				</ul>
			</div><!--/end container-->
		</footer>
	</div>
	<!--=== End Footer v8 ===-->

	<div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container"> <!-- this is the container wrapper -->
			

			<div id="cd-login"> <!-- log in form -->
				<form class="cd-form" id="auth-form" method="post" action="<?php echo site_url().'/auth/doLogin';?>" autocomplete="off">
					<!-- <p class="social-login">
						<span class="social-login-facebook"><a href="#"><i class="fa fa-facebook"></i> Facebook</a></span>
						<span class="social-login-google"><a href="#"><i class="fa fa-google"></i> Google</a></span>
						<span class="social-login-twitter"><a href="#"><i class="fa fa-twitter"></i> Twitter</a></span>
					</p> -->
					<p class="social-login">
						<span class="cd-error-message"><?php echo ($this->session->flashdata('error_login')) ? $this->session->flashdata('error_login') : ''; ?></span>
					</p>
					<div class="lined-text"><span>Login to your account</span><hr></div>

					<p class="fieldset">
						<label class="image-replace cd-email" for="signin-email">E-mail</label>
						<input class="full-width has-padding has-border" name="username" id="signin-email" type="text" placeholder="Username">
						<span class="cd-error-message">Please fill the blank form</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-password">Password</label>
						<input class="full-width has-padding has-border" id="signin-password" type="password" name="password" placeholder="Password">
						<a href="javascript:void(0);" class="hide-password">Show</a>
						<span class="cd-error-message">Please fill the blank form</span>
					</p>

					<p class="fieldset">
						<input type="checkbox" id="remember-me" checked>
						<label for="remember-me">Remember me</label>
					</p>

					<p class="fieldset">
						<input class="full-width" type="submit" name="login-submit" value="Login">
					</p>
				</form>

				<!-- <p class="cd-form-bottom-message"><a href="javascript:void(0);">Forgot your password?</a></p> -->
				<!-- <a href="javascript:void(0);" class="cd-close-form">Close</a> -->
			</div> <!-- cd-login -->

			<div id="cd-signup"> <!-- sign up form -->
				<form class="cd-form">
					<p class="social-login">
						<span class="social-login-facebook"><a href="#"><i class="fa fa-facebook"></i> Facebook</a></span>
						<span class="social-login-google"><a href="#"><i class="fa fa-google"></i> Google</a></span>
						<span class="social-login-twitter"><a href="#"><i class="fa fa-twitter"></i> Twitter</a></span>
					</p>

					<div class="lined-text"><span>Or register your new account on Blog</span><hr></div>

					<p class="fieldset">
						<label class="image-replace cd-username" for="signup-username">Username</label>
						<input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-email" for="signup-email">E-mail</label>
						<input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signup-password">Password</label>
						<input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Password">
						<a href="javascript:void(0);" class="hide-password">Hide</a>
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<input type="checkbox" id="accept-terms">
						<label for="accept-terms">I agree to the <a href="javascript:void(0);">Terms</a></label>
					</p>

					<p class="fieldset">
						<input class="full-width has-padding" type="submit" value="Create account">
					</p>
				</form>

				<!-- <a href="javascript:void(0);" class="cd-close-form">Close</a> -->
			</div> <!-- cd-signup -->

			<div id="cd-reset-password"> <!-- reset password form -->
				<p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

				<form class="cd-form">
					<p class="fieldset">
						<label class="image-replace cd-email" for="reset-email">E-mail</label>
						<input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<input class="full-width has-padding" type="submit" value="Reset password">
					</p>
				</form>

				<p class="cd-form-bottom-message"><a href="javascript:void(0);">Back to log-in</a></p>
			</div> <!-- cd-reset-password -->
			<a href="javascript:void(0);" class="cd-close-form">Close</a>
		</div> <!-- cd-user-modal-container -->
	</div> <!-- cd-user-modal -->
</div><!--/wrapper-->
<script type="text/javascript">
	/*function auth_act(){
		console.log($('#auth-form').serialize());
	}*/
</script>

<!--[if lt IE 9]>
	<script src="assets/plugins/respond.js"></script>
	<script src="assets/plugins/html5shiv.js"></script>
	<script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.9.7/Blog-Magazine/blog_page_layouts1.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Jan 2017 12:29:54 GMT -->
</html>