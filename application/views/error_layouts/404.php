<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Unauthorized</title>

  <!-- Bootstrap core CSS -->

  <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo base_url();?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->

  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script> var site_url = "<?php echo site_url();?>"</script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>
<body>
<div class="col-lg-12">
	<div class="jumbotron well" align="center">
		
		<h1><i class="fa fa-ban fa-4x"></i><br> 403 !<br> Forbidden</h1>
		<p>
			You Don't Have Permission to Access The Page Now.<br> Please Contact Administrator Web For further Information.
		</p>
		<button class="btn btn-info" onclick="goBack()">Go Back</button>
	</div>
</div>


<script>
function goBack() {
    window.history.back();
}
</script>
</body>
</html>