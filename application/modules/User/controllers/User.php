<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends My_Back {

	protected static $modul = "LIST_USER";

	protected static $access = [];
	
	public function __construct(){
		
		parent::__construct();
		SELF::$access = $this->general->check_role($this->session->userdata('jabatan_id'), SELF::$modul, $this->access_arr);
		if(!SELF::$access['view'])
			redirect('testing/unAuthorized');

		$this->load->model('User_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		// check if role add have a value
		$data = [
			'title' 	=> 'List User',
			'header'	=> 'List User',
			'add_role'	=> (SELF::$access['add']) ? "" : "display:none;",
			'custom_js'	=> array('service' => 'assets/service-js/user/list-user.js')
		];
		
		$this->_nampil('userView', $data);
	}
	
	public function getData(){
		
		$limit  = $this->config->item('per_page');
		$offset = $this->uri->segment(3,0);
		$search = $this->input->get('search');
		$url 	= 'user/getData/';
		
		$data = $this->User_model->get_data($search, $limit, $offset);
		//print_r($data);exit;
		$total = $data['total'];
		$tr = $paging = '';
		if(!$data['data']){
			
			$tr .="<tr>";
			$tr .="<td colspan='5'>Not Found...</td>";
			$tr .="</tr>";

			$msg = 'Data Not found';
		};
		
		if($data['data']){
			$no = $offset + 1;
			
			$i = 1;
			foreach($data['data'] as $o){
				
				//styling button section, hope better way's. but now is fine
				$status = ($o->active=='0')?'1':'0';
				$active = ($o->active=='1')? "<i class='fa fa-ban'></i> Deactivate" : "<i class='fa fa-check'></i> Activate" ;
				$edit_role 	 = (SELF::$access['edit']) ? "" : "display:none;";
				$remove_role = (SELF::$access['remove']) ? "" : "display:none;";
				
				$tr .="<tr>";
				$tr .="<td>".$no."</td>";

				//button action
				if(!SELF::$access['edit'] && !SELF::$access['remove']){
					$tr .="<td>No Privileges</td>";
				}else{
					$tr .="<td><div class='btn-group'>";
					$tr .="<button class='btn btn-xs btn-default'>...</button>";
					$tr .="<button data-toggle='dropdown' class='btn btn-xs btn-default dropdown-toggle'><span class='caret'></span></button>";
					$tr .="<ul class='dropdown-menu'>";
					$tr .="<li style='".$edit_role."'><a href='".site_url().'user/detailView/'.$o->id."'><i class='fa fa-edit'></i> Edit</a></li>";
					$tr .="<li style='".$edit_role."'><a href='javascript:void(0)' onClick='activated_user(".$o->id.','.$status.")'>".$active."</a></li>";
					$tr .="<li style='".$remove_role."'><a href='javascript:void(0)' onClick='delete_user(".$o->id.")'><i class='fa fa-trash'></i> Delete</a></li>";
					$tr .="</ul></div></td>";
				}
				//end button action
				
				$tr .="<td>".$o->username."</td>";
				$tr .="<td>".$o->email."</td>";
				$tr .="<td>".$o->nama_jabatan."</td>";
				$tr .="<td>".$o->last_seen."</td>";
				$tr .="</tr>";
			$i++;$no++;
			}
			$paging .="<li><span class='page-info'>Displaying ".($i-1)." of ".$total." data</span></li>";
			$paging .= $this->_paging($url,$total,$limit);
			
			$msg = 'Data Loaded...';
		}
		//echo $tr;exit;
		//return $tr;
		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr,'paging'=>$paging));
		
	}
	
	public function addView(){
		$data = [
			'title' 	=> 'Add User',
			'header'	=> 'Add New User',
			'custom_js'	=> array('service' => 'assets/service-js/user/add-user.js')

		];
		
		$this->_nampil('addUserView', $data);
	}
	
	public function detailView(){
		$data = [
			'id'	=> $this->uri->segment(3),
			'title' => 'Detail User',
			'header'=> 'Detail User',
			'custom_js'	=> array('service' => 'assets/service-js/user/detail-user.js')
		];
		
		$this->_nampil('detailUserView', $data);
	}
	
	public function getDetail(){
		
		$get = $this->User_model->get_detail($this->input->get('id'));
		if($get){
			$res = ['status'=>true, 'msg'=>'Data Loaded...', 'data'=>$get];
		}else{
			$res = ['status'=>false,'msg'=>'Data Not Found...','data'=>[]];
		}
		$this->response($res);
	}
	
	public function check(){
		
		$query = $this->User_model->check_password($this->input->post(null, true));
		
		switch($query){
			
			case 0:
				$msg = 'Data tidak valid atau tidak sesuai';
				break;
			case 1:
				$msg = 'User Telah Terdaftar';
				break;
			case 9;
				$msg = 'Password Tidak sesuai';
				break;
			default:
				$msg = 'Status not defined';
		}
		
		//print_r($msg);exit
		if($query==1){
			$res = ['status'=>true,'msg'=>$msg];
		}else{
			$res = ['status'=>false,'msg'=>$msg];
		}
			
		$this->response($res);
	}
	
	public function addData(){
		
		$data = $this->input->post(null, true);
		$query = $this->User_model->save_data($data);
		
		if(!$query){
			$res = ['status'=>false,'msg'=>'Gagal menambahkan data'];
		}else{
			$res = ['status'=>true,'msg'=>'Berhasil menambahkan data'];
		}
		$this->response($res);
	}
	
	public function updateData(){
		//print_r($this->input->input_stream(null,true));exit;
		$update = $this->User_model->update_data($this->input->input_stream(null,true));
		if($update){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Ubah'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Ubah'];
		}
		$this->response($res);
	}
	
	public function activatedUser(){
		
		$data = $this->input->input_stream(null,true);
		//print_r($data);die;
		if($this->User_model->activate_user($data)){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Update..'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Update..'];
		}
		$this->response($res);
	}
	
	public function deleteUser(){
		
		$id = $this->input->input_stream('id');
		//die($id);
		if($this->User_model->delete_user($id)){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Hapus..'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Hapus..'];
		}
		$this->response($res);
	}
	
	
	public function get_check(){
		$page = 'DATA';
		$access = 'view';
		
		print_r($this->general->check_access($page,$access));exit;
		
	}
	
	public function getJabatan(){
		
		$query = $this->db->get('m_jabatan')->result_array();
		$sel = '';
		$sel .="<select class='form-control' required id='select-form'><option value=''>--Select Tipe User--</option>";
		foreach($query as $a){
			$sel .="<option value='".$a['jabatan_id']."'>".$a['nama_jabatan']."</option>";
		}
		$sel .="</select>";
		//print_r($sel);exit;
		$this->response(array('data'=>$sel));
	}
	
}
