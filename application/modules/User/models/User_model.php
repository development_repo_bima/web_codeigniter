<?php 

class User_model extends CI_model{
	
	public function get_data($search=null, $limit=0, $offset=0){
		//print_r($offset);exit;
		$this->db->select('u.username, u.email, u.last_seen,u.id,u.jabatan_id,u.active, j.nama_jabatan')
					->from('m_user as u')
					->join('m_jabatan as j','j.jabatan_id = u.jabatan_id','left');
					//->join('m_akses as a','a.jabatan_id = j.jabatan_id','left');
			if($search)
				$this->db->like('username', $search);
		
		$this->db->order_by('id', 'DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result();
		$data['total'] = $this->db->count_all('m_user');
		
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}
	
	public function get_detail($id){
		
		//print_r($this->db->last_query());exit;
		//active record ways still get errors. how it's work??
		return $this->db->query("SELECT u.username,u.email,u.jabatan_id, j.nama_jabatan 
									FROM m_user u 
									LEFT JOIN m_jabatan j ON j.jabatan_id = u.jabatan_id 
									WHERE u.id = {$id}")->row_array();
	}

	public function save_data($data){
	
		//print_r($data);exit;
		if(trim($data['password']) != trim($data['pass_conf']))
			return false;
		
		$salt = md5(trim($data['username']));
		$save = [
			'username'	=>$data['username'],
			'email'   	=>$data['mail'],
			'salt'	  	=>$salt,
			'hash'	  	=>crypt(trim($data['password']), '$2a$12$'.$salt.'$'),
			'jabatan_id'=>$data['tipe_user']
			//'created_at'=>date('Y-m-d')
		];
		//print_r($save);exit;
		$this->db->insert('m_user', $save);
		return true;
		
	}
	
	public function update_data($data){
		//print_r($data);exit;
		$salt = md5(trim($data['username']));
			
		$update = [
				'username'	=>$data['username'],
				'email'		=>$data['email']
		];
		
		if(isset($data['password']) && $data['password'] != ''){
			$update['salt'] = $salt;
			$update['hash'] = crypt(trim($data['password']), '$2a$12$'.$salt.'$');
		}
		
		if(isset($data['tipe_user'])){
			$update['jabatan_id'] = $data['tipe_user'];
		}
		
		return $this->db->update('m_user',$update,array('id'=>$data['id']));
	}
	
	public function check_password($data){
		
		if($data['username_check']=='' || $data['password_check'] ==''){
			$msg = 8;
		}else{
			$def = $this->db->get_where('m_user',['username'=>$data['username_check']])->row();
		
			if(!$def){
				$msg = 0;
			}else{
				$ver = $this-> _verified(trim($data['password_check']), $def->hash, $def->salt);
				if($ver){
					$msg = 1;
				}else{
					$msg = 9;
				}
			}
		}
		//print_r($msg);exit;
		return $msg;
	}
	
	public function activate_user($data){
		
		return $this->db->update('m_user',array('active'=>$data['status']), array('id'=>$data['id']));
	}
	
	public function delete_user($id){
		
		return $this->db->delete('m_user', array('id'=>$id));
	}
	
	private function _verified($password=null, $hash=null, $salt=null){
		
		$cek = crypt($password, '$2a$12$'.$salt.'$');
		if($cek === $hash){
			return true;
		}else{
			return false;
		}
	}


}