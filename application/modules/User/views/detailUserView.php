<!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
		
			

        </div>
        <!-- /top tiles -->
		
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
			<legend id="header-tuts"><?php echo isset($header) ? $header : '';?></legend>
				<br>
				<div id="detail-view" class="col-md-6">
					<table class="table data-detail">
						<tbody>
							<tr>
								<td><p>Username</p></td>
								<td><p id="nameuser"></p></td>
							</tr>
							<tr>
								<td><p>Email</p></td>
								<td><p id="emailuser"></p></td>
							</tr>
							<tr>
								<td><p>Tipe User</p></td>
								<td><p id="jabatannama"></p></td>
							</tr>
						</tbody>
					</table>
					<button id="edit-button" class="btn btn-success">Edit</button>
					<a id="cancel-button" onClick="cancel()" class="btn btn-danger">Cancel</a>
				</div>
				<div id="edit-view" style="display:none;">
					<form class="form-horizontal" id="form-edit" method="post">
						<fieldset>
						</fieldset>
						<!-- Button -->
						<div class="form-group">
						  <div class="col-md-6">
							<div class="form-group select-form">
							  <label>Username</label>
							  <input id="username-edit" name="username" class="form-control" placeholder="username here" required="" type="text">
							</div>
							<div class="form-group select-form">
							  <label>Email</label>
							  <input id="email-edit" name="email" class="form-control" placeholder="email here" required="" type="email">
							</div>
							<div class="form-group select-form">
							  <label>Password</label><small class="pull-right" style="color:red;">Ganti Password? <input type="checkbox" id="change-pass" onClick="disabledform()"></small>
							  <input disabled=true id="password-edit" name="password" class="form-control" placeholder="password here" required="" type="password">
							</div>
							<div class="form-group select-form">
								<label>Tipe User</label><small class="pull-right" style="color:red;">Ganti Tipe User? <input type="checkbox" id="change-role" onClick="disabledselect()"></small>
								<span id="fuchs"></span>
							</div>
							<button id="save-edit" class="btn btn-success">Save</button>
							<a id="cancel" onClick="cancel()" class="btn btn-danger">Cancel</a>
						  </div>
						</div>
					</form>
				</div>
				
              <div class="clearfix"></div>
            </div>
          </div>

        </div>
		<script type="text/javascript">var id = <?php echo isset($id) ? $id : 0;?>;</script>