<div class="right_col" role="main">
	<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph" id="add-form">
			<button id="checkbest" name="" class="btn btn-danger pull-right">Check Password</button>
			
				<form class="form-horizontal" id="form-user" method="post">
				<fieldset>

				<!-- Form Name -->
				<legend><?php echo isset($header) ? $header : '';?></legend>
				
				<!-- Prepended text-->
				<div class="form-group ">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>Username</label>
					  <input id="username" name="username" class="form-control" placeholder="username here" required="" type="text">
					</div>
				  </div>
				</div>

				<!-- Prepended text-->
				<div class="form-group">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>Email</label>
					  <input id="mail" name="mail" class="form-control" placeholder="email here" required="" type="email">
					</div>
					
				  </div>
				</div>
				

				<!-- Prepended text-->
				<div class="form-group">
				  <div class="col-md-12 col-sm-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>Password</label>
					  <input id="password" name="password" class="form-control" placeholder="password here" required="" type="password">
					</div>
				  </div>
				</div>
				
				<!-- Prepended text-->
				<div class="form-group">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>Password Confirm</label>
					  <input id="password-conf" name="pass_conf" class="form-control" placeholder="Re-type password" required="" type="password">
					</div>
					<div class="alert alert-danger alert-dismissable col-md-8 col-xs-12" style="display:none;" id="warns">
						<strong></strong>
					</div>
				  </div>
				</div>
				<div class="form-group select-form col-md-8 col-xs-12">
					<label>Tipe User</label>
					<span id="fuchs"></span>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <div class="col-md-8 col-xs-12"> 
				  	<label>Keterangan</label>                    
					<textarea class="form-control" id="info" name="info">Additional Info</textarea>
				  </div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-8">
					<button id="submitted" name="submitted" class="btn btn-success" disabled="true">Save</button>
					<button id="cancelled" onClick="history.back();" name="cancel" class="btn btn-danger">Cancel</button>
				  </div>
				</div>

				</fieldset>
				</form>
				<div class="clearfix"></div>
            </div>
			<div class="dashboard_graph" style="display:none;" id="chk-form">
				<form class="form-horizontal" id="form-check" method="post">
				<fieldset>

				<!-- Form Name -->
				<legend>CheckUser</legend>

				<!-- Prepended text-->
				<div class="form-group">
				  <div class="col-md-8">
					<div class="input-group checks">
					  <span class="input-group-addon">Username</span>
					  <input id="username_check" name="username_check" class="form-control" placeholder="username here" required="" type="text">
					</div>
					
				  </div>
				</div>

				<!-- Prepended text-->
				<div class="form-group">
				  <div class="col-md-8">
					<div class="input-group checks">
					  <span class="input-group-addon">Password</span>
					  <input id="password_check" name="password_check" class="form-control" placeholder="password here" type="password">
					</div>
					
				  </div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-8">
					<button id="check-pass" class="btn btn-success" disabled="true">Check</button>
					<button id="back-to-form" class="btn btn-danger">Cancel</button>
				  </div>
				</div>

				</fieldset>
				</form>
				<div class="clearfix"></div>
            </div>
          </div>

        </div>