<?php

class Testing extends My_Back{

	public function __construct(){
		parent::__construct();
		
		$this->load->model('Testing_model');
	}
	
	public function index(){
		$this->load->view('error_layouts/404');
	}

	public function unAuthorized(){
		$this->load->view('error_layouts/404');
	}

	public function notFound(){

		$this->_nampil('404_page');
	}
}