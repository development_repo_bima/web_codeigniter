<?php 

class Response extends My_Front{
	
	public function __construct(){
		parent::__construct();
		
		$this->load->model('Testing_model');
	}
	
	public function index(){
		$this->load->view('error_layouts/404');
	}

	public function unAuthorized(){
		$this->load->view('error_layouts/404');
	}

	public function notFound(){
		$data = [
			'custom_css' => [
				'style'	=> 'public/assets/css/page_404_error.css'
			]
		];
		$this->_render('404_page', $data);
	}
}