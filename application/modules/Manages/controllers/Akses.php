<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Akses extends My_Back{
	
	public function __construct(){
		
		parent::__construct();
		if($this->session->userdata('jabatan_id') != 1)
			redirect('testing/unAuthorized');

		$this->load->model('Akses_model');
	}
	
	public function index(){
		$this->load->view('errors/html/error_404');
	}

	public function detail(){
		$jabatan_id = $this->uri->segment(4,0);
		//die($jabatan_id);
		$exist = $this->Akses_model->exist_access($jabatan_id);
		//print_r($exist);exit;
		$d = $this->_add_box(0);
		if($exist>0){
			$d = $this->_edit_box(0, $jabatan_id);
		}
		//die($c);
		$data = array(
			'custom_css' => array('cekbox'=>'assets/css/checkboxes.css'),
			'tr'		 => $d,
			'jabatan_id' => $jabatan_id
		);
		
		$this->_nampil('credentialView',$data);
	}
	
	public function _add_box($parent=0){
		
		$tr = '';
		$q = $this->db->query("SELECT * FROM m_menu WHERE parent = {$parent} AND flag = 0")->result();
		$arr_act = $this->_act_alias();
		//print_r($this->db->last_query());
		foreach($q as $a){
			//print_r($role);exit;
			if($a->induk == 1){
				$check_st = "";
				$tr .="<tr>";
				$tr .="<td>".$a->menu."</td>";
				$tr .="<td align='center'><input type='checkbox' ".$check_st." name='data[".$a->const."][view]' id='view' value='1'></td>";
				$tr .="<td colspan='3'></td>";
				$tr .="</tr>";
			}else{
				$tr .="<tr>";
				$nbsp = "&nbsp;&nbsp;&nbsp;&nbsp;";
				if($a->parent == 0){
					$tr .="<td>".$a->menu."</td>";
				}else{
					$tr .="<td>".$nbsp.$a->menu."</td>";
				}
				foreach($arr_act as $act){
					$check_st2 = '';
					//print_r($this->db->last_query());	
					if(isset($role[$act]) && $role[$act]==1){
							$check_st2 = "checked='checked'";
							//exit($role[$act]);
					}
					//print_r($check_st2);exit;
					$tr .="<td align='center'><input type='checkbox' ".$check_st2." name='data[".$a->const."][".$act."]' id='".$act."' value='1'></td>";
				}
				$tr .="</tr>";
			}
			$tr .= $this->_add_box($a->id);
		}
		return $tr;
	}
	
	public function _edit_box($parent=0,$id=''){
		//die($id);
		$tr = '';
		$q = $this->db->query("SELECT * FROM m_menu WHERE parent = {$parent} AND flag = 0")->result();
		$arr_act = $this->_act_alias();
		//print_r($this->db->last_query());
		foreach($q as $a){
			//print($a->const);print($id);
			$role = $this->Akses_model->get_role($a->const,$id);
			//print_r($role);
			if($a->induk == 1){
				$check_st = ($role['view']==1)? "checked='checked'" : "";
				$tr .="<tr>";
				$tr .="<td>".$a->menu."</td>";
				$tr .="<td align='center'><input type='checkbox' ".$check_st." name='data[".$a->const."][view]' id='view' value='1'></td>";
				$tr .="<td colspan='3'></td>";
				$tr .="</tr>";
			}else{
				$tr .="<tr>";
				$nbsp = "&nbsp;&nbsp;&nbsp;&nbsp;";
				if($a->parent == 0){
					$tr .="<td>".$a->menu."</td>";
				}else{
					$tr .="<td>".$nbsp.$a->menu."</td>";
				}
				foreach($arr_act as $act){
					$check_st2 = '';
					//print_r($this->db->last_query());	
					if(isset($role[$act]) && $role[$act]==1){
							$check_st2 = "checked='checked'";
							//exit($role[$act]);
					}
					//print_r($check_st2);exit;
					$tr .="<td align='center'><input type='checkbox' ".$check_st2." name='data[".$a->const."][".$act."]' id='".$act."' value='1'></td>";
				}
				$tr .="</tr>";
			}
			$tr .= $this->_edit_box($a->id, $id);
		}
		
		return $tr;
	}
	
	public function disp(){
		print_r($this->_edit_box(0,1));exit;
	}
	
	public function save(){
		$data = $this->input->post(null,true);
		$pass = $this->Akses_model->save_privilege($data);
		if($pass)
			redirect('manages/jabatan');
	}
	
	private function _act_alias(){
		
		return array('view','add','edit','remove');
	}
	
}