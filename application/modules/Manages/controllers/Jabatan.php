<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jabatan extends My_Back{

	protected static $modul = "MANAGES_JABATAN";

	protected static $access = [];
	
	public function __construct(){
		
		parent::__construct();

		SELF::$access = $this->general->check_role($this->session->userdata('jabatan_id'), SELF::$modul, $this->access_arr);

		if(!SELF::$access['view'])
			redirect('testing/unAuthorized');

		$this->load->model('Jabatan_model');
	}
	
	public function index(){
		$data = [
			'title' 	=> 'List Jabatan',
			'header'	=> 'List Jabatan',
			'add_role'	=> (SELF::$access['add'])?"":"display:none;",
			'custom_js'	=> ['service'=>'assets/service-js/jabatan/list-jabatan.js']
		];
		
		$this->_nampil('jabatanView', $data);
	}
	
	public function getJabatan(){
		
		$limit  = $this->config->item('per_page');
		$offset = $this->uri->segment(3,0);
		$search = $this->input->get('search');
		$url 	= 'manages/jabatan/getJabatan/';
		
		$data = $this->Jabatan_model->get_data($search, $limit, $offset);
		//print_r($data);exit;
		$total = $data['total'];
		$tr = $paging = '';
		if(!$data['data']){
			
			$tr .="<tr>";
			$tr .="<td colspan='4'>Not Found...</td>";
			$tr .="</tr>";

			$msg = 'Data Not found';
		};
		
		if($data['data']){
			$no = $offset + 1;
			$i = 1;
			foreach($data['data'] as $o){
				$edit_role 	 = (SELF::$access['edit']) ? "" : "display:none;";
				$remove_role = (SELF::$access['remove']) ? "" : "display:none;";

				$tr .="<tr>";
				$tr .="<td>".$no."</td>";

				//button action
				$tr .="<td><div class='btn-group'>";
				$tr .="<button class='btn btn-xs btn-default'>...</button>";
				$tr .="<button data-toggle='dropdown' class='btn btn-xs btn-default dropdown-toggle'><span class='caret'></span></button>";
				$tr .="<ul class='dropdown-menu'>";
				$tr .="<li style='".$edit_role."'><a href='".site_url().'manages/jabatan/detailView/'.$o->jabatan_id."'><i class='fa fa-edit'></i> Edit</a></li>";
				$tr .="<li style='".$remove_role."'><a href='javascript:void(0)' onClick='delete_user(".$o->jabatan_id.")'><i class='fa fa-trash'></i> Delete</a></li>";
				$tr .="<li style='".$edit_role."' class='divider'></li>";
				$tr .="<li style='".$edit_role."'><a href='".site_url().'manages/akses/detail/'.$o->jabatan_id."'><i class='fa fa-wrench'></i> Privilege</a></li>";
				$tr .="</ul></div></td>";
				//end button action
				
				$tr .="<td>".$o->nama_jabatan."</td>";
				$tr .="<td>".$o->deskripsi."</td>";
				$tr .="</tr>";
			$i++;$no++;
			}
			$paging .="<li><span class='page-info'>Displaying ".($i-1)." of ".$total." data</span></li>";
			$paging .= $this->_paging($url,$total,$limit);
			
			$msg = 'Data Loaded...';
		}
		//echo $tr;exit;
		//return $tr;
		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr,'paging'=>$paging));
	}

	public function addView(){
		$data = [
			'title' => 'Add Jabatan',
			'header'=> 'Add New Jabatan',
			'custom_js'	=> ['service'=>'assets/service-js/jabatan/add-jabatan.js']
		];
		
		$this->_nampil('addJabatanView', $data);
	}

	public function addData(){
		
		//print_r($this->input->post(null, true));exit;
		$data = $this->input->post(null, true);
		$query = $this->Jabatan_model->save_data($data);
		
		if(!$query){
			$res = ['status'=>false,'msg'=>'Jabatan Gagal di Tambahkan'];
		}else{
			$res = ['status'=>true,'msg'=>'Jabatan Berhasil di Tambahkan'];
		}
		$this->response($res);
	}

	public function detailView(){
		$data = [
			'id'	=> $this->uri->segment(4,0),
			'title'	=> 'Detail Jabatan',
			'header'=> 'Detail Jabatan',
			'custom_js'	=> ['service'=>'assets/service-js/jabatan/detail-jabatan.js']
		];

		$this->_nampil('detailJabatanView', $data);
	}

	public function getDetail(){
		
		$get = $this->Jabatan_model->get_detail($this->input->get('id'));
		//print_r($get);exit;
		
		if($get){
			$res = ['status'=>true,'msg'=>'Data Loaded...','data'=>$get,'act'=>null];
		}else{
			$res = ['status'=>false,'msg'=>'Data Not Found...','data'=>[],'act'=>null];
		}
		$this->response($res);
	}

	public function updateData(){
		//print_r($this->input->input_stream(null,true));exit;
		$update = $this->Jabatan_model->update_data($this->input->input_stream(null,true));
		if($update){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Ubah'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Ubah'];
		}
		$this->response($res);
	}

	public function deleteJabatan(){

		$id = $this->input->input_stream('id');

		if($this->Jabatan_model->delete_jabatan($id)){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Hapus..'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Hapus..'];
		}
		$this->response($res);
	}
	
}