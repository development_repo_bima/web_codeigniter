<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends My_Back{
	
	protected static $modul = "MANAGES_MENU";

	protected static $access = [];

	public function __construct(){
		
		parent::__construct();

		SELF::$access = $this->general->check_role($this->session->userdata('jabatan_id'), SELF::$modul, $this->access_arr);

		if(!SELF::$access['view'])
			redirect('testing/unAuthorized');

		$this->load->model('Menu_model');
	}
	
	public function index(){
		$data = [
			'title' 	=> 'List Menu',
			'header'	=> 'List Menu',
			'add_role'	=> (SELF::$access['add'])?"":"display:none;",
			'custom_js'	=> array('service'=>'assets/service-js/menu/list-menu.js')
		];
		
		$this->_nampil('menuView', $data);
	}
	
	public function getMenu(){
		
		$limit  = $this->config->item('per_page');
		$offset = $this->uri->segment(3,0);
		$search = $this->input->get('search');
		$url 	= 'manages/menu/getMenu/';
		
		$data = $this->Menu_model->get_data($search, $limit, $offset);
		
		$total = $data['total'];
		$tr = $paging = '';
		if(!$data['data']){
			
			$tr .="<tr>";
			$tr .="<td colspan='4'>Not Found...</td>";
			$tr .="</tr>";

			$msg = 'Data Not found';
		};
		
		if($data['data']){
			$no = $offset + 1;
			$i = 1;
			
			foreach($data['data'] as $o){
				
				$edit_role 	 = (SELF::$access['edit']) ? "" : "display:none;";
				$remove_role = (SELF::$access['remove']) ? "" : "display:none;";

				$head_menu = ($o->induk === '1') ? 'Head Menu' : '';
				
				$tr .="<tr>";
				$tr .="<td>".$no."</td>";

				//button action
				$tr .="<td><div class='btn-group'>";
				$tr .="<button class='btn btn-xs btn-default'>...</button>";
				$tr .="<button data-toggle='dropdown' class='btn btn-xs btn-default dropdown-toggle'><span class='caret'></span></button>";
				$tr .="<ul class='dropdown-menu'>";
				$tr .="<li style='".$edit_role."'><a href='".site_url().'manages/menu/detailView/'.$o->id."'><i class='fa fa-edit'></i> Edit</a></li>";
				$tr .="<li style='".$remove_role."'><a href='javascript:void(0)' onClick='delete_menu(".$o->id.")'><i class='fa fa-trash'></i> Delete</a></li>";
				$tr .="</ul></div></td>";
				//end button action
				
				$tr .="<td>".$o->menu."</td>";
				$tr .="<td>".$o->icon."</td>";
				$tr .="<td>".$o->url."</td>";
				$tr .="<td>".$head_menu."</td>";
				$tr .="</tr>";
			$i++;$no++;
			}
			$paging .="<li><span class='page-info'>Displaying ".($i-1)." of ".$total." data</span></li>";
			$paging .= $this->_paging($url,$total,$limit);
			
			$msg = 'Data Loaded...';
		}
		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr,'paging'=>$paging));
	}

	public function addView(){
		$data = [
			'title' => 'Add Menu',
			'header'=> 'Add New Menu',
			'custom_js'	=> array('service'=>'assets/service-js/menu/add-menu.js')
		];
		
		$this->_nampil('addMenuView', $data);
	}
	
	public function addData(){
		
		//print_r($this->input->post(null, true));exit;
		$data = $this->input->post(null, true);
		$query = $this->Menu_model->save_data($data);
		
		if(!$query){
			$res = ['status'=>false,'msg'=>'Menu Gagal di Tambahkan'];
		}else{
			$res = ['status'=>true,'msg'=>'Menu Berhasil di Tambahkan'];
		}
		$this->response($res);
	}
	
	public function detailView(){
		$data = [
			'id'	=> $this->uri->segment(4),
			'title' => 'Detail Menu',
			'header'=> 'Detail Menu',
			'custom_js'	=> array('service'=>'assets/service-js/menu/detail-menu.js')
		];
		
		$this->_nampil('detailMenuView', $data);
	}
	
	public function getDetail(){
		
		$get = $this->Menu_model->get_detail($this->input->get('id'));
		//print_r($get);exit;
		
		if($get){
			$res = ['status'=>true,'msg'=>'Data Loaded...','data'=>$get,'act'=>null];
		}else{
			$res = ['status'=>false,'msg'=>'Data Not Found...','data'=>[],'act'=>null];
		}
		$this->response($res);
	}
	
	public function updateData(){
		//print_r($this->input->input_stream(null,true));exit;
		$update = $this->Menu_model->update_data($this->input->input_stream(null,true));
		if($update){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Ubah'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Ubah'];
		}
		$this->response($res);
	}

	public function deleteMenu(){

		$id = $this->input->input_stream('id');

		if($this->Menu_model->delete_menu($id)){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Hapus..'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Hapus..'];
		}
		$this->response($res);
	}
	
	/*
	* function getParent
	* digunakan untuk menampilkan input select berdasarkan parent Id
	* return DOM Element
	*/
	public function getParent(){
		
		$query = $this->db->get_where('m_menu', array('induk'=>1))->result_array();
		$sel = '';
		$sel .="<select class='form-control' name='parent_id' required id='select-parent-form'><option value=''>--Select Parent Name--</option>";
		foreach($query as $a){
			
				$sel .="<option value='".$a['id']."'>".$a['menu']."</option>";
		}
		$sel .="</select>";
		//print_r($sel);exit;
		$this->response(array('data'=>$sel));
	}
	
}