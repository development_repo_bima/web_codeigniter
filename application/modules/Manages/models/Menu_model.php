<?php 

class Menu_model extends CI_Model{
	
	public function get_data($search=null, $limit=0, $offset=0){
		//print_r($offset);exit;
		$this->db->select('*')->from('m_menu')->where('flag',0);
			if($search)
				$this->db->like('menu', $search);
		
		$this->db->order_by('id', 'DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result();
		$data['total'] = $this->db->get_where('m_menu', ['flag'=>0])->num_rows();
		
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}
	
	public function get_detail($id){
		
		$detail = $this->db->select('const,menu,icon,url,parent,induk')->get_where('m_menu',array('id'=>$id))->row_array();
		if($detail){
			$parent = $this->db->select('menu')->get_where('m_menu',array('id'=>$detail['parent']))->row_array();
			$detail['parents'] = $parent['menu']; 
		}
		//$this->db->select('a.const,a.menu,a.icon,a.url,a.parent,a.induk')
		//print_r($this->db->last_query());exit;
		return $detail;
	}

	public function save_data($data){
		
		$save = [
			'const'	=> $data['init_menu'],
			'menu'	=> $data['menu_name'],
			'icon'	=> $data['menu_icon'],
			'url'	=> $data['menu_url'],
			'parent'=> ($data['parent_id'] != '') ? $data['parent_id'] : 0,
			'induk' => isset($data['is_parent']) ? $data['is_parent'] : 0
		];

		if($data['parent_id'] == '' && !isset($data['is_parent']))
			$save['induk'] = 1; 
		
		//print_r($save);exit;
		
		$this->db->insert('m_menu', $save);
		return true;
	}
	
	public function update_data($data){
		//print_r($data);exit;
		$update = [
				'menu'	=>$data['menu_name'],
				'icon'	=>$data['menu_icon'],
				'url'	=>$data['menu_url'],
				'parent'=>isset($data['parent_id']) ? (($data['parent_id']=='')? $data['parent_default'] : $data['parent_id']) : 0,
				'induk' =>isset($data['is_parent']) ? $data['is_parent'] : 0
		];
		//custom condition, a bit messy in here..
		if(isset($data['parent_id'])){
			if($data['parent_default']==0 && $data['parent_id'] == '' && !isset($data['is_parent'])){
				$update['induk'] = 1;
			}
		}
		//print_r($update);die;
		return $this->db->update('m_menu',$update,array('id'=>$data['id']));
	}
	
	public function delete_menu($id){
		
		return $this->db->delete('m_menu', array('id'=>$id));
	}
}