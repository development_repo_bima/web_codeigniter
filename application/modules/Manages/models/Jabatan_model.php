<?php 

class Jabatan_model extends CI_Model{
	
	public function get_data($search=null, $limit=0, $offset=0){
		//print_r($offset);exit;
		$this->db->select('jabatan_id, nama_jabatan, deskripsi')->from('m_jabatan');
			if($search)
				$this->db->like('nama_jabatan', $search);
		
		$this->db->order_by('jabatan_id', 'ASC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result();
		$data['total'] = $this->db->count_all('m_jabatan');
		
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}
	
	public function get_detail($id){
		
		return $this->db->get_where('m_jabatan',array('jabatan_id'=>$id))->row_array();
	}

	public function save_data($data){
	
		//print_r($data);exit;
		$save = [
			'nama_jabatan'=>$data['jab_name'],
			'deskripsi'   =>$data['jab_desc']
			//'created_at'=>date('Y-m-d')
		];
		
		$this->db->insert('m_jabatan', $save);
		return true;
		//print_r($save);exit;
	}
	
	public function update_data($data){
		//print_r($data);exit;
		$update = [
				'nama_jabatan'	=>$data['jabatan'],
				'deskripsi'		=>$data['keterangan']
		];
		return $this->db->update('m_jabatan',$update,array('jabatan_id'=>$data['id']));
	}
	
	public function delete_jabatan($id){
		
		return $this->db->delete('m_jabatan', array('jabatan_id'=>$id));
	}
}