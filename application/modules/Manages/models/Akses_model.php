<?php 

class Akses_model extends CI_Model{
	
	public function exist_access($id){
		return $this->db->limit(1)->get_where('m_akses', array('jabatan_id'=>$id))->num_rows();
	}

	public function get_role($modul, $id){
		
		$q = "SELECT * FROM m_akses WHERE modul = '%s' AND jabatan_id = %d";
		$query = $this->db->query(sprintf($q,$modul,$id));
		if($query->num_rows()>0){
			//print_r($query->row_array());exit;
			return $query->row_array();
		}else{
			return $query = array('view'=>'','add'=>'','edit'=>'','remove'=>'');
		}
	}
	
	public function save_privilege($data){
		//print_r($data['data']);print_r($data['jabatan_id']);exit;
		foreach($data['data'] as $page=>$akses){
			
			if(!isset($akses['view'])) $akses['view']=0;
			if(!isset($akses['add'])) $akses['add']=0;
			if(!isset($akses['edit'])) $akses['edit']=0;
			if(!isset($akses['remove'])) $akses['remove']=0;
			
			$tmp = array(
				'jabatan_id'=> $data['jabatan_id'],
				'modul'		=> $page,
				'view'		=> $akses['view'],
				'add'		=> $akses['add'],
				'edit'		=> $akses['edit'],
				'remove'	=> $akses['remove'],
			);
			$arr[] = $tmp;
			//print_r($tmp);echo "<br>";
		}
		$this->db->trans_begin();
			$this->db->delete('m_akses',array('jabatan_id' => $data['jabatan_id']));
			$this->db->insert_batch('m_akses', array_merge($arr));
		if($this->db->trans_status()===false){
		    
		    $this->db->trans_rollback();
		    return false;
		}else{
		    
		    $this->db->trans_commit();
		    return true;
		}
		//exit;
	}
}