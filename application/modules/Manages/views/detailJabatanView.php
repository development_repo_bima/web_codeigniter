<!-- page content -->
      <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row tile_count">
        </div>
        <!-- /top tiles -->
		
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
			<legend><?php echo isset($header) ? $header : '';?></legend>
				<br>
				<div id="detail-view" class="col-md-6">
					<table class="table data-detail">
						<tbody>
							<tr>
								<td width="30%">Nama Jabatan</td>
								<td width="3%">:</td>
								<td width="60%"><p id="nama-jabatan"></p></td>
							</tr>
							<tr>
								<td>Deskripsi</td>
								<td>:</td>
								<td><p id="ket-jabatan"></p></td>
							</tr>
						</tbody>
					</table>
					<button id="edit-button" class="btn btn-success">Edit</button>
					<button id="cancel-button" onClick="cancel()" class="btn btn-danger">Cancel</button>
				</div>
				<div id="edit-view" style="display:none;">
					<form class="form-horizontal" id="form-edit" method="post">
						<fieldset>
							<div class="form-group">
							  <div class="col-md-6">
								<div class="form-group select-form">
								  <label>Nama Jabatan</label>
								  <input id="jabform-nama" name="jabatan_name" class="form-control" placeholder="Jabatan name here" required="" type="text">
								</div>
								<div class="form-group select-form">
								  <label>Deskripsi</label>
								  <textarea class="form-control" id="jabform-ket" name="deskripsi">Deskripsi</textarea>
								</div>
								<button id="save-edit" class="btn btn-success">Save</button>
								<a id="cancel" onClick="cancel()" class="btn btn-danger">Cancel</a>
							  </div>
						</div>
						</fieldset>
					</form>
				</div>
				
              <div class="clearfix"></div>
            </div>
          </div>

        </div>
		<script type="text/javascript">
			var id = <?php echo $id;?>;
			
		</script>