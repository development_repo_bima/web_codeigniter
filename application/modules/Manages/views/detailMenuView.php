<!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
		
			

        </div>
        <!-- /top tiles -->
		
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
			<legend><?php echo isset($header) ? $header : '';?></legend>
				<br>
				<div class="col-md-6" id="detail-view">
					<table class="table data-detail">
						<tbody>
							<tr>
								<td width="30%">Initial Menu</td>
								<td width="3%">:</td>
								<td width="60%"><p id="init-menu"></p></td>
							</tr>
							<tr>
								<td>Menu Name</td>
								<td>:</td>
								<td><p id="menu-name"></p></td>
							</tr>
							<tr>
								<td>Icon</td>
								<td>:</td>
								<td><p id="icon-menu"></p></td>
							</tr>
							<tr>
								<td>URL</td>
								<td>:</td>
								<td><p id="url-menu"></p></td>
							</tr>
							<tr>
								<td>Parent Menu</td>
								<td>:</td>
								<td><p id="parent-id"></p></td>
							</tr>
							<tr>
								<td>Is Parent</td>
								<td>:</td>
								<td><p id="is-parent"></p></td>
							</tr>
						</tbody>
					</table>
					<button id="edit-button" class="btn btn-success">Edit</button>
					<button id="cancel-button" onClick="cancel()" class="btn btn-danger">Cancel</button>
				</div>
				<div id="edit-view" class="col-md-6" style="display:none;">
					<form class="form-horizontal" id="form-edit" method="post">
						<fieldset>
							<div class="form-group">
							  <div class="col-md-8">
								<div class="form-group select-form">
								  <label>Menu Name</label>
								  <input id="menuform-menu" name="menu_name" class="form-control" placeholder="Menu name here" required="" type="text">
								</div>
								<div class="form-group select-form">
								  <label>Icon Code</label>
								  <input id="menuform-icon" name="menu_icon" class="form-control" placeholder="Icon code here" required="" type="text">
								</div>
								<div class="form-group select-form">
								  <label>URL Menu</label>
								  <input id="menuform-url" name="menu_url" class="form-control" placeholder="URL menu here" required="" type="text">
								</div>
								<div class="form-group select-form">
									<label>Menu Type</label><small class="pull-right" style="color:red;">As Parent Menu? <input type="checkbox" id="is-parents" name="is_parent" value="1" onClick="asParentForm()"></small>
									<span id="parent-menu"></span>
									<input id="menuform-parent" name="parent_default" type="hidden">
								</div>
								<button id="save-edit" class="btn btn-success">Save</button>
								<a id="cancel" onClick="cancel()" class="btn btn-danger">Cancel</a>
							  </div>
						</div>
						</fieldset>
						<!-- Button -->
					</form>
				</div>
				
              <div class="clearfix"></div>
            </div>
          </div>

        </div>
		<script type="text/javascript">
			var id = <?php echo isset($id) ? $id : 0;?>;
		</script>