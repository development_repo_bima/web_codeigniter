<div class="right_col" role="main">
	<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph" id="add-form">
				<div class="col-md-6">
					<form class="form-horizontal" id="form-menu" method="post">
					<fieldset>

					<!-- Form Name -->
					<legend><?php echo isset($header) ? $header : '';?></legend>
					
					<div class="form-group ">
						  <label>Initial Menu</label>
						  <input id="init-menu" name="init_menu" class="form-control" placeholder="Initial Name" required="" type="text">
					</div>
					
					<!-- Prepended text-->
					<div class="form-group ">
					  <label>Nama Menu</label>
					  <input id="menu-name" name="menu_name" class="form-control" placeholder="Menu Name" required="" type="text">
					</div>
					
					<div class="form-group ">
					  <label>Menu Icon</label>
					  <input id="icon" name="menu_icon" class="form-control" placeholder="Icon code" required="" type="text">
					</div>
					
					<div class="form-group ">
					  <label>Path URL</label>
					  <input id="url" name="menu_url" class="form-control" placeholder="Url Path" required="" type="text">
					</div>
					
					<div class="form-group ">
						<label>Parent Menu?</label>
						<input id="parent-status" value="1" type="checkbox" name="is_parent"/>
					</div>
					
					<div class="form-group">
						<span id="child"></span>
					</div>

					<!-- Button -->
					<div class="form-group">
						<button id="submitted" name="submitted" class="btn btn-success">Save</button>
						<a id="cancelled" name="cancel" onClick="cancel()" class="btn btn-danger">Cancel</a>
					</div>

					</fieldset>
					</form>
				
				<div class="clearfix"></div>
				</div>
            </div>
          </div>

        </div>
		<script type="text/javascript">
		
		
		</script>