<div class="right_col" role="main">
	<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph" id="add-form">
				<form class="form-horizontal" id="form-jabatan" method="post">
				<fieldset>

				<!-- Form Name -->
				<legend><?php echo isset($header) ? $header : '';?></legend>
				
				<!-- Prepended text-->
				<div class="form-group ">
				  <div class="col-md-8 add-new">
					  <label>Nama Jabatan</label>
					  <input id="jabform-name" name="jab_name" class="form-control" placeholder="nama jabatan here" required="" type="text">
				  </div>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <div class="col-md-8 add-new"> 
				  	<label>Deskripsi</label>                    
					<textarea class="form-control" id="jabform-desc" name="jab_desc">Deskripsi</textarea>
				  </div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-8">
					<button id="submitted" name="submitted" class="btn btn-success" disabled="true">Save</button>
					<button id="cancelled" name="cancel" class="btn btn-danger" onclick="window.history.back()">Cancel</button>
				  </div>
				</div>

				</fieldset>
				</form>
				<div class="clearfix"></div>
            </div>
          </div>

        </div>
		<script type="text/javascript">
		
		
		</script>