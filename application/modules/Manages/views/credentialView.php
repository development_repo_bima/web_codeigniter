<div class="right_col" role="main">
	<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph" id="add-form">
				<form id="credential-form" role="form" action="<?php echo site_url(); ?>manages/akses/save" method="POST">
					<input type="hidden" name="jabatan_id" id="id" value="<?php echo $jabatan_id; ?>">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th style="text-align:center;" width="60%">Nama Modul<br><span></span></th>
								<th style="text-align:center;">View <br><input type="checkbox" name="checkboxes" id="view_all" onClick="checkAll('view', true)" value="1"></th>
								<th style="text-align:center;">Add <br><input type="checkbox" name="checkboxes" id="add_all" onClick="checkAll('add', true)" value="1"></th>
								<th style="text-align:center;">Edit <br><input type="checkbox" name="checkboxes" id="edit_all" onClick="checkAll('edit', true)" value="1"></th>
								<th style="text-align:center;">Delete <br><input type="checkbox" name="checkboxes" onClick="checkAll('remove', true)" value="1"></th>
							</tr>
						</thead>
						<tbody>
							<?php echo $tr; ?>
							<!-- <tr>
								<td>User</td>
								<td><input type="checkbox" name="checkboxes['view']" id="view" value="1"></td>
								<td><input type="checkbox" name="checkboxes['add']" id="add" value="1"></td>
								<td><input type="checkbox" name="checkboxes['edit']" id="edit" value="1"></td>
								<td><input type="checkbox" name="checkboxes['remove']" id="remove" value="1"></td>
							</tr>
							<!-- <tr>
								<td>&nbsp;Upload</td>
								<td><input type="checkbox" name="checkboxes" id="view" value="1"></td>
								<td><input type="checkbox" name="checkboxes" id="add" value="1"></td>
								<td><input type="checkbox" name="checkboxes" id="edit" value="1"></td>
								<td><input type="checkbox" name="checkboxes" id="remove" value="1"></td>
							</tr>
							<tr>
								<td>Credential</td>
								<td><input type="checkbox" name="checkboxes" id="view" value="1"></td>
								<td><input type="checkbox" name="checkboxes" id="add" value="1"></td>
								<td><input type="checkbox" name="checkboxes" id="edit" value="1"></td>
								<td><input type="checkbox" name="checkboxes" id="remove" value="1"></td>
							</tr> -->
							</tbody>
						</table>
						<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                        <a href="<?php echo site_url();?>manages/jabatan" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
				</form>
				<div class="clearfix"></div>
            </div>
          </div>

        </div>
		<script type="text/javascript">
		
		$(document).ready(function(){
			
			
		});
		//function check all in same checkbox
		function checkAll(type, status){
			//get checkbox data on form
			var n = document.forms['credential-form'];
			//count length
			var len = n.elements.length;
			//looping
			for(i=0;i<len;i++){
				//if met the match condition check all checkbox
				if(n.elements[i] && n.elements[i].type == 'checkbox'){
					if(n.elements[i].id==type)
						n.elements[i].checked=status;
				}
			} 
		}
		
		</script>