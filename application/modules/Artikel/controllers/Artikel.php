<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends My_Back {
	
	protected static $modul = "LIST_ARTIKEL";

	protected static $access = [];

	public function __construct(){
		
		parent::__construct();

		SELF::$access = $this->general->check_role($this->session->userdata('jabatan_id'), SELF::$modul, $this->access_arr);
		if(!SELF::$access['view'])
			redirect('testing/unAuthorized');

		$this->load->model('Artikel_model');
	}

	public function index(){
		
		$data = [
			'title' 	=> 'List Artikel',
			'header'	=> 'List Artikel',
			'add_role'	=> (SELF::$access['add']) ? "" : "display:none;",
			'custom_js'	=> array('service'=>'assets/service-js/artikel/list-artikel.js')
		];
		
		$this->_nampil('artikelView', $data);
	}
	
	public function getData(){
		
		$limit  = $this->config->item('per_page');
		$offset = $this->uri->segment(3,0);
		$search = $this->input->get('search');
		$url 	= 'artikel/getData/';
		
		$data = $this->Artikel_model->get_data($search, $limit, $offset);
		//print_r($data);exit;
		$total = $data['total'];
		$tr = $paging = '';
		if(!$data['data']){
			
			$tr .="<tr>";
			$tr .="<td colspan='7'>Empty Data...</td>";
			$tr .="</tr>";

			$msg = 'Empty Data';
		};
		
		if($data['data']){
			$no = $offset + 1;
			
			$i = 1;
			foreach($data['data'] as $o){
				
				//styling button section, hope better way's. but now is fine
				$status = ($o->status=='0')?'1':'0';
				$active = ($o->status=='1')? "<i class='fa fa-ban'></i> Deactivate" : "<i class='fa fa-check'></i> Activate" ;
				$edit_role 	 = (SELF::$access['edit']) ? "" : "display:none;";
				$remove_role = (SELF::$access['remove']) ? "" : "display:none;";
				
				$tr .="<tr>";
				//button action
				if(!SELF::$access['edit'] && !SELF::$access['edit']){
					$tr .="<td>No Privileges</td>";
				}else{
					$tr .="<td><div class='btn-group'>";
					$tr .="<button class='btn btn-xs btn-default'>...</button>";
					$tr .="<button data-toggle='dropdown' class='btn btn-xs btn-default dropdown-toggle' aria-expanded='false'><span class='caret'></span></button>";
					$tr .="<ul class='dropdown-menu' role='menu'>";
					$tr .="<li style='".$edit_role."'><a href='".site_url().'artikel/detailView/'.$o->uid.'/'.urlencode($o->title)."'><i class='fa fa-edit'></i> Preview</a></li>";
					$tr .="<li style='".$edit_role."'><a href='javascript:void(0)' onClick='activated_artikel(".$o->id.','.$status.")'>".$active."</a></li>";
					$tr .="<li style='".$remove_role."'><a href='javascript:void(0)' onClick='delete_artikel(".$o->id.")'><i class='fa fa-trash'></i> Delete</a></li>";
					$tr .="</ul></div></td>";
				}
				//end button action
				$tr .="<td>".$no."</td>";
				$tr .="<td>".$o->title."</td>";
				$tr .="<td>".$o->author."</td>";
				$tr .="<td>".$o->name."</td>";
				$tr .="<td>".(($o->status == '0') ? 'Not Showed' : 'Showed')."</td>";
				$tr .="</tr>";
			$i++;$no++;
			}
			$paging .="<li><span class='page-info'>Displaying ".($i-1)." of ".$total." data</span></li>";
			$paging .= $this->_paging($url,$total,$limit);
			
			$msg = 'Data Loaded...';
		}

		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr,'paging'=>$paging));
		
	}
	
	public function addView(){
		$data = [
			'title' 	=> 'Add Artikel',
			'header'	=> 'Add New Artikel',
			'custom_css'=> array(
								'customs'=>'assets/summernote/summernote.css',
								'buttons'=> 'assets/css/button.css'
							),
			'custom_js'	=> array(
								'service'=>'assets/service-js/artikel/add-artikel.js',
								'customs'=>'assets/summernote/summernote.js',
								'form'=>'assets/js/jquery.form.js'
							)
		];
		
		$this->_nampil('addArtikelView', $data);
	}
	//this function for test html only.. used it wisely
	public function htmlTest(){
		$data = [
			'custom_css' => array('buttons'=> 'assets/css/button.css')
		];

		$this->_nampil('examples',$data);
	}
	
	public function detailView(){
		$data = [
			'id'	=> $this->uri->segment(3),
			'title' => 'Preview Artikel',
			'header'=> 'Preview Artikel',
			'custom_css'=> ['customs'=>'assets/summernote/summernote.css'],
			'custom_js'	=> [
								'service'=>'assets/service-js/artikel/detail-artikel.js',
								'customs'=>'assets/summernote/summernote.js',
								'form'=>'assets/js/jquery.form.js'
							]
		];
		
		$this->_nampil('detailArtikelView', $data);
	}
	
	public function getDetail(){
		
		$get = $this->Artikel_model->get_detail($this->input->get('id'));
		if($get){
			$res = ['status'=>true, 'msg'=>'Data Loaded...', 'data'=>$get];
		}else{
			$res = ['status'=>false,'msg'=>'Data Not Found...','data'=>[]];
		}
		$this->response($res);
	}
	
	public function addData(){
		
		$data = $this->input->post(null, true);
		//print_r($data);exit;
		
		if($data['title_artikel'] == ''){
			$res = array('status'=>false, 'msg'=>'silahkan isi form title!');
		}else{ 
			$this->_config_upload();
			if(!$this->upload->do_upload()){
				//echo "gagal";exit;
				$res = array('status'=>false, 'msg'=>$this->upload->display_errors());
			}else{
				//print_r($_FILES['userfile']);exit;
				$det = $this->upload->data();
				$data = array_merge($data,$det);
				//print_r($data);die;
				$query = $this->Artikel_model->save_data($data);
				if($query){
					$res = array('status'=>true, 'msg'=>'Artikel di simpan..');
				}else{
					@unlink($det['file_path'].$det['file_name']);
					$res = array('status'=>false, 'msg'=>'Terjadi Kesalahan saat menyimpan file..');
				}
			}
		}
		$this->response($res);
	}
	
	public function updateData(){
		//print_r($this->input->input_stream(null,true));exit;
// 		$this->_config_upload();
		//print_r($this->input->post(null,true));die;
		$update = $this->Artikel_model->update_data($this->input->post(null,true));
		if($update){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Ubah'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Ubah'];
		}
		$this->response($res);
	}
	
	public function activatedArtikel(){
		
		$data = $this->input->input_stream(null,true);
		//print_r($data);die;
		if($this->Artikel_model->publish_artikel($data)){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Update..'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Update..'];
		}
		$this->response($res);
	}
	
	public function deleteArtikel(){
		
		$id = $this->input->input_stream('id');
		//die($id);
		if($this->Artikel_model->delete_artikel($id)){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Hapus..'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Hapus..'];
		}
		$this->response($res);
	}
	
	public function getCategory(){
		
		$query = $this->db->get('m_category')->result_array();
		$sel = '';
		$sel .="<select class='form-control' name='select_cat' required id='select-form'><option value=''>--Select Kategori Artikel--</option>";
		foreach($query as $a){
			$sel .="<option value='".$a['category_id']."'>".$a['name']."</option>";
		}
		$sel .="</select>";
		//print_r($sel);exit;
		$this->response(array('data'=>$sel));
	}
	
	// upload config, for beauty code this function must be separated. but okay for now :D
	private function _config_upload(){
	
		$config = array(
				'file_name'		=> $_FILES['userfile']['name'],
				'upload_path' 	=> './assets/files',
				'allowed_types'	=> 'jpeg|jpg|png',
				'max_size'		=> 1024,
				'encrypt_name'	=> false,
				'overwrite'		=> false,
				'remove_spaces'	=> true
		);
	
		$this->load->library('upload', $config);
	}
	
	private function _check_in($filename, $title){
	
		$name = str_replace(' ','_',$title.','.$filename);
	
		return $this->db->get_where('m_files',array('filename'=>$name))->row();
	
	}

	// for summernote
	/*
	 * summernote auto upload media
	 * when user hit insert image in summernote, save the image to dir and get path
	*/
	public function upload_img(){
		if(empty($_FILES['file']))
			exit();	

		$errorImgFile = base_url('assets/images/img_upload_error.jpg');
		$destinationFilePath = './assets/img_article/'.$_FILES['file']['name'];
		if(!move_uploaded_file($_FILES['file']['tmp_name'], $destinationFilePath)){
			echo $errorImgFile;
		}
		else{
			echo base_url($destinationFilePath);
		}
	}
	
}
