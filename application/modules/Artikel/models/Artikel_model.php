<?php 

class Artikel_model extends CI_model{
	
	public function get_data($search=null, $limit=0, $offset=0){
		//print_r($offset);exit;
		$this->db->select('a.*, c.initial, c.name')
					->from('t_artikel as a')
					->join('m_category as c','c.category_id = a.category','left');
					//->join('m_akses as a','a.jabatan_id = j.jabatan_id','left');
			if($search){
				$this->db->like('title', $search);
				$this->db->or_like('content', $search);
			}
		$this->db->order_by('id', 'DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result();
		$data['total'] = $this->db->count_all('t_artikel');
		
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}
	
	public function get_detail($id){
		
		//print_r($this->db->last_query());exit;
		//active record ways still get errors. how it's work??
		/* return $this->db->query("SELECT u.username,u.email,u.jabatan_id, j.nama_jabatan 
									FROM m_user u 
									LEFT JOIN m_jabatan j ON j.jabatan_id = u.jabatan_id 
									WHERE u.id = {$id}")->row_array(); */
		return $this->db->select('content, title, image')->from('t_artikel')->where(array('uid'=>$id))->get()->row_array();
	}

	public function save_data($data){
		//print_r($data);die;
	
		$save = [
			'uid'		=> uniqid(mt_rand()),
			'title'		=> $data['title_artikel'],
			'content'	=> $data['contents_save'],
			'source'	=> $data['source'],
			'url_source'=> $data['source_url'],
			'author'	=> $this->session->userdata('username'),
			'category'	=> $data['select_cat'],
			'image'		=> isset($data['file_name']) ? $data['file_name'] : '',
			'created_at'=> date('Y-m-d H:i:s'),
			'status'	=> 0
			//'created_at'=>date('Y-m-d')
		];
		//print_r($save);exit;
		return $this->db->insert('t_artikel', $save);
		
	}
	
	public function update_data($data){
		//print_r($data);exit;
			
		$update = [
				'title'	 =>$data['edit_title'],
				'content'=>$data['edit_artikel']
		];
		
		return $this->db->update('t_artikel',$update,array('uid'=>$data['uid']));

		//print_r($this->db->last_query());die;
	}

	public function publish_artikel($data){
		
		return $this->db->update('t_artikel',array('status'=>$data['status']), array('id'=>$data['id']));
	}
	
	public function delete_artikel($id){
		
		return $this->db->delete('t_artikel', array('id'=>$id));
	}
	
}