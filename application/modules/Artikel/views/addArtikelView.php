<div class="right_col" role="main">
	<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph" id="add-form">
			
				<form class="form-horizontal" id="form-artikel" method="post">
				<fieldset>

				<!-- Form Name -->
				<legend><?php echo isset($header) ? $header : '';?></legend>
				
				<div class="form-group col-md-12">
				  <div class="col-md-6">
					<div class="">
					  <label>Title<small>*</small></label>
					  <input id="title" name="title_artikel" class="form-control" placeholder="Create title.." required="" type="text">
					</div>
				  </div>
				  <div class="col-md-6">
					<div class="select-form">
						<label>Kategori Artikel<small>*</small></label>
						<span id="fuchs"></span>
					</div>
				  </div>
				</div>
				<div class="form-group col-md-12">
				  <div class="col-md-6">
					<div class="input-group">
					  <label class="input-group">image<small>*</small></label>
					  <input id="userfile" name="userfile" class="form-control" type="file">
					  <label class="input-group">News Source</label>
					  <input id="source" name="source" class="form-control" type="text">
					  <label class="input-group">URL Source</label>
					  <input id="source-url" name="source_url" class="form-control" type="text">
					</div>
				  </div>
				  <div class="col-md-6">
					<div class="well">
					  <label class="input-group"><i class="fa fa-info-circle"></i> How to use the editor</label>
					  <ol>
					  	<li>Slide on to added contents</li>
					  	<li>When finished, slide off to save content</li>
					  </ol>
					  <span class="pull-right"><small>Slide this button</small> <i class="fa fa-hand-o-down"></i></span>
					</div>
				  </div>
				</div>
				<div class="form-group col-md-12">
					<div class="input-group artic col-md-12">
					  <label>Contents<small>*</small></label>
					  <span class="material-switch pull-right">
                            <input id="switch-editor" name="switch_summernote" type="checkbox" />
                            <label for="switch-editor" id="switch-label" class="label-info" title="Slide It to add contents"></label>
                      </span>
					  <textarea class="summernote" id="contents" name="content_artikel" disabled><p><b>Create your Article..</b></p></textarea>
					  <textarea style="display:none;" name="contents_save" id="container-editor"></textarea>
					</div>
				</div>

				<!-- Button -->
				<div class="form-group col-md-12">
				  <div class="col-md-12">
					<button id="submitted" name="submitted" class="btn btn-success">Save</button>
					<button id="cancelled" onClick="history.back();" name="cancel" class="btn btn-danger">Cancel</button>
				  </div>
				</div>

				</fieldset>
				</form>
				<div class="clearfix"></div>
            </div>
          </div>

        </div>