<!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
		
			

        </div>
        <!-- /top tiles -->
		
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
			<legend id="header-tuts"><?php echo isset($header) ? $header : '';?></legend>
				<br>
				<div id="detail-view" class="col-md-12 col-sm-12 col-xs-12">
				
					<h3 id="title-artikel" align="center"></h3>
					<p id="images-content"></p>
					<div class="jumbotron" id="content-artikel"></div>
					
					<button id="edit-button" class="btn btn-success">Edit</button>
					<a id="cancel-button" onClick="cancel()" class="btn btn-danger">Cancel</a>
				</div>
				<div id="edit-view" style="display:none;">
					<form class="form-horizontal" id="form-edit" method="post">
						<fieldset>
						</fieldset>
						<!-- Button -->
						<div class="form-group">
						  <div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group select-form">
							  <label>Title</label>
							  <input id="title-edit" name="edit_title" class="form-control" placeholder="Title here" required="" type="text">
							  <input id="uid" name="uid" class="form-control" required="" type="hidden">
							</div>
							<div class="form-group select-form">
							  <label>Content</label>
							  <textarea name="edit_artikel" class="summernote" required=""></textarea>
							</div>
							<button id="save-edit" class="btn btn-success">Save</button>
							<a id="cancel" onClick="cancel()" class="btn btn-danger">Cancel</a>
						  </div>
						</div>
					</form>
				</div>
				
              <div class="clearfix"></div>
            </div>
          </div>

        </div>
		<script type="text/javascript">var id = "<?php echo isset($id) ? $id : 0;?>";</script>