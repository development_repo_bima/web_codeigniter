<!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
		
			

        </div>
        <!-- /top tiles -->

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
			<legend><?php echo isset($header) ? $header : '';?></legend>
				<form role="form" class="form-inline" id="search" method="post">
					<div class="pull-right input-group">
						
						<span class="input-group-btn">
							<a href="<?php echo site_url().'artikel/addView';?>" title="Add New Artikel" style="<?php echo isset($add_role)?$add_role:''; ?>" id="tambah" name="tambah" class="btn btn-info"><span class=" glyphicon glyphicon-plus"></span></a>
						</span>

	                    <div class="input-group col-xs-12" id="custom-search-input">
		                    <input type="text" id="input-search" name="search" class="form-control input-md" placeholder="Search" />
		                    <span class="input-group-btn">
		                        <button  class="btn btn-info btn-md" id="cari" title="search" type="button">
		                            <i class="glyphicon glyphicon-search"></i>
		                        </button>
		                        <button style="display:none;" class="btn btn-info btn-md" id="clear" title="clear" type="button">
		                            <i class="glyphicon glyphicon-remove"></i>
		                        </button>
		                    </span>
		                </div>
                        <!-- <input type="text" id="input-search" name="search" class="search-query form-control" placeholder="Search" />
                        <span class="input-group-btn">
                        	<button style="display:none;" id="cari" title="Search" name="cari" class="btn btn-success"><span class=" glyphicon glyphicon-search"></span></button>
                        	<button id="clear" title="Clear Search" class="btn btn-danger"><span class=" glyphicon glyphicon-remove"></span></button>
                        </span> -->
                    </div>
				</form>
				<br>
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-condensed" id="data-table">
					<thead>
						<tr>
							<th>Action</th>
							<th>#</th>
							<th>Title</th>
							<th>Author</th>
							<th>Category</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			  </div>
              <div class="pagination pull-right">
				  <ul class="pagination">
				  </ul>
			  </div>

              <div class="clearfix"></div>
            </div>
          </div>

        </div>
		
		<script type="text/javascript">
			
		</script>
		