

<!-- Container Part -->
<div class="container content">
	<div class="row">
		<div class="col-md-10 col-sm-12 col-xs-12">
			<!-- Blog Grid -->
			<div class="blog-grid margin-bottom-30">
				<h2 class="blog-grid-title-lg" id="title-article">Learning ana Analysing Technology: iMac</h2>
				<div class="overflow-h margin-bottom-10">
					<ul class="blog-grid-info pull-left">
						<li id="authors"></li>
						<li id="create-date">Mar 6, 2015</li>
					</ul>
					<div class="pull-right">
						<!-- Go to www.addthis.com/dashboard to customize your tools -->
						<div class="addthis_sharing_toolbox"></div>
					</div>
				</div>
				<img class="img-responsive" id="image-heading" alt="">
			</div>
			<!-- End Blog Grid -->
			<p id=content></p><br>

			<ul class="source-list">
				<li><strong>Source:</strong> <a id="source"></a></li>
				<li><strong>Author:</strong> <a id="author-below"></a></li>
			</ul>

			<!-- Blog Grid Tagds -->
			<!-- <ul class="blog-grid-tags">
			   <li class="head">Tags</li>
			   <li><a href="#">Hi-Tech</a></li>
			   <li><a href="#">Gadgets</a></li>
			   <li><a href="#">Study</a></li>
			   <li><a href="#">Skills</a></li>
			</ul> -->
			<!-- End Blog Grid Tagds -->

			<!-- Blog Thumb v4 -->
			<!-- <div class="margin-bottom-50">
				<h2 class="title-v4">Related Posts</h2>
				<div class="row margin-bottom-50">
					<div class="col-sm-3 col-xs-6 sm-margin-bottom-30">
						<!-- Blog Thumb v4 -->
						<div class="blog-thumb-v4">
							<div class="blog-thumb-item">
								<img class="img-responsive margin-bottom-10" src="assets/img/blog/img40.jpg" alt="">
								<div class="center-wrap">
									<div class="center-alignCenter">
										<div class="center-body">
											<a href="https://player.vimeo.com/video/74197060" class="fbox-modal fancybox.iframe video-play-btn" title="What will fashion be like in 25 years?">
												<img class="video-play-btn" src="assets/img/icons/video-play.png" alt="">
											</a>
										</div>
									</div>
								</div><!--/end center wrap-->
							</div>
							<h3><a href="blog_single.html">What will fashion be like in 25 years?</a></h3>
						</div>
						<!-- End Blog Thumb v4 -->
					</div>

					<div class="col-sm-3 col-xs-6 sm-margin-bottom-30">
						<!-- Blog Thumb v4 -->
						<div class="blog-thumb-v4">
							<img class="img-responsive margin-bottom-10" src="assets/img/blog/img35.jpg" alt="">
							<h3><a href="blog_single.html">Where will be your next destination</a></h3>
						</div>
						<!-- End Blog Thumb v4 -->
					</div>

					<div class="col-sm-3 col-xs-6">
						<!-- Blog Thumb v4 -->
						<div class="blog-thumb-v4">
							<div class="blog-thumb-item">
								<img class="img-responsive margin-bottom-10" src="assets/img/blog/img43.jpg" alt="">
								<div class="center-wrap">
									<div class="center-alignCenter">
										<div class="center-body">
											<a href="https://player.vimeo.com/video/74197060" class="fbox-modal fancybox.iframe video-play-btn" title="Suffering from gastritis? Here's the solution ....">
												<img class="video-play-btn" src="assets/img/icons/video-play.png" alt="">
											</a>
										</div>
									</div>
								</div><!--/end center wrap-->
							</div>
							<h3><a href="blog_single.html">Suffering from gastritis? Here's the solution ....</a></h3>
						</div>
						<!-- End Blog Thumb v4 -->
					</div>

					<div class="col-sm-3 col-xs-6">
						<!-- Blog Thumb v4 -->
						<div class="blog-thumb-v4">
							<img class="img-responsive margin-bottom-10" src="assets/img/blog/img41.jpg" alt="">
							<h3><a href="blog_single.html">The places you must visit, before you die</a></h3>
						</div>
						<!-- End Blog Thumb v4 -->
					</div>
				</div><!--/end row-->
			</div> -->
			<!-- End Blog Thumb v4 -->

			<!-- Blog Comments v2 -->
			<!-- <div class="margin-bottom-50">
				<h2 class="title-v4">Comments (3)</h2>

				<!-- Blog Comments v2 -->
				<div class="row blog-comments-v2">
					<div class="commenter">
						<img class="rounded-x" src="assets/img/team/img1.jpg" alt="">
					</div>
					<div class="comments-itself">
						<h4>
							Jorny Alnordussen
							<span>6 hours ago / <a href="#">Reply</a></span>
						</h4>
						<p>Gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod..</p>
					</div>
				</div>
				<!-- End Blog Comments v2 -->

				<!-- Blog Comments v2 -->
				<div class="row blog-comments-v2 blog-comments-v2-reply">
					<div class="commenter">
						<img class="rounded-x" src="assets/img/team/img2.jpg" alt="">
					</div>
					<div class="comments-itself">
						<h4>
							Susie Lau
							<span>6 hours ago / <a href="#">Reply</a></span>
						</h4>
						<p>Gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod..</p>
					</div>
				</div>
				<!-- End Blog Comments v2 -->

				<!-- Blog Comments v2 -->
				<div class="row blog-comments-v2">
					<div class="commenter">
						<img class="rounded-x" src="assets/img/team/img3.jpg" alt="">
					</div>
					<div class="comments-itself">
						<h4>
							Marcus Farrell
							<span>6 hours ago / <a href="#">Reply</a></span>
						</h4>
						<p>Gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod..</p>
					</div>
				</div>
				<!-- End Blog Comments v2 -->
			</div> -->
			<!-- End Blog Comments v2 -->

			<!-- Form -->
			<!-- <h2 class="title-v4">Post a Comment</h2>
			<form action="https://htmlstream.com/preview/unify-v1.9.7/Blog-Magazine/assets/php/sky-forms-pro/demo-comment-process.php" method="post" id="sky-form3" class="sky-form comment-style-v2">
				<fieldset>
					<div class="row sky-space-30">
						<div class="col-md-6">
							<div>
								<input type="text" name="name" id="name" placeholder="Name" class="form-control bg-color-light">
							</div>
						</div>
						<div class="col-md-6">
							<div>
								<input type="text" name="email" id="email" placeholder="Email" class="form-control bg-color-light">
							</div>
						</div>
					</div>

					<div class="sky-space-30">
						<div>
							<textarea rows="8" name="message" id="message" placeholder="Write comment here ..." class="form-control bg-color-light"></textarea>
						</div>
					</div>

					<p><button type="submit" class="btn-u btn-u-default">Submit</button></p>
				</fieldset>

				<div class="message">
					<i class="rounded-x fa fa-check"></i>
					<p>Your comment was successfully posted!</p>
				</div>
			</form> -->
			<!-- End Form -->
		</div>
	</div>
</div>
<!--=== End Container Part ===-->
<script>
  $(document).ready(function(){
    var id = "<?php echo $id; ?>";
    //console.log(id);
    get_detail(id);
  });

  function get_detail(id){
    if(!id){
      //alert('gagal');
      swal("Error","Disallowed Action","error");
      notFound();
    }
    $.ajax({
      url : site_url + 'service/getDetail',
      data : {'id' : id},
      dataType : 'json',
      type : 'get',
      success : function(result){

          //huge applied here
          $('#create-date').html(result.data.created_at); //append date
          $('#categories').html(result.data.name); //append categories
          $('#title-article').html(result.data.title); // append title
          $('#content').html(result.data.content); // append content
          $('#image-heading').attr('src',site_url+'assets/files/'+result.data.image); //append images path
          $('#authors').html(result.data.author); // append author name
          $('#author-below').html(result.data.author); // append author name
          $('#source').html(result.data.source).attr('href',result.data.url_source); // append source

      },
      error : function(x,h,r){
        console.log(r);
        notFound();
      }
    })
  }
  function notFound(){
    location.replace(site_url+'testing/response/notFound');
  }
  </script>