<!--=== Content Part ===-->
	<div class="container content">
		<!--Error Block-->
		<div class="row">
			<!--Basic Table-->
					<div class="panel panel-green margin-bottom-40">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-tasks"></i> Laporan Penyaluran ZIS</h3>
						</div>
						<div class="panel-body">
							<p>Laporan Penyaluran ZIS ini adalah laporan dari permohonan bantuan yang diajukan dari aplikasi SIMASJID</p>
						</div>
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Nama</th>
									<th class="hidden-sm">No Telepon</th>
									<th>Alamat</th>
									<th>Jumlah</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<!--End Basic Table-->
		</div>
		<!--End Error Block-->
	</div>
	<!--=== End Content Part ===-->
<script type="text/javascript">
	$(document).ready(function(){
		get_data();
	});
	function get_data(url, param){
		if(!url)
			url = site_url +'service/daftarPengeluaran'
		
		$.ajax({
				url : url,
				/*data: $('#search').serialize(),*/
				data: {search:param},
				dataType : 'json',
				type : 'get',
				success : function(result){
					$(".table tbody").html(result.data);
					//$("ul.pagination").html(result.paging);
				},
				error: function(x,h,r){
					console.log(r);
				}
			})
	}
</script>