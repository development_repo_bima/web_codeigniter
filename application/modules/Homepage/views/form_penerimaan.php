<!--=== Container Part ===-->
<div class="container content-sm">
	<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				<form class="reg-page" id="form-zakat-in" method="post" autocomplete="off">
					<div class="reg-header">
						<h2>Bayar Zakat</h2>
						<p>Bayar Zakat, Infaq atau Shodaqoh sekarang gak perlu ribet. Cukup transfer terus lapor disini. Insya Allah lebih mudah dan berkah. Mau lihat Laporan penerimaan ZIS via online? <a href="#">Cek Di Sini</a></p>
					</div>

					<!-- <label>Metode Pembayaran ZIS</label>
					<?php echo $this->modul->getPilihan(1); ?> -->
					<br>
					<div class="row">
					  	<div class="col-sm-12">
						  	<label class="radio-inline" for="radios-0">
						      	<input type="radio" name="jenis" id="radios-0" value="3" checked="checked">
							      Zakat
							    </label> 
							    <label class="radio-inline" for="radios-1">
							      <input type="radio" name="jenis" id="radios-1" value="4">
							      Infaq
							    </label> 
							    <label class="radio-inline" for="radios-2">
							      <input type="radio" name="jenis" id="radios-2" value="5">
							      Shodaqoh
						    </label> 
					  	</div>
				  	</div><br>

				  	<span id="trf-info">
				  	<div class="row">
						<div class="col-sm-6">
							<label>No Rekening <span class="color-red">*</span></label>
							<input type="number" id="no-rek" name="norek" class="form-control margin-bottom-20">
						</div>
						<div class="col-sm-6">
							<label>Atas Nama <span class="color-red">*</span></label>
							<input type="text" name="an" class="form-control margin-bottom-20">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<label>Nama Bank <span class="color-red">*</span></label>
							<input type="text" id="bank" name="bank" class="form-control margin-bottom-20">
						</div>
						<div class="col-sm-6">
							<label>unggah Bukti Bayar <span class="color-red">*</span></label>
							<input type="file" id="userfile" name="file" class="form-control margin-bottom-20">
						</div>
					</div>
					</span>

					<label id="chg-label">Jumlah yang Dibayarkan <span class="color-red">*</span></label>
					<input type="number" name="amount" class="form-control margin-bottom-20">

					<label>Nama Pengirim <span class="color-red">*</span></label>
					<input type="text" name="nama" class="form-control margin-bottom-20">

					<label>No Telepon</label>
					<input type="text" name="telp" class="form-control margin-bottom-20">

					<label>Email</label>
					<input type="email" name="email" class="form-control margin-bottom-20">

					<label>Alamat <span class="color-red">*</span></label>
					<textarea name="alamat" class="form-control margin-bottom-20"></textarea>

					<hr>

					<div class="row">
						<div class="col-lg-6 checkbox">
							<!-- <label>
								<input type="checkbox">
								I read <a href="page_terms.html" class="color-green">Terms and Conditions</a>
							</label> -->
						</div>
						<div class="col-lg-6 text-right">
							<button class="btn-u" type="submit">Selesai</button>
							<a type="reset" class="btn btn-danger">reset</a>
						</div>
					</div>
				</form>
			</div>
		</div>
</div>
<!--=== End Container Part ===-->
<script type="text/javascript">
	$(document).ready(function(){
		$('#select-form').change(function(){
			if($(this).val()==='1'){
				$('#trf-info').fadeIn(); 
				$('#chg-label').html('Jumlah Transfer');
				$('#no-rek').attr('required', true);
				$('#bank').attr('required', true);
				$('#userfile').attr('required', true);
			}else{
				$('#trf-info').fadeOut();
				$('#chg-label').html('Jumlah yang dibayarkan');
				$('#no-rek').attr('required', false);
				$('#bank').attr('required', false);
				$('#userfile').attr('required', false);
			}
		});

	var url = site_url + 'service/saveInData'

	$('#form-zakat-in').ajaxForm({
		
		url : url,
		//delegation : true,
		type : 'post',
		data : $('#form-zakat-in').serialize(),
		dataType : 'json',
		resetForm : true,
		success: function(result){
			if(result.status){
				//alert(result.msg);
				swal({
						title :"Berhasil",
						text : result.msg,
						type : "success",
						showCancelButton : false,
						closeOnConfirm : false,
						showLoaderOnConfirm : true,
					}, function(){
						setTimeout( function(){
							window.location.replace(site_url + 'homepage/invoice/'+result.last_id);
						}, 2000)
					});
			}else{
				swal("Gagal",result.msg,"error");
				//alert(result.msg);
			}
		},
		error: function(x,h,r){
			console.log(r);
		}
		
	});
	return false;
	});
</script>
