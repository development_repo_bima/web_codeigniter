<!-- Master Slider -->
<div class="blog-ms-v1 blog-ms-v1-extend bg-color-darker">
	<div class="master-slider ms-skin-default" id="masterslider">
		<?php echo $this->modul->top_articles(); ?>
	</div>
</div>
<!-- End Master Slider -->
<!--=== Container Part ===-->
<div class="container content-sm">
	<div class="container headline-center-v2 headline-center-v2-dark margin-bottom-60">
		<h2>Shortcuts</h2>
		<span class="bordered-icon"><i class="fa fa-th-large"></i></span>
		<!-- Service Box -->
		<div class="row text-center margin-bottom-60">
			<div class="col-md-6 md-margin-bottom-50">
				<img alt="" src="<?php echo base_url(); ?>public/assets/img/icons/flat/01.png" class="image-md margin-bottom-20">
				<h1 class="title-v3-md margin-bottom-10">Berzakat</h1>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cur modo, tortor mauris con</p>
			</div>
			<div class="col-md-6 flat-service">
				<img alt="" src="<?php echo base_url(); ?>public/assets/img/icons/flat/03.png" class="image-md margin-bottom-20">
				<h2 class="title-v3-md margin-bottom-10">Permohonan Bantuan</h2>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cur modo, tortor mauris con</p>
			</div>
		</div>
		<!-- End Service Box -->
	</div><!--/container -->

	<!--=== Recent Posts ===-->
	<div class="container content-sm">
		<div class="headline-center-v2 headline-center-v2-dark margin-bottom-60">
			<h2>New Posts</h2>
			<span class="bordered-icon"><i class="fa fa-th-large"></i></span>
			<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et qua s molestias excepturi vehicula sem ut volutpat. Ut non libero magna fusce co.</p>
		</div><!--/Headline Center V2-->

		<div class="row">
			<?php echo $this->modul->index_articles(); ?>
		</div>
	</div><!--/container-->
	<!--=== End Recent Posts ===-->
</div>

	