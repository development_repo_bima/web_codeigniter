<!--=== Container Part ===-->
<div class="container content-sm">
	<div id="load-content"></div>
	<!-- Pager v4 -->
	<ul class="pager pager-v4" id="load-paging">
		<!-- <li class="previous"><a class="rounded-3x" href="#">&larr; Older</a></li>
		<li class="page-amount">1 of 7</li>
		<li class="next"><a class="rounded-3x" href="#">Newer &rarr;</a></li> -->
	</ul>
	<!-- End Pager v4 -->
</div>
<!--=== End Container Part ===-->
<script type="text/javascript">
	$(document).ready(function(){
		get_data();
	});
	function get_data(url, param){
		if(!url)
			url = site_url +'service/daftarBerita'
		
		$.ajax({
			url : url,
			/*data: $('#search').serialize(),*/
			data: {search:param},
			dataType : 'json',
			type : 'get',
			success : function(result){
				$("#load-content").html(result.data);
				$("#load-paging").html(result.page);
			},
			error: function(x,h,r){
				console.log(r);
			}
		})
	}
</script>