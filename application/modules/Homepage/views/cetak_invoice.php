<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- Mirrored from htmlstream.com/preview/unify-v1.9.7/page_invoice.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Jan 2017 12:32:35 GMT -->
<head>
	<title>Bukti Bayar</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/headers/header-default.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/footers/footer-v1.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/animate.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/plugins/font-awesome/css/font-awesome.min.css">

	<!-- CSS Page Style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/pages/page_invoice.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/theme-skins/dark.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/custom.css">
</head>

<body>
	<div class="wrapper">
		
		<!--=== Content Part ===-->
		<div class="container content">
			<!--Invoice Header-->
			<div class="row invoice-header">
				<div class="col-xs-6">
					<!-- You also can use a title instead of image -->
					<h2 class="pull-left">Bukti Bayar</h2>
				</div>
				<div class="col-xs-4 invoice-numb">
					#123456789 / 20, October 2013
					<span>Auiderer facilis consectetur</span>
				</div>
			</div>
			<!--End Invoice Header-->

			<!--Invoice Detials-->
			<div class="row invoice-info">
				<div class="col-xs-5">
					<div class="tag-box tag-box-v3">
						<h2>Informasi Penyumbang:</h2>
						<ul class="list-unstyled">
							<li><strong>Nama:</strong> <?php echo $nama_donatur; ?></li>
							<li><strong>No Telp:</strong> <?php echo $no_telp; ?></li>
							<li><strong>Email:</strong> <?php echo ($email)?$email:'-'; ?></li>
							<li><strong>Alamat:</strong> <?php echo $alamat; ?></li>
						</ul>
					</div>
				</div>
				<div class="col-xs-5">
					<div class="tag-box tag-box-v3">
						<h2>Detail Pembayaran:</h2>
						<!-- <ul class="list-unstyled">
							<li><strong>Metode Bayar:</strong> <?php echo $nama_pilihan; ?></li>
							<li><strong>Bank Name:</strong> 123456789012</li>
							<li><strong>Account Number:</strong> 123456789012</li>
							<li><strong>SWIFT Code:</strong> 123DEMO45DEMO</li>
							<li><strong>V.A.T Reg #:</strong> 678DEMO45545</li>
						</ul> -->
						<ul class="list-unstyled">
							<li><strong>Metode Bayar:</strong> <?php echo $metode_bayar; ?></li>
							<li><strong>Pembayaran:</strong> <?php echo $jenis_bayar; ?></li>
						</ul>
					</div>
				</div>
			</div>
			<!--End Invoice Detials-->

			<!--Invoice Table-->
			<div class="panel panel-default margin-bottom-40">
				<div class="panel-heading">
					<h3 class="panel-title">Invoice Details</h3>
				</div>
				<div class="panel-body">
					<p>Telah kami terima sejumlah uang untuk pembayaran Zakat Bpk/Ibu <?php echo $nama_donatur; ?> sebesar .. pada hari .. tanggal .. . Semoga Allah SWT membalas zakat Bpk/Ibu berlipat-lipat ganda. Aamiin</p>
					<p align="right">Jakarta, 1 Januari 2017 <br> Ketua Masjid Baitul</p>
					<p align="right"></p>

					<br>
					<br>
					<br>
					<p align="right"><b>Marjoni</b></p>
				</div>
				
			</div>
			<!--End Invoice Table-->

			<!--Invoice Footer-->
			<!-- <div class="row">
				<div class="col-xs-6">
					<div class="tag-box tag-box-v3 no-margin-bottom">
						<address class="no-margin-bottom">
							25, Lorem Lis Street, Orange <br>
							California, US <br>
							Phone: 800 123 3456 <br>
							Fax: 800 123 3456 <br>
							Email: <a href="mailto:info@example.com">info@example.com</a>
						</address>
					</div>
				</div>
				<div class="col-xs-6 text-right">
					<ul class="list-unstyled invoice-total-info">
						<li><strong>Sub - Total Amount:</strong> $109365</li>
						<li><strong>Discount:</strong> 14.8%</li>
						<li><strong>VAT ($6):</strong> $8371</li>
						<li><strong>Grand Total:</strong> $101486</li>
					</ul>
					<button class="btn-u sm-margin-bottom-10" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</button>
					<button class="btn-u">Submit The Invoice</button>
				</div>
			</div> -->
			<!--End Invoice Footer-->
		</div><!--/container-->
		<!--=== End Content Part ===-->

	</div><!--/wrapper-->
</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.9.7/page_invoice.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 20 Jan 2017 12:32:35 GMT -->
</html>
