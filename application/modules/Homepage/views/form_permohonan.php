<!--=== Container Part ===-->
<div class="container content-sm">
	<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				<form class="reg-page" id="form-zakat-out" method="post" autocomplete="off">
					<div class="reg-header">
						<h2>Permohonan Bantuan</h2>
						<p></p>
					</div>

					<label>Jenis Pemohon</label>
					<?php echo $this->modul->getPilihan(2); ?>
					<br>

					<span id="penerima-span" style="display:none;">
						<label id="label-nama"> <span class="color-red">*</span></label>
						<input type="text" required name="nama" class="form-control margin-bottom-20">
					</span>

					<label>Jumlah Bantuan <span class="color-red">*</span></label>
					<input type="number" name="amount" class="form-control margin-bottom-20">

					<label>No Telepon <span class="color-red">*</span></label>
					<input type="text" name="telp" class="form-control margin-bottom-20">

					<label>Email</label>
					<input type="email" name="email" class="form-control margin-bottom-20">

					<label>Alamat <span class="color-red">*</span></label>
					<textarea name="alamat" class="form-control margin-bottom-20"></textarea>

					<label>Keterangan</label>
					<textarea name="keterangan" class="form-control margin-bottom-20"></textarea>

					<hr>

					<div class="row">
						<div class="col-lg-6 checkbox">
							<!-- <label>
								<input type="checkbox">
								I read <a href="page_terms.html" class="color-green">Terms and Conditions</a>
							</label> -->
						</div>
						<div class="col-lg-6 text-right">
							<button class="btn-u" onClick="save_data(); return false;" type="submit">Selesai</button>
						</div>
					</div>
				</form>
			</div>
		</div>
</div>
<!--=== End Container Part ===-->
<script type="text/javascript">
	$(document).ready(function(){
		$('#select-form').change(function(){
			if($(this).val()==='6'){
				$('#penerima-span').fadeIn(); 
				$('#label-nama').html('Nama Yayasan Penerima');
			}else if($(this).val()==='7'){
				$('#penerima-span').fadeIn();
				$('#label-nama').html('Nama Penerima');
			}else{
				$('#penerima-span').fadeOut();
			}
		})
	})

	function save_data(){
	
		var url = site_url + 'service/addOutData'
		
		$.ajax({
			
			url : url,
			dataType : 'json',
			type : 'post',
			data : $('#form-zakat-out').serialize()+'&jenis_penerima='+$('#select-form').val(),
			success: function(result){
				if(result.status){
					//alert(result.msg);
					swal({
							title :"Berhasil",
							text : result.msg,
							type : "success",
							showCancelButton : false,
							closeOnConfirm : false,
							showLoaderOnConfirm : true,
						}, function(){
							setTimeout( function(){
								window.location.replace(site_url + 'homepage/permohonan');
							}, 2000)
						});
				}else{
					swal("Gagal",result.msg,"error");
					//alert(result.msg);
				}
			},
			error: function(x,h,r){
				console.log(r);
			}
		})
	}
</script>