<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends My_Front{

	public function __construct(){
		parent::__construct();
		$this->load->model('Front_model');
	}

	public function index(){
		$data = [
			
		];
		$this->_render('new_index', $data);
		#$this->_render('form_index', $data);
	}

	public function pages_404(){
		$data = [
			'custom_css' => [
				'style'	=> 'public/assets/css/page_404_error.css'
			]
		];
		$this->_render('404_page', $data);
	}
	
	public function news_event(){
		$this->_render('list_artikel');
	}

	/* Transaksi */
	public function bayar_zakat(){
		
		$data = [
			'custom_css' => [
				'style'	=> 'public/assets/css/page_log_reg_v1.css',
				'style2'=> 'assets/dist/sweetalert.css'
			],
			'custom_js'	=> [
				'form'=>'assets/js/jquery.form.js',
				'style'	=> 'assets/dist/sweetalert.min.js'
			]
		];
		$this->_render('form_penerimaan', $data);	
	}

	public function permohonan(){
		$data = [
			'custom_css' => [
				'style'	=> 'public/assets/css/page_log_reg_v1.css',
				'style2'=> 'assets/dist/sweetalert.css'
			],
			'custom_js'	=> [
				'form'=>'assets/js/jquery.form.js',
				'style'	=> 'assets/dist/sweetalert.min.js'
			]
		];
		$this->_render('form_permohonan', $data);	
	}

	/* laporan menu */
	public function laporan_pemasukan(){
		$data = [
			'custom_css' => [
				//'style'	=> 'public/assets/css/page_404_error.css'
			]
		];
		$this->_render('in_report', $data);
	}

	public function laporan_pengeluaran(){
		$data = [
			'custom_css' => [
				'style'	=> 'public/assets/css/page_404_error.css'
			]
		];
		$this->_render('out_report', $data);
	}

	public function post(){

		$data = [
			'id' 		=> $this->uri->segment(3),
			'custom_css' => [
				'style'	=> 'assets/dist/sweetalert.css'
			],
			'custom_js' => [
				'style'	=> 'assets/dist/sweetalert.min.js'
			]
		];

		$this->_render('single_artikel', $data);
	}

	public function test_cetak(){

		$this->load->library('pdf');
		$data = $this->Front_model->get_invoice_data($id);
		$this->pdf->load_view('cetak_invoice',$data);
		$this->pdf->Output();
	}

	public function invoice($id = null){
		$data = $this->Front_model->get_invoice_data($id);

		$this->load->view('invoice',$data);
	}

}