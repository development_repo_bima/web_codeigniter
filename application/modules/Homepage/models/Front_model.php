<?php 

class Front_model extends CI_Model{
	
	public function get_Artikel($search = null, $limit = 0, $offset = 0){
		//print_r($offset);exit;
		$this->db->select('a.*, c.initial, c.name')
					->from('t_artikel as a')
					->join('m_category as c','c.category_id = a.category','left')
					->where('status','1');
					//->join('m_akses as a','a.jabatan_id = j.jabatan_id','left');
			if($search){
				$this->db->like('title', $search);
				$this->db->or_like('content', $search);
			}
		$this->db->order_by('id', 'DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result_array();
		//$data['total'] = count();
		$data['total'] = count($this->db->select('id')->from('t_artikel')->where('status','1')->get()->result_array());
		//print_r(count($data['test']));die;
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}

	public function get_detail($id){
		
		//print_r($this->db->last_query());exit;
		//active record ways still get errors. how it's work??
		/* return $this->db->query("SELECT u.username,u.email,u.jabatan_id, j.nama_jabatan 
									FROM m_user u 
									LEFT JOIN m_jabatan j ON j.jabatan_id = u.jabatan_id 
									WHERE u.id = {$id}")->row_array(); */
		return $this->db->select('a.*,b.name')->from('t_artikel a')->join('m_category as b','b.category_id = a.category','left')->where(array('uid'=>$id,'status'=>'1'))->get()->row_array();
	}

	public function get_invoice_data($id){
		/*return $this->db->get_where('t_in_zakat',['id'=>$id])->row_array();*/

		return $this->db->select('*, b.nama_pilihan AS metode_bayar, a.nama_pilihan AS jenis_bayar')
					->from('t_in_zakat')
					->join('m_pilihan a','a.id_metode = t_in_zakat.jenis_dana','left')
					->join('m_pilihan b','b.id_metode = t_in_zakat.metode_bayar','left')
					->where('id',$id)
					->get()->row_array();

	}
}