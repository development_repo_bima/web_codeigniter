<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends My_Front{

	public function __construct(){
		parent::__construct();
		$this->load->model('Service_model');
	}

	/*public function index(){
		$data = [
			'hot_news' => $this->modul->top_articles()
		];
		$this->_nampil('front_view', $data);
	}*/

	public function index(){
		$data = [
			
		];
		$this->_render('new_index', $data);
		#$this->_render('form_index', $data);
	}

	public function detailView(){
		$data = [
			'id' 		=> $this->uri->segment(3),
			'hot_news'	=> $this->modul->top_articles()
		];

		$get = $this->db->select('counter')->from('t_artikel')->where(array('uid'=>$data['id'],'status'=>'1'))->get()->row_array();
		$inc = $this->db->update('t_artikel',array('counter'=>($get['counter']+1)),array('uid'=>$data['id']));
		if($inc){
			//print_r($inc);die;
			$this->_nampil('detailView', $data);
		}else{
			site_url().'front';
		}
	}

	public function getDetail(){
		$uid = $this->Service_model->get_detail($this->input->get('id'));
		
		if($uid){
			$uid['created_at'] = date("j F Y, g:i a",strtotime($uid['created_at']));
			$res = ['status'=>http_response_code(), 'msg'=>'Data Loaded...', 'data'=>$uid, 'url'=>current_url()];
		}else{
			$this->output->set_status_header('404');
			$res = ['status'=>http_response_code(),'msg'=>'Data Not Found...','data'=>[],'url'=>current_url()];
		}
		$this->response($res);
	}
	
	public function daftarBerita(){
		
		$search = null;
		$limit 	= 5;
		$offset = $this->uri->segment(3,0);
		$url 	= '/service/artikelList/';
		
		$all = $this->Service_model->get_artikel($search, $limit, $offset);
		
		$total = $all['total'];
		$div = $paging = '';
		if(!$all['data']){
			$div.= '<div><span>Empty</span></div>';

			$msg = 'Empty..';
		}else{
			$no = $offset + 1; $i=1;
			foreach($all['data'] as $k=>$v){
				//print_r($v['image']);die;
				//print_r($v);
				$date = date("j F Y, g:i a",strtotime($v['created_at']));
				//print_r($v['content']);
				$concat = $v['counter'].",'".$v['uid']."'";

				$div .= '<div class="row margin-bottom-50">';
				$div .= '<div class="col-sm-5 sm-margin-bottom-20">';
				$div .= '<img class="img-responsive" src="'.base_url().'assets/files/'.$v['image'].'" alt="'.$v['title'].'"></div>';
		
				$div .= '<div class="col-sm-7">';
				$div .= '<div class="blog-grid">';
				$div .= '<h3><a href="'.site_url().'homepage/post/'.$v['uid'].'/'.urlencode($v['title']).'">'.$v['title'].'</a></h3>';
				$div .= '<ul class="blog-grid-info">
							<li>#'.$v['name'].'</li>
							<li>'.$date.'</li>
							<li><a href="#"><i class="fa fa-comments"></i> 0</a></li>
						</ul>';

				$div .= '<p>'.$this->sub($v['content']).'</p>';
				$div .= '<a class="r-more" href="'.site_url().'homepage/post/'.$v['uid'].'/'.urlencode($v['title']).'">Read More...</a></div></div></div>';

				$i++;$no++;
			}
			/*$paging .="<li><span class='btn btn-warning'>Menampilkan ".($i-1)." Artikel Per halaman</span></li>&nbsp;";*/
			$paging .= $this->_front_paging($url,$total,$limit);
			
			$msg = 'Data Loaded...';
		}
		
		echo json_encode(array('status'=>true,'msg'=>$msg,'data'=>$div,'page'=>$paging));
	}

	public function daftarPenerimaan(){
		$limit  = $this->config->item('per_page');
		$offset = $this->uri->segment(3,0);
		$search = $this->input->get('search');
		$url 	= 'service/daftarPenerimaan/';
		
		$data = $this->Service_model->get_penerimaan($search, $limit, $offset);
		//print_r($data);exit;
		$total = $data['total'];
		$tr = $paging = '';
		if(!$data['data']){
			if($search){
				$tr .="<tr>";
				$tr .="<td colspan='5'>Not Found...</td>";
				$tr .="</tr>";

				$msg = 'Data Not found';
			}else{
				$tr .="<tr>";
				$tr .="<td colspan='5'>Empty Data...</td>";
				$tr .="</tr>";

				$msg = 'Empty Data';
			}
		};
		
		if($data['data']){
			$no = $offset + 1;
			
			$i = 1;
			$tot_in = 0;
			foreach($data['data'] as $o){
				
				$tr .="<tr>";
				$tr .="<td>".$no."</td>";

				$tr .="<td>".$o->nama_donatur."</td>";
				$tr .="<td class='hidden-sm'>".$o->no_telp."</td>";
				$tr .="<td>".$o->alamat."</td>";
				$tr .="<td>".'Rp. '.$this->general->num_format($o->jumlah_dana)."</td>";
				$tr .="</tr>";

			$i++;$no++; $tot_in += $o->jumlah_dana;
			}

			$tr .='<tr><td></td><td></td><td></td><td class="pull-right"><b>TOTAL</b></td>';
			$tr .="<td><b>".'Rp. '.$this->general->num_format($tot_in)."</b></td></tr>";

			$paging .="<li><span class='page-info'>Displaying ".($i-1)." of ".$total." data</span></li>";
			//$paging .= $this->_paging($url,$total,$limit);
			
			$msg = 'Data Loaded...';
		}

		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr,'paging'=>$paging));
	}


	public function daftarPengeluaran(){
		$limit  = $this->config->item('per_page');
		$offset = $this->uri->segment(3,0);
		$search = $this->input->get('search');
		$url 	= 'service/daftarPengeluaran';
		
		$data = $this->Service_model->get_pengeluaran($search, $limit, $offset);
		
		$total = $data['total'];
		$tr = $paging = '';
		if(!$data['data']){
			if($search){
				$tr .="<tr>";
				$tr .="<td colspan='7'>Not Found...</td>";
				$tr .="</tr>";

				$msg = 'Data Not found';
			}else{
				$tr .="<tr>";
				$tr .="<td colspan='7'>Empty Data...</td>";
				$tr .="</tr>";

				$msg = 'Empty Data';
			}
		};
		
		if($data['data']){
			$no = $offset + 1;
			
			$i = 1; $tot_out = 0;
			foreach($data['data'] as $o){
				
				$tr .="<tr>";
				$tr .="<td>".$no."</td>";

				$tr .="<td>".$o->nama_penerima."</td>";
				$tr .="<td>".$o->no_telp."</td>";
				$tr .="<td>".$o->alamat_penerima."</td>";
				$tr .="<td>".'Rp. '.$this->general->num_format($o->jumlah_diterima)."</td>";
				$tr .="</tr>";
			$i++;$no++; $tot_out += $o->jumlah_diterima;
			}

			$tr .='<tr><td></td><td></td><td></td><td class="pull-right"><b>TOTAL</b></td>';
			$tr .="<td><b>".'Rp. '.$this->general->num_format($tot_out)."</b></td></tr>";

			$paging .="<li><span class='page-info'>Displaying ".($i-1)." of ".$total." data</span></li>";
			//$paging .= $this->_paging($url,$total,$limit);
			
			$msg = 'Data Loaded...';
		}

		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr,'paging'=>$paging));
	}

	public function viewer(){
		$data = $this->input->input_stream(null, true);
		$inc_count = array(
			'counter'	=> $data['counter'] + 1
		);

		$update_count = $this->db->update('t_artikel',$inc_count,array('uid'=>$data['uid']));

		print_r($data);die;
	}

	public function saveInData(){
		
		$data = $this->input->post(null, true);
		

		if(isset($_FILES['userfile']['name'])){

			$this->_config_upload($data);

			if(!$this->upload->do_upload()){
				//echo "gagal";exit;
				$res = array('status'=>false, 'msg'=>$this->upload->display_errors());
			}else{
				
				$det = $this->upload->data();
				$data['bukti_bayar'] = $det['file_name'];

				$query = $this->Service_model->save_data($data);
				
			}
		}else{
			$query = $this->Service_model->save_data($data);
			
		}
		if($query[0]){
			$res = array('status'=>true, 'msg'=>'Berhasil menambahkan data', 'last_id'=>$query[1]);
		}else{
			if(isset($_FILES['userfile']['name'])){@unlink($det['file_path'].$det['file_name']);}
			$res = array('status'=>false, 'msg'=>'Terjadi Kesalahan saat menyimpan file..');
		}
		$this->response($res);
	}

	public function addOutData(){
		
		$data = $this->input->post(null, true);
		
		$query = $this->Service_model->set_data($data);
		if($query){
			$res = array('status'=>true, 'msg'=>'Berhasil menambahkan data');
		}else{
			$res = array('status'=>false, 'msg'=>'Terjadi Kesalahan, \n silahkan coba lagi');
		}
		$this->response($res);
	}

	public function sub($str){
		$str = strip_tags($str);
		$new_str = preg_replace('/\s+?(\S+)?$/', '', substr($str, 0, 201));
		return $new_str;
	}

}