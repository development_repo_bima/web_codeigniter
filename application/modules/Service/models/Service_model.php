<?php 

class Service_model extends CI_Model{
	
	public function get_Artikel($search = null, $limit = 0, $offset = 0){
		//print_r($offset);exit;
		$this->db->select('a.*, c.initial, c.name')
					->from('t_artikel as a')
					->join('m_category as c','c.category_id = a.category','left')
					->where('status','1');
					//->join('m_akses as a','a.jabatan_id = j.jabatan_id','left');
			if($search){
				$this->db->like('title', $search);
				$this->db->or_like('content', $search);
			}
		$this->db->order_by('id', 'DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result_array();
		//$data['total'] = count();
		$data['total'] = count($this->db->select('id')->from('t_artikel')->where('status','1')->get()->result_array());
		//print_r(count($data['test']));die;
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}

	public function get_penerimaan($search = null, $limit = 0, $offset = 0){
		$this->db->select('*')
					->from('t_in_zakat')
					->join('m_pilihan','m_pilihan.id_metode = t_in_zakat.jenis_dana','left');
			if($search)
				$this->db->like('nama_donatur', $search);
		
		$this->db->order_by('id', 'DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result();
		$data['total'] = $this->db->count_all('t_in_zakat');
		
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}

	public function get_pengeluaran($search = null, $limit = 0, $offset = 0){

		$this->db->select('*')
					->from('t_out_zakat')
					->join('m_pilihan','m_pilihan.id_metode = t_out_zakat.jenis_penerima','left');
					//->join('m_akses as a','a.jabatan_id = j.jabatan_id','left');
			if($search)
				$this->db->like('nama_penerima', $search);
				$this->db->or_like('nama_pilihan', $search);
		
		$this->db->order_by('id', 'DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result();
		$data['total'] = $this->db->count_all('t_out_zakat');
		
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}

	public function get_detail($id){
		
		//print_r($this->db->last_query());exit;
		//active record ways still get errors. how it's work??
		/* return $this->db->query("SELECT u.username,u.email,u.jabatan_id, j.nama_jabatan 
									FROM m_user u 
									LEFT JOIN m_jabatan j ON j.jabatan_id = u.jabatan_id 
									WHERE u.id = {$id}")->row_array(); */
		return $this->db->select('a.*,b.name')->from('t_artikel a')->join('m_category as b','b.category_id = a.category','left')->where(array('uid'=>$id,'status'=>'1'))->get()->row_array();
	}

	public function save_data($data){
	
		$save = [
			'no_transaksi'	=> mt_rand(),
			'metode_bayar'	=>1,
			'jenis_dana'	=>$data['jenis'],
			'no_rek'		=>isset($data['norek'])?$data['norek']:'',
			'atas_nama'		=>isset($data['an'])?$data['an']:'',
			'nama_bank'		=>isset($data['bank'])?$data['bank']:'',
			'bukti_bayar'	=>isset($data['bukti_bayar'])?$data['bukti_bayar']:'',
			'nama_donatur'	=>$data['nama'],
			'no_telp'		=>isset($data['telp'])?$data['telp']:'-',
			'email'   		=>isset($data['email'])?$data['email']:'-',
			'alamat' 		=>$data['alamat'],
			'jumlah_dana'  	=>$data['amount'],
			//'tgl_penerimaan'=>$data['tgl_penerimaan'],
			'created_at'	=>date('Y-m-d h:i:s'),
			'updated_at'	=>date('Y-m-d h:i:s'),
		];
		
		$this->db->insert('t_in_zakat', $save);
		$last_id = $this->db->insert_id();
		return [true,$last_id];
		
	}

	public function set_data($data){
		$save = [
			'no_transaksi'		=> mt_rand(),
			'jenis_penerima'	=>$data['jenis_penerima'],
			'nama_penerima'		=>$data['nama'],
			'jumlah_diterima'	=>$data['amount'],
			'no_telp'			=>isset($data['telp'])?$data['telp']:'-',
			'email'   			=>isset($data['email'])?$data['email']:'-',
			'ket'   			=>isset($data['keterangan'])?$data['keterangan']:'-',
			'alamat_penerima'	=>$data['alamat'],
			//'tgl_penerimaan'=>$data['tgl_penerimaan'],
			'created_at'		=>date('Y-m-d h:i:s'),
			'updated_at'		=>date('Y-m-d h:i:s'),
		];
		
		$this->db->insert('t_out_zakat', $save);
		return true;
	}
}