<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title;?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo isset($custom_css) ? base_url().$custom_css : ''; ?>" rel="stylesheet">

    <!-- Custom CSS 
    <link href="css/small-business.css" rel="stylesheet"> -->
	<link href="<?php echo base_url(); ?>assets/dist/sweetalert.css" rel="stylesheet">
	
	<!-- Jquery reference must on top or javascript code not running properly -->
	<script type"text/javascript"> var site_url = "<?php echo site_url();?>";</script>
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/dist/sweetalert.min.js"></script>
    <script src="<?php echo isset($custom_js) ? base_url().$custom_js : '' ; ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

</head>

<body>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-login">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-6">
							<a href="#" class="active" id="login-form-link"><i class="fa fa-sign-in"></i> Login</a>
						</div>
						<div class="col-xs-6">
							<a href="#" id="register-form-link"><i class="fa fa-user-plus"></i> Register</a>
						</div>
					</div>
					<hr>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<span style="color:#EA4335"><?php echo ($this->session->flashdata('error_login')) ? $this->session->flashdata('error_login') : ''; ?></span>
							<form id="login-form" action="<?php echo site_url().'/auth/doLogin';?>" method="post" role="form" style="display: block;" autocomplete="off">
								<div class="form-group">
									<input type="text" name="username" id="username_log" tabindex="1" class="form-control" placeholder="Username" value="">
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password_log" tabindex="2" class="form-control" placeholder="Password">
								</div>
								<div class="form-group text-center">
									<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
									<label for="remember"> Remember Me</label>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<div class="text-center">
												<a href="<?php echo site_url(); ?>" tabindex="5" class="forgot-password">Go to Homepage</a>
											</div>
										</div>
									</div>
								</div>
							</form>
							<form id="register-form" method="post" role="form" style="display: none;">
								<div class="form-group">
									<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" autocomplete="off">
								</div>
								<div class="form-group">
									<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="" autocomplete="off">
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off">
								</div>
								<div class="form-group">
									<input type="password" name="confirm_password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password" autocomplete="off">
								</div>
								<div class="form-group select-form">
									<span id="fuchs"></span>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<button type="button" id="regis" class="form-control btn btn-register">Register</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

</html>
<script type="text/javascript">
	$(document).ready(function(){
		get_jabatan();
		$(document).on("click", "#regis", function(){
			//alert("haha")
			//console.log($("#username").val());
			if($("#username").val=='' || $("#email").val()=='' || $("#password").val()=='' || $("#confirm-password").val()==''){
				swal("Oops..","Harap isi semua form","error");
			}else if($("#confirm-password").val()!= $("#password").val()){
				swal("Oops..","Password dan confirm password tidak sama", "error");
			}else{
				save_data();
			}
			//
		})
	});
	function save_data(){
		var data = "jabatan_id="+$('#select-form').val()+'&'+$("#register-form").serialize()
		var url = site_url + '/auth/register';
		
		$.ajax({
			url:url,dataType:'json',type:'post',data:data,
			success: function(result){
				if(result.status==true){
					swal("Sukses",result.msg,"success");
					setTimeout(function(){
					   window.location.reload(1);
					}, 3000);
				}else{
					swal("Gagal",result.msg,"error");
				}
			},
			error:function(x,h,r){
				alert(r);
			}
		});
		
	}
	function get_jabatan(){
		//$.get(site_url+'auth/getJabatan','json',function(result){$('span').html(result.data)});
		$.ajax({
			url : site_url + '/auth/getJabatan',dataType:'json',type:'get',
			success:function(result){
				$('#fuchs').html(result.data);
			}
		})
	}
	
</script>