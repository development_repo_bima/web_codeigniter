<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends My_Front {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = [
			'title'		=> 'Halaman Login',
			'custom_js' => 'assets/js/script.js',
			'custom_css'=> 'assets/css/style.css'
		];
		$this->load->view('auth_view', $data);
		//$this->tools->getLayout('auth_view',$data);
	}
	
	public function register(){
		$this->load->model('Auth_model');
		//print_r($this->input->post(null, true));exit;
		$checks = $this->Auth_model->register($this->input->post(null, true));
		
		if($checks){
			$response = array('status'=>true, 'msg'=>"Selamat Bergabung! Silahkan Login...");
		}else{
			$response = array('status'=>false, 'msg'=>'Hampir berhasil, Silahkan perbaiki formnya...');
		}
		
		$this->response($response);
	}
	
	public function doLogin(){
		$this->load->model('Auth_model');
		$user = $this->Auth_model->checkUsername($this->input->post('username', true));

		if($user){
			$status = $this->Auth_model->checkStatus($this->input->post('username', true));
			
			if($status){
			
				$pass = $this->input->post('password', true);
				
				if($this->_validateCreds($pass, $user['hash'], $user['salt']))
					//echo "berhasil";	
					//return true;
					$newSession = [
								'loggedIn'	=> true,
								'username' 	=>$user['username'],
								'email'		=>$user['email'],
								'jabatan_id'=>$user['jabatan_id']
							];
					$this->session->set_userdata($newSession);
					//print_r($this->session->userdata('username'));exit;
					//echo $this->session->userdata('token');exit;
					$this->db->update('m_user', array('last_seen'=>date('Y-m-d h:i:s')), array('id'=>$user['id']));
					redirect('home');
			}
			
			$this->session->set_flashdata('error_login', 'Akun Anda Nonaktif');
			redirect('Auth');
			
		}
		$this->session->set_flashdata('error_login', 'Username atau Password Salah');
		redirect('Auth');
		//echo "gagal";
		//return false;
	}
	
	public function getJabatan(){
		
		$query = $this->db->get_where('m_jabatan',['credentials'=>1])->result_array();
		$sel = '';
		$sel .="<select class='form-control' required id='select-form'><option value=''>--Select Tipe User--</option>";
		foreach($query as $a){
			//administrator not displayed on select
			if($a['jabatan_id']!=1){
				$sel .="<option value='".$a['jabatan_id']."'>".$a['nama_jabatan']."</option>";
			}
		}
		$sel .="</select>";
		//print_r($sel);exit;
		$this->response(array('data'=>$sel));
	}
	
	private function _validateCreds($password, $hash, $salt){
		
		if(!$hash and !$salt)
			return false;
		
		$results_ = crypt(trim($password), '$2a$12$'.$salt.'$'); 
		
		if($results_ =! $hash)
			return false;
		
		return true;
		
	}
	
	/* private function _blade($view,$data=array()){
		$this->load->view('parts/header' ,$data);
		$this->load->view($view, $data);
		$this->load->view('parts/footer' ,$data);
	} */
}
