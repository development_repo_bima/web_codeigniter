<?php 
/* author = Bima Samudra<bimarossoneri@gmail.com> */

class Auth_model extends CI_Model{
	
	public function register($data){
		$salt = md5(trim($data['username']));
		//print_r($data);exit;
		if($data['password'] === $data['confirm_password']){
			$arr = [
				'username' 	=> $data['username'],
				'email'		=> $data['email'],
				'jabatan_id'=> $data['jabatan_id'],
				'salt'		=> $salt,
				'hash' 		=> crypt(trim($data['password']),'$2a$12$'.$salt.'$')
			];
			//print_r($arr);exit;
			return $this->db->insert('m_user', $arr);
			
		}else{
			return false;
		}
		//echo $data['username'];exit;
		
	}
	
	public function checkUsername($user){
		
		$sql = $this->db->get_where('m_user', ['username'=>$user])->row_array();
		
		return $sql;
		//print_r($sql);exit;
		
	}
	
	public function checkstatus($user){
		
		$sql = $this->db->get_where('m_user', ['username'=>$user, 'active'=>1])->row_array();
		
		return $sql;
		//print_r($sql);exit;
		
	}
	
}

?>