<?php 

class Home extends My_Back {

	public function __construct(){
		parent::__construct();

	}

	public function index(){
		$data = [
			'name' => $this->session->userdata('username')
		];
		$this->_nampil('homeView', $data);
	}
	
}