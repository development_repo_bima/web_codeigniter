<?php

class Upload_model extends CI_Model{

	private $table = 'm_files';
	
	public function getList($search=null, $limit=0, $offset=0){
		$this->db->select('*')->from($this->table);
			//print_r($search);die;
		if($search){
			$this->db->like('title',$search);
			$this->db->or_like('filename',$search);
		}
		
		
		$this->db->order_by('id','DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result();
		$data['total'] = $this->db->count_all($this->table);
		
		return $data;
	}
	
	public function saveUpload($title,$filename,$path){
		
		$val = array(
			'filename' 	=> $filename,
			'title'		=> $title,
			'path'		=> $path,
			'type'		=> 1,
			'date'		=> date('Y-m-d H:i:s')
		);
		
		return $this->db->insert($this->table, $val);
	}
}