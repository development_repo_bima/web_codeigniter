<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends My_Back{
	
	public function __construct(){
		parent::__construct();
		
		$this->load->model('Upload_model');
		
	}
	
	public function index($parent=0,$hasil=''){
		/*
		*	if we need to load extra css or js,
		*	defined the path(s) here and it will loaded.
		*	that things needed to reduce time when loading any page :) cheers..
		*/
		$data = array(
				'custom_css'=> array('customs'=>'assets/css/fancybox.custom.css'),
				'title' => 'List Upload',
				'header'=> 'List Data Upload'
		);
		
		$this->_nampil('listUploadView',$data);
	}
	
	public function addUpload(){
		
		$data = array(
				'custom_js' => array('form'=>'assets/js/jquery.form.js'),
				'title' => 'Form Upload',
				'header'=> 'New Upload'
		);
		$this->_nampil('addUploadLive', $data);
		
	}
	
	public function detailUpload(){
		$id = $this->uri->segment(3);
		$data = $this->Upload_model->getList(null,$id);;
		$tr='';
		foreach($data['data'] as $a){
			$tr .="<div class='col-md-3 col-sm-4 col-xs-6'>";
			$tr .="<label>".$a->title."</label>";
			$tr .="<a class='pull-right' href='".site_url()."upload/delete/".$a->id."' style='color:red;'><i class='fa fa-trash'></i> delete</a>";
			$tr .="<label class='pull-right'>".$a->date."|</label>";
			$tr .="<a title='click for details' href='".site_url()."upload/detailUpload/".$a->id."'>";
			$tr .="<img style='margin-bottom:20px;' class='img-responsive' src='".site_url()."assets/files/".$a->filename."'/>";
			$tr .="</a></div>";
		}
			$msg = 'Data loaded...';
			//$this->response()
		die($tr);
	}
	
	public function getUploadList(){
		
		$limit = $this->config->item('per_page');
		$offset = 0;
		$q = $this->input->post('search');
		$tr = '';
		$data = $this->Upload_model->getList($q,$limit,$offset);
		//print_r($data);exit;
		if(!$data['data']){
			$tr .="<div class='col-md-3 col-sm-4 col-xs-6'>";
			$tr .="<label>Not Found</label></div>";
			
			$msg = 'Data Not found';
		}else{
			foreach($data['data'] as $a){
				$tr .="<div class='col-md-3 col-sm-4 col-xs-6'>";
				$tr .="<label>".$a->title."</label>";
				$tr .="<a class='pull-right' href='".site_url()."upload/delete/".$a->id."' style='color:red;'><i class='fa fa-trash'></i> delete</a>";
				$tr .="<label class='pull-right'>".$a->date."|</label>";
				$tr .="<a title='click for details' href='".site_url()."upload/detailUpload/".$a->id."'>";
				$tr .="<img style='margin-bottom:20px;' class='img-responsive' src='".site_url()."assets/files/".$a->filename."'/>";
				$tr .="</a></div>";
			}
			$msg = 'Data loaded...';
		}
		//echo $tr;exit;
		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr));
	}
	
	public function doUploads(){
		
		$data = $this->input->post(null, true);
		//print_r($data);exit;
		
		if($data['title'] == ''){
			$res = array('status'=>false, 'msg'=>'silahkan isi form title!');
		}else{ 
			$this->_config_upload($data['title']);
			$cek = $this->_check_in($_FILES['userfile']['name'],$data['title']);
			if($cek){
				$res = array('status'=>false, 'msg'=>'nama file sudah ada, silahkan ubah nama file dan upload kembali');
			}else if(!$this->upload->do_upload()){
				//echo "gagal";exit;
				$res = array('status'=>false, 'msg'=>$this->upload->display_errors());
			}else{
				//print_r($_FILES['userfile']);exit;
				$det = $this->upload->data();
				$query = $this->Upload_model->saveUpload($data['title'], $det['file_name'], $det['file_path']);
				if($query){
					$res = array('status'=>true, 'msg'=>'data berhasil di upload..');
				}else{
					@unlink($det['file_path'].$det['file_name']);
					$res = array('status'=>false, 'msg'=>'Terjadi Kesalahan saat menyimpan file..');
				}
			}
		}
		$this->response($res);
		
	}
	
	private function _config_upload($name = null){
		
		$config = array(
				'file_name'		=> $name.','.$_FILES['userfile']['name'],
				'upload_path' 	=> './assets/files',
				'allowed_types'	=> 'jpeg|jpg|png',
				'max_size'		=> 1024,
				'encrypt_name'	=> false,
				'overwrite'		=> false,
				'remove_spaces'	=> true
		);
		
		$this->load->library('upload', $config);
	}
	
	private function _check_in($filename, $title){
		
		$name = str_replace(' ','_',$title.','.$filename);
		
		return $this->db->get_where('m_files',array('filename'=>$name))->row();
		
	}
	
}