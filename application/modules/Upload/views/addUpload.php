<div class="right_col" role="main">
	<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="dashboard_graph" id="chk-form">
				

				<!-- Form Name -->
				<legend><?php echo $header;?></legend>
				<h3 id="loading"></h3>
				<form class="form-horizontal" id="form-upload" method="post" action="#">
				<fieldset>
				<!-- Prepended text -->
				<div class="form-group">
				  <div class="col-md-5">
					<div class="input-group">
					  <label class="input-group">Title</label>
					  <input id="title" name="title" class="form-control" placeholder="title for file" type="text" autocomplete="off">
					</div>
					
				  </div>
				</div> 
				
				<!-- Prepended text-->
				<div class="form-group">
				  <div class="col-md-5">
					<div class="input-group">
					  <label class="input-group">Upload</label>
					  <input id="userfile" name="userfile" class="form-control" required="" type="file">
					</div>
					
				  </div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-4">
					<button id="do-upload" class="btn btn-success">upload</button>
					<button id="back-to-form" class="btn btn-danger">Cancel</button>
				  </div>
				</div>

				</fieldset>
				</form>
				<div class="clearfix"></div>
            </div>
		</div>
	</div>
			
			<script type="text/javascript">
			$(document).ready(function() {
				
				/* $('#do-upload').click(function(e){
					e.preventDefault();
					save_upload();
				}) */
				
				//ajax for upload, thanks to mike malsup for useful code :)
				$('#form-upload').ajaxForm({
					
					url : site_url + 'upload/doUploads',
					//delegation : true,
					type : 'post',
					data : $('#form-upload').serialize(),
					dataType : 'json',
					resetForm : true,
					beforeSend : function(){
						$('#loading').html('Uploading...');
					},
					success : function(result){
						if(result.status){
							alert(result.msg);
							window.location.reload();
						}else{
							alert(result.msg);
							window.location.reload();
						}
					},
					error : function(x,h,r){
						console.log(r);
					}
					
				});
				return false;
				
			});
			</script>