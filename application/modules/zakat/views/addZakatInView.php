<div class="right_col" role="main">
	<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph" id="add-form">
			
				<form class="form-horizontal" id="form-zakat-in" method="post">
				<fieldset>

				<!-- Form Name -->
				<legend><?php echo isset($header) ? $header : '';?></legend>

				<div class="form-group select-form col-md-8 col-xs-12">
					<label>Metode Pembayaran ZIS</label>
					<span id="metode-bayar"></span>
				</div>
				
				<!-- Prepended text-->
				<div class="form-group ">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>Jenis ZIS</label>
					  <div class="col-md-12">
					  	<label class="radio-inline" for="radios-0">
					      	<input type="radio" name="jenis" id="radios-0" value="3" checked="checked">
						      Zakat
						    </label> 
						    <label class="radio-inline" for="radios-1">
						      <input type="radio" name="jenis" id="radios-1" value="4">
						      Infaq
						    </label> 
						    <label class="radio-inline" for="radios-2">
						      <input type="radio" name="jenis" id="radios-2" value="5">
						      Shodaqoh
						    </label> 
					  </div>
					</div>
				  </div>
				</div>

				<span id="trf-info" style="display:none;">
					<div class="form-group ">
					  <div class="col-md-12 col-xs-12">
						<div class="add-new col-md-4 col-xs-12">
						  <label>No Rekening Pengirim</label>
						  <input id="no-rek" name="norek" class="form-control" placeholder="Ketik No Rekening Pengirim.." type="text">
						</div>
						<div class=" col-md-4 col-xs-12">
						  <label>Atas Nama</label>
						  <input id="an" name="an" class="form-control" placeholder="boleh dikosongkan.." type="text">
						</div>
					  </div>
					</div>
					<!-- Prepended text-->
					<div class="form-group">
					  <div class="col-md-12 col-xs-12">
					  	<div class="add-new col-md-4 col-xs-12">
						  <label>Nama Bank</label>
						  <input id="bank" name="bank" class="form-control" placeholder="Ketik nama bank pengirim.." type="text">
						</div>
						<div class="add-new col-md-4 col-xs-12">
						  <label class="input-group">Unggah Bukti Bayar</label>
						  <input id="userfile" name="userfile" class="form-control" type="file">
						</div>
						
					  </div>
					</div>
				</span>

				<div class="form-group ">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label id="chg-label">Jumlah yang dibayarkan</label>
					  <input id="Jumlah" name="jml_zakat" class="form-control" placeholder="Masukkan nominal yang dibayarkan.." required="" type="number">
					</div>
				  </div>
				</div>

				<!-- Prepended text-->
				<div class="form-group ">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>Nama Pengirim</label>
					  <input id="nama-donatur" name="nama_donatur" class="form-control" placeholder="Ketik Nama.." required="" type="text">
					</div>
				  </div>
				</div>

				<div class="form-group ">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>No Telepon</label>
					  <input id="telp" name="telp" class="form-control" placeholder="Ketik No Telp.." type="text">
					</div>
				  </div>
				</div>

				<!-- Prepended text-->
				<div class="form-group">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>Email</label>
					  <input id="email" name="email" class="form-control" placeholder="Alamat email jika ada.." type="email">
					</div>
					
				  </div>
				</div>

				<!-- <div class="form-group ">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>Jumlah Dana</label>
					  <input id="tgl-terima" name="tgl_penerimaan" class="form-control" placeholder="Pilih tanggal.." required="" type="text">
					</div>
				  </div>
				</div> -->
				
				<!-- Textarea -->
				<div class="form-group">
				  <div class="col-md-8 col-xs-12"> 
				  	<label>Alamat</label>                    
					<textarea class="form-control" required="" id="alamat" name="alamat"></textarea>
				  </div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-8">
					<button id="do-upload" name="submitted" class="btn btn-success" disabled="true">Save</button>
					<button id="cancelled" onClick="history.back();" name="cancel" class="btn btn-danger">Cancel</button>
				  </div>
				</div>

				</fieldset>
				</form>
				<div class="clearfix"></div>
            </div>
          </div>

        </div>