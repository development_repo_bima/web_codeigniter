<!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
		
			

        </div>
        <!-- /top tiles -->
		
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
			<legend id="header-tuts"><?php echo isset($header) ? $header : '';?></legend>
				<br>
				<div id="detail-view">
					<div class="col-md-12">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">
										Informasi Penerima Bantuan
									</h3>
								</div>
								<div class="panel-body">
									<table class="table table-user-information">
										<tbody>
											<tr>
												<td><p>Nama Pemberi Zakat</p></td>
												<td><p id="nama-penerima"></p></td>
											</tr>
											<tr>
												<td><p>No Telp</p></td>
												<td><p id="notelp"></p></td>
											</tr>
											<tr>
												<td><p>Email</p></td>
												<td><p id="emailuser"></p></td>
											</tr>
											<tr>
												<td><p>Alamat</p></td>
												<td><p id="alamatPenerima"></p></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">
										Informasi Transaksi Bantuan
									</h3>
								</div>
								<div class="panel-body">
									<table class="table table-user-information">
										<tbody>
											<tr>
												<td><p>Tipe</p></td>
												<td><p id="tipe"></p></td>
											</tr>
											<tr>
												<td><p>Jumlah Dana</p></td>
												<td><p id="jml-dana"></p></td>
											</tr>
											<tr>
												<td><p>Tanggal</p></td>
												<td><p id="tgl-transaksi"></p></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<button id="edit-button" class="btn btn-success">Edit</button>
					<a id="cancel-button" onClick="cancel()" class="btn btn-danger">Cancel</a>
				</div>
				<div id="edit-view" style="display:none;">
					<form class="form-horizontal" id="form-edit" method="post">
						<fieldset>
						</fieldset>
						<!-- Button -->
						<div class="form-group">
						  <div class="col-md-6">
							<div class="form-group select-form">
							  <label>Nama Penerima</label>
							  <input id="nama-penerima-edit" name="nama_penerima" class="form-control" placeholder="Ubah nama.." required="" type="text">
							</div>
							<div class="form-group select-form">
							  <label>No Telepon</label>
							  <input id="notelp-edit" name="telp" class="form-control" placeholder="Ubah no telepon.." required="" type="email">
							</div>
							<div class="form-group select-form">
							  <label>Email</label>
							  <input id="email-edit" name="email" class="form-control" placeholder="Ubah email.." required="" type="email">
							</div>
							<div class="form-group select-form">
							  <label>Alamat</label>
							  <textarea class="form-control" id="alamat-penerima-edit" name="alamat"></textarea>
							</div>

							<!-- <div class="form-group select-form">
							  <label>Password</label><small class="pull-right" style="color:red;">Ganti Password? <input type="checkbox" id="change-pass" onClick="disabledform()"></small>
							  <input disabled=true id="password-edit" name="password" class="form-control" placeholder="password here" required="" type="password">
							</div>
							<div class="form-group select-form">
								<label>Tipe User</label><small class="pull-right" style="color:red;">Ganti Tipe User? <input type="checkbox" id="change-role" onClick="disabledselect()"></small>
								<span id="fuchs"></span>
							</div> -->
							<button id="save-edit" class="btn btn-success">Save</button>
							<a id="cancel" onClick="cancel()" class="btn btn-danger">Cancel</a>
						  </div>
						</div>
					</form>
				</div>
				
              <div class="clearfix"></div>
            </div>
          </div>

        </div>
		<script type="text/javascript">var id = <?php echo isset($id) ? $id : 0;?>;</script>
		<script type="text/javascript">var role = <?php echo $role;?>;</script>