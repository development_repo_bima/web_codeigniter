<div class="right_col" role="main">
	<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph" id="add-form">
			
				<form class="form-horizontal" id="form-zakat-out" method="post">
				<fieldset>

				<!-- Form Name -->
				<legend><?php echo isset($header) ? $header : '';?></legend>

				<div class="form-group select-form col-md-8 col-xs-12">
					<label>Penerima Bantuan</label>
					<span id="bantuan-id"></span>
				</div>
				
				<!-- Prepended text-->
				<span id="penerima-span" style="display:none;">
					<div class="form-group ">
					  <div class="col-md-12 col-xs-12">
						<div class="input-group add-new col-md-8 col-xs-12">
						  <label id="label-nama"></label>
						  <input id="nama-donatur" name="nama_penerima" class="form-control" placeholder="Ketik Nama.." required="" type="text">
						</div>
					  </div>
					</div>
				</span>

				<div class="form-group ">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label id="chg-label">Jumlah yang diterima</label>
					  <input id="Jumlah" name="jml_keluar" class="form-control" placeholder="Masukkan nominal yang diterima.." required="" type="number">
					</div>
				  </div>
				</div>

				<div class="form-group ">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>No Telepon</label>
					  <input id="telp" name="telp" class="form-control" placeholder="Ketik No Telp.." type="text">
					</div>
				  </div>
				</div>

				<!-- Prepended text-->
				<div class="form-group">
				  <div class="col-md-12 col-xs-12">
					<div class="input-group add-new col-md-8 col-xs-12">
					  <label>Email</label>
					  <input id="email" name="email" class="form-control" placeholder="Alamat email jika ada.." type="email">
					</div>
					
				  </div>
				</div>
				
				<!-- Textarea -->
				<div class="form-group">
				  <div class="col-md-8 col-xs-12"> 
				  	<label>Alamat</label>                    
					<textarea class="form-control" required="" id="alamat" name="alamat"></textarea>
				  </div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-8">
					<button id="do-upload" name="submitted" class="btn btn-success" disabled="true">Save</button>
					<button id="cancelled" onClick="history.back();" name="cancel" class="btn btn-danger">Cancel</button>
				  </div>
				</div>

				</fieldset>
				</form>
				<div class="clearfix"></div>
            </div>
          </div>

        </div>