<!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
		
			

        </div>
        <!-- /top tiles -->

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
        			
        				<!-- <fieldset>
                  <div class="control-group">
                    <div class="controls">
                      <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="single_cal3" placeholder="First Name" aria-describedby="inputSuccess2Status3">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                      </div>
                    </div>
                  </div>
                </fieldset> -->
                <form method="post" class="form-horizontal" id="form-laporan">
                <fieldset>
                  <legend><?php echo isset($header) ? $header : '';?></legend>
                  <!-- Prepended text-->
                  <div class="form-group" id="default-date">
                    <div class="col-md-12 col-xs-12">
                      <div class="input-group add-new col-md-8 col-xs-12">
                        <label>Pilihan Laporan Keuangan</label>
                        <div class="col-md-12">
                          <label class="radio-inline" for="radios-0">
                              <input type="radio" name="defaultlaporan" id="radios-0" value="1" checked="checked">
                              1 Bulan Terakhir
                            </label> 
                            <label class="radio-inline" for="radios-1">
                              <input type="radio" name="defaultlaporan" id="radios-1" value="3">
                              3 Bulan Terakhir
                            </label> 
                            <label class="radio-inline" for="radios-2">
                              <input type="radio" name="defaultlaporan" id="radios-2" value="5">
                              5 Bulan Terakhir
                            </label> 
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group ">
                    <div class="col-md-12 col-xs-12">
                      <div class="input-group add-new col-md-8 col-xs-12">
                        <div class="col-md-12">
                          <label class="checkbox" for="checkboxes-0">
                            <input type="checkbox" name="withdate" id="with-date" value="1">
                            Tampilkan Pilihan Tanggal
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <span id="show-date" style="display:none;">
                    <div class="form-group">
                      <label>Pilih Periode yang diinginkan</label>
                      <div class="input-prepend input-group">
                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" style="width: 200px" name="range" id="range-date" class="form-control" value="<?php echo date('m/d/Y').' - '. date('m/d/Y'); ?>" />
                      </div>
                    </div>
                  </span>
                  <!-- Button -->
                  <div class="form-group">
                    <div class="col-md-8">
                    <button id="view" name="submitted" class="btn btn-success" onclick="post_data();return false;">Lihat</button>
                    </div>
                  </div>
                </form>
                </fieldset>
                <div class="table-responsive">
                    <h2 align="center">Rekapitulasi Laporan Keuangan Masjid.. Bulan.. Tahun ...</h2>
                    <table class="table table-bordered table-hover table-condensed table-responsive" id="data-table">
                      <thead>
                        <tr>
                          <th>Laporan</th>
                          <th>#</th>
                          <th>Nama</th>
                          <th>Tanggal</th>
                          <th>Jumlah</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>

        </div>

		