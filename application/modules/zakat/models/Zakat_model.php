<?php 

class Zakat_model extends CI_model{
	
	public function get_data($search=null, $limit=0, $offset=0){
		//print_r($offset);exit;
		$this->db->select('*')
					->from('t_in_zakat')
					->join('m_pilihan','m_pilihan.id_metode = t_in_zakat.jenis_dana','left');
			if($search)
				$this->db->like('nama_donatur', $search);
		
		$this->db->order_by('id', 'DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result();
		$data['total'] = $this->db->count_all('t_in_zakat');
		
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}
	
	public function get_detail($id){
		
		//print_r($this->db->last_query());exit;
		//active record ways still get errors. how it's work??
		return $this->db->query("
			SELECT a.*, b.nama_pilihan AS metode, c.nama_pilihan AS jenis
				FROM t_in_zakat a 
				LEFT JOIN m_pilihan b ON a.metode_bayar = b.id_metode 
				LEFT JOIN m_pilihan c ON a.jenis_dana = c.id_metode
				WHERE id = {$id};")->row_array();
	}

	public function save_data($data){
	
		$save = [
			'no_transaksi'	=> mt_rand(),
			'metode_bayar'	=>isset($data['bukti_bayar'])?1:2,
			'jenis_dana'	=>$data['jenis'],
			'no_rek'		=>isset($data['norek'])?$data['norek']:'',
			'atas_nama'		=>isset($data['an'])?$data['an']:'',
			'nama_bank'		=>isset($data['bank'])?$data['bank']:'',
			'bukti_bayar'	=>isset($data['bukti_bayar'])?$data['bukti_bayar']:'',
			'nama_donatur'	=>$data['nama_donatur'],
			'no_telp'		=>isset($data['telp'])?$data['telp']:'-',
			'email'   		=>isset($data['email'])?$data['email']:'-',
			'alamat' 		=>$data['alamat'],
			'jumlah_dana'  	=>$data['jml_zakat'],
			//'tgl_penerimaan'=>$data['tgl_penerimaan'],
			'created_at'	=>date('Y-m-d h:i:s'),
			'updated_at'	=>date('Y-m-d h:i:s'),
		];
		
		$this->db->insert('t_in_zakat', $save);
		return true;
		
	}
	
	public function update_data($data){
		//print_r($data);exit;
		/*$salt = md5(trim($data['username']));
			
		$update = [
				'username'	=>$data['username'],
				'email'		=>$data['email']
		];
		
		if(isset($data['password']) && $data['password'] != ''){
			$update['salt'] = $salt;
			$update['hash'] = crypt(trim($data['password']), '$2a$12$'.$salt.'$');
		}
		
		if(isset($data['tipe_user'])){
			$update['jabatan_id'] = $data['tipe_user'];
		}*/

		$update = [
			'nama_donatur'	=>$data['nama_donatur'],
			'no_telp'		=>isset($data['telp'])?$data['telp']:'-',
			'email'   		=>isset($data['email'])?$data['email']:'-',
			'alamat' 		=>$data['alamat'],
			'updated_at'	=>date('Y-m-d h:i:s'),
		];
		
		return $this->db->update('t_in_zakat',$update,array('id'=>$data['id']));
	}
	
	public function approval_data($data){
		
		return $this->db->update('t_in_zakat',array('approval'=>$data['status']), array('id'=>$data['id']));
	}
	
	public function delete_data($id){
		
		return $this->db->delete('t_in_zakat', array('id'=>$id));
	}
	
	private function _verified($password=null, $hash=null, $salt=null){
		
		$cek = crypt($password, '$2a$12$'.$salt.'$');
		if($cek === $hash){
			return true;
		}else{
			return false;
		}
	}


}