<?php 

class Zakat_out_model extends CI_model{
	
	public function get_data($search=null, $limit=0, $offset=0){
		//print_r($offset);exit;
		$this->db->select('*')
					->from('t_out_zakat')
					->join('m_pilihan','m_pilihan.id_metode = t_out_zakat.jenis_penerima','left');
					//->join('m_akses as a','a.jabatan_id = j.jabatan_id','left');
			if($search)
				$this->db->like('nama_penerima', $search);
				$this->db->or_like('nama_pilihan', $search);
		
		$this->db->order_by('id', 'DESC');
		
		$data['data'] = $this->db->limit($limit, $offset)->get()->result();
		$data['total'] = $this->db->count_all('t_out_zakat');
		
		//print_r($this->db->last_query());
		//print_r($data);exit;
		return $data;
	}
	
	public function get_detail($id){
		
		//print_r($this->db->last_query());exit;
		//active record ways still get errors. how it's work??
		return $this->db->query("
			SELECT a.*, b.nama_pilihan
				FROM t_out_zakat a
				LEFT JOIN m_pilihan b ON b.id_metode = a.jenis_penerima 
				WHERE id = {$id};")->row_array();
	}

	public function save_data($data){
	
		$save = [
			'no_transaksi'		=> mt_rand(),
			'jenis_penerima'	=>$data['jenis_penerima'],
			'nama_penerima'		=>$data['nama_penerima'],
			'jumlah_diterima'	=>$data['jml_keluar'],
			'no_telp'			=>isset($data['telp'])?$data['telp']:'-',
			'email'   			=>isset($data['email'])?$data['email']:'-',
			'alamat_penerima'	=>$data['alamat'],
			//'tgl_penerimaan'=>$data['tgl_penerimaan'],
			'created_at'		=>date('Y-m-d h:i:s'),
			'updated_at'		=>date('Y-m-d h:i:s'),
		];
		
		$this->db->insert('t_out_zakat', $save);
		return true;
		
	}
	
	public function update_data($data){
		
		$update = [
			'nama_penerima'		=>$data['nama_penerima'],
			'no_telp'			=>isset($data['telp'])?$data['telp']:'-',
			'email'   			=>isset($data['email'])?$data['email']:'-',
			'alamat_penerima'	=>$data['alamat'],
			'updated_at'		=>date('Y-m-d h:i:s'),
		];
		
		return $this->db->update('t_out_zakat',$update,array('id'=>$data['id']));
	}
	
	public function approval_data($data){
		
		return $this->db->update('t_out_zakat',array('approval'=>$data['status']), array('id'=>$data['id']));
	}
	
	public function delete_data($id){
		
		return $this->db->delete('t_out_zakat', array('id'=>$id));
	}
	
}