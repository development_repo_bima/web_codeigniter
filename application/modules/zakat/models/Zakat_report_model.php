<?php 

class Zakat_report_model extends CI_model{
	
	

	public function get_laporan(){
		$in = $this->db->select('nama_donatur, created_at, jumlah_dana')->from('t_in_zakat')->where('approval', '1')->get()->result_array();

		$out = $this->db->select('nama_penerima, created_at, jumlah_diterima')->from('t_out_zakat')->where('approval', '1')->get()->result_array();

		return ['in'=>$in,'out'=>$out];
	}
	
}