<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends My_Back {

	protected static $modul = "ZAKAT_REPORT";

	protected static $access = [];
	
	public function __construct(){
		
		parent::__construct();
		SELF::$access = $this->general->check_role($this->session->userdata('jabatan_id'), SELF::$modul, $this->access_arr);
		if(!SELF::$access['view'])
			redirect('testing/unAuthorized');

		$this->load->model('Zakat_report_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		// check if role add have a value
		
		$data = [
			'title' 	=> 'Laporan Keuangan Masjid',
			'header'	=> 'Laporan Keuangan Masjid',
			'add_role'	=> (SELF::$access['add']) ? "" : "display:none;",
			'custom_js'	=> array(
									'datepicker_atr'=> 'assets/js/moment/moment.min.js',
									'datepicker' 	=> 'assets/js/datepicker/daterangepicker.js',
									'service' 		=> 'assets/service-js/zakat/zakat_report.js'
									
								)
		];
		
		$this->_nampil('zakatReportView', $data);

		//$this->load->view('datepick');
	}
	
	public function viewData(){
		print_r($this->input->post(null, true));die;
		$tr = '';
		$total_in = $total_out = 0;	
		$i = 1;
		$j = 1;
		$data = $this->Zakat_report_model->get_laporan();
		if($data['in']){
			foreach ($data['in'] as $key => $value) {
				$tr .= "<tr>";
				$tr .= "<td>".(($i==1)?'Penerimaan':'')."</td>";
				$tr .= "<td>".$i."</td>";
				$tr .= "<td>".$value['nama_donatur']."</td>";
				$tr .= "<td>".$value['created_at']."</td>";
				$tr .= "<td>".'Rp. '.$this->general->num_format($value['jumlah_dana'])."</td>";
				$tr .= "</tr>";
				$i++; 
				$total_in += $value['jumlah_dana'];
			}
		}else{
			$tr .= "<tr>";
			$tr .= "<td colspan='5'>Tidak Ada Data Penerimaan</td>";
			$tr .= "</tr>";			
		}

		$tr .= "<tr>";
		$tr .= "<td colspan='4'><b>Total Penerimaan</b></td>";
		$tr .= "<td><b>".'Rp. '.$this->general->num_format($total_in)."</b></td>";
		$tr .= "</tr>";

		if($data['out']){
			foreach ($data['out'] as $key => $value) {
				$tr .= "<tr>";
				$tr .= "<td>".(($j==1)?'Pengeluaran':'')."</td>";
				$tr .= "<td>".$j."</td>";
				$tr .= "<td>".$value['nama_penerima']."</td>";
				$tr .= "<td>".$value['created_at']."</td>";
				$tr .= "<td>".'Rp. '.$this->general->num_format($value['jumlah_diterima'])."</td>";
				$tr .= "</tr>";
				$j++; $total_out += $value['jumlah_diterima'];
			}
		}else{
			$tr .= "<tr>";
			$tr .= "<td colspan='5'>Tidak Ada Data Pengeluaran</td>";
			$tr .= "</tr>";	
		}

		$tr .= "<tr>";
		$tr .= "<td colspan='4'><b>Total Pengeluaran</b></td>";
		$tr .= "<td><b>".'Rp. '.$this->general->num_format($total_out)."</b></td>";
		$tr .= "</tr>";
		$tr .= "<tr>";
		$tr .= "<td colspan='4'><b>Sisa Saldo Bulan ...</b></td>";
		$tr .= "<td><b>".'Rp. '.$this->general->num_format(($total_in - $total_out))."</b></td>";
		$tr .= "</tr>";
		$msg = 'Data Loaded...';
		/*print_r($total_in);die;*/
		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr));
	}
	public function getData(){
		
		$limit  = $this->config->item('per_page');
		$offset = $this->uri->segment(3,0);
		$search = $this->input->get('search');
		$url 	= 'zakat/pengeluaran/getData/';
		
		$data = $this->Zakat_out_model->get_data($search, $limit, $offset);
		//print_r($data);exit;
		$total = $data['total'];
		$tr = $paging = '';
		if(!$data['data']){
			if($search){
				$tr .="<tr>";
				$tr .="<td colspan='7'>Not Found...</td>";
				$tr .="</tr>";

				$msg = 'Data Not found';
			}else{
				$tr .="<tr>";
				$tr .="<td colspan='7'>Empty Data...</td>";
				$tr .="</tr>";

				$msg = 'Empty Data';
			}
		};
		
		if($data['data']){
			$no = $offset + 1;
			
			$i = 1;
			foreach($data['data'] as $o){
				
				//styling button section, hope better way's. but now is fine
				$status = ($o->approval=='0')?'1':'0';
				$active = ($o->approval=='1')? "<i class='fa fa-ban'></i> Deactivate" : "<i class='fa fa-check'></i> Activate" ;

				$edit_roles	 = (SELF::$access['edit']) ? "" : "display:none;";
				$edit_role 	 = (SELF::$access['edit'] && $this->session->userdata('jabatan_id') == 3) ? "" : "display:none;";
				$remove_role = (SELF::$access['remove']) ? "" : "display:none;";
				
				$tr .="<tr>";
				$tr .="<td>".$no."</td>";

				//button action
				if(!SELF::$access['edit'] && !SELF::$access['remove']){
					$tr .="<td>No Privileges</td>";
				}else{
					$tr .="<td><div class='btn-group'>";
					$tr .="<button class='btn btn-xs btn-default'>...</button>";
					$tr .="<button data-toggle='dropdown' class='btn btn-xs btn-default dropdown-toggle'><span class='caret'></span></button>";
					$tr .="<ul class='dropdown-menu'>";
					$tr .="<li style='".$edit_roles."'><a href='".site_url().'zakat/pengeluaran/detailView/'.$o->id."'><i class='fa fa-edit'></i> Detail</a></li>";
					$tr .="<li style='".$edit_role."'><a href='javascript:void(0)' onClick='approval(".$o->id.','.$status.")'>".$active."</a></li>";
					$tr .="<li style='".$remove_role."'><a href='javascript:void(0)' onClick='delete_record(".$o->id.")'><i class='fa fa-trash'></i> Delete</a></li>";
					$tr .="</ul></div></td>";
				}
				//end button action
				
				$tr .="<td>".$o->nama_pilihan."</td>";
				$tr .="<td>".$o->nama_penerima."</td>";
				$tr .="<td>".$o->no_telp."</td>";
				$tr .="<td>".'Rp. '.$this->general->num_format($o->jumlah_diterima)."</td>";
				$tr .="<td>".$o->alamat_penerima."</td>";
				$tr .="<td>".(($o->approval)?'Disetujui':'Menunggu Konfirmasi')."</td>";
				$tr .="</tr>";
			$i++;$no++;
			}
			$paging .="<li><span class='page-info'>Displaying ".($i-1)." of ".$total." data</span></li>";
			$paging .= $this->_paging($url,$total,$limit);
			
			$msg = 'Data Loaded...';
		}

		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr,'paging'=>$paging));
		
	}
	


}
