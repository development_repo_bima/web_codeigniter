<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan extends My_Back {

	protected static $modul = "ZAKAT_IN";

	protected static $access = [];
	
	public function __construct(){
		
		parent::__construct();
		SELF::$access = $this->general->check_role($this->session->userdata('jabatan_id'), SELF::$modul, $this->access_arr);
		if(!SELF::$access['view'])
			redirect('testing/unAuthorized');

		$this->load->model('Zakat_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		// check if role add have a value
		
		$data = [
			'title' 	=> 'Daftar Penerimaan Masjid',
			'header'	=> 'Daftar Penerimaan Masjid',
			'add_role'	=> (SELF::$access['add']) ? "" : "display:none;",
			'custom_js'	=> array('service' => 'assets/service-js/zakat/list-zakat-in.js')
		];
		
		$this->_nampil('zakatInView', $data);
	}
	
	public function getData(){
		
		$limit  = $this->config->item('per_page');
		$offset = $this->uri->segment(3,0);
		$search = $this->input->get('search');
		$url 	= 'zakat/Penerimaan/getData/';
		
		$data = $this->Zakat_model->get_data($search, $limit, $offset);
		//print_r($data);exit;
		$total = $data['total'];
		$tr = $paging = '';
		if(!$data['data']){
			if($search){
				$tr .="<tr>";
				$tr .="<td colspan='7'>Not Found...</td>";
				$tr .="</tr>";

				$msg = 'Data Not found';
			}else{
				$tr .="<tr>";
				$tr .="<td colspan='7'>Empty Data...</td>";
				$tr .="</tr>";

				$msg = 'Empty Data';
			}
		};
		
		if($data['data']){
			$no = $offset + 1;
			
			$i = 1;
			foreach($data['data'] as $o){
				
				//styling button section, hope better way's. but now is fine
				$status = ($o->approval=='0')?'1':'0';
				$active = ($o->approval=='1')? "<i class='fa fa-ban'></i> Deactivate" : "<i class='fa fa-check'></i> Activate" ;

				$edit_roles	 = (SELF::$access['edit']) ? "" : "display:none;";
				$edit_role 	 = (SELF::$access['edit'] && $this->session->userdata('jabatan_id') == 3) ? "" : "display:none;";
				$remove_role = (SELF::$access['remove']) ? "" : "display:none;";
				
				$tr .="<tr>";
				$tr .="<td>".$no."</td>";

				//button action
				if(!SELF::$access['edit'] && !SELF::$access['remove']){
					$tr .="<td>No Privileges</td>";
				}else{
					$tr .="<td><div class='btn-group'>";
					$tr .="<button class='btn btn-xs btn-default'>...</button>";
					$tr .="<button data-toggle='dropdown' class='btn btn-xs btn-default dropdown-toggle'><span class='caret'></span></button>";
					$tr .="<ul class='dropdown-menu'>";
					$tr .="<li style='".$edit_roles."'><a href='".site_url().'zakat/Penerimaan/detailView/'.$o->id."'><i class='fa fa-edit'></i> Detail</a></li>";
					$tr .="<li style='".$edit_roles."'><a href='javascript:void(0)' onClick='approval(".$o->id.','.$status.")'>".$active."</a></li>";
					$tr .="<li style='".$remove_role."'><a href='javascript:void(0)' onClick='delete_record(".$o->id.")'><i class='fa fa-trash'></i> Delete</a></li>";
					$tr .="</ul></div></td>";
				}
				//end button action

				$tr .="<td>".$o->nama_pilihan."</td>";
				$tr .="<td>".$o->nama_donatur."</td>";
				$tr .="<td>".$o->no_telp."</td>";
				$tr .="<td>".'Rp. '.$this->general->num_format($o->jumlah_dana)."</td>";
				$tr .="<td>".$o->alamat."</td>";
				$tr .="<td>".(($o->approval)?'Disetujui':'Menunggu Konfirmasi')."</td>";
				$tr .="</tr>";
			$i++;$no++;
			}
			$paging .="<li><span class='page-info'>Displaying ".($i-1)." of ".$total." data</span></li>";
			$paging .= $this->_paging($url,$total,$limit);
			
			$msg = 'Data Loaded...';
		}

		$this->response(array('status'=>true,'msg'=>$msg,'data'=>$tr,'paging'=>$paging));
		
	}
	
	public function addView(){
		if(!SELF::$access['add']) redirect('testing/unAuthorized');

		$data = [
			'title' 	=> 'Tambah data penerimaan masjid',
			'header'	=> 'Tambah data penerimaan masjid',
			'custom_js'	=> [
							'form'=>'assets/js/jquery.form.js',
							'service' => 'assets/service-js/zakat/add-zakat-in.js'
							]

		];
		
		$this->_nampil('addZakatInView', $data);
	}
	
	public function detailView(){
		if(!SELF::$access['edit']) redirect('testing/unAuthorized');

		$data = [
			'id'	=> $this->uri->segment(4),
			'title' => 'Detail Pemberi ZIS',
			'header'=> 'Detail Pemberi ZIS',
			'role'	=> $this->session->userdata('jabatan_id'),
			'custom_js'	=> array('service' => 'assets/service-js/zakat/detail-zakat-in.js')
		];
		
		$this->_nampil('detailZakatInView', $data);
	}
	
	public function getDetail(){
		
		$get = $this->Zakat_model->get_detail($this->input->get('id'));
		
		if($get){
			$res = ['status'=>true, 'msg'=>'Data Loaded...', 'data'=>$get];
		}else{
			$res = ['status'=>false,'msg'=>'Data Not Found...','data'=>[]];
		}
		$this->response($res);
	}
	
	public function addData(){
		
		$data = $this->input->post(null, true);
		

		if(isset($_FILES['userfile']['name'])){

			$this->_config_upload($data);

			if(!$this->upload->do_upload()){
				//echo "gagal";exit;
				$res = array('status'=>false, 'msg'=>$this->upload->display_errors());
			}else{
				
				$det = $this->upload->data();
				$data['bukti_bayar'] = $det['file_name'];

				$query = $this->Zakat_model->save_data($data);
				if($query){
					$res = array('status'=>true, 'msg'=>'Berhasil menambahkan data');
				}else{
					@unlink($det['file_path'].$det['file_name']);
					$res = array('status'=>false, 'msg'=>'Terjadi Kesalahan saat menyimpan file..');
				}
			}
		}else{
			$query = $this->Zakat_model->save_data($data);
			if($query){
				$res = array('status'=>true, 'msg'=>'Berhasil menambahkan data');
			}else{
				$res = array('status'=>false, 'msg'=>'Terjadi Kesalahan, \n silahkan coba lagi');
			}
		}
		$this->response($res);
	}
	
	public function updateData(){
		//print_r($this->input->input_stream(null,true));exit;
		$update = $this->Zakat_model->update_data($this->input->input_stream(null,true));
		if($update){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Ubah'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Ubah,\n coba beberapa saat lagi'];
		}
		$this->response($res);
	}
	
	public function approval(){
		
		$data = $this->input->input_stream(null,true);
		
		if($this->Zakat_model->approval_data($data)){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Update..'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Update..'];
		}
		$this->response($res);
	}
	
	public function delete(){
		
		$id = $this->input->input_stream('id');
		//die($id);
		if($this->Zakat_model->delete_data($id)){
			$res = ['status'=>true,'msg'=>'Data Berhasil Di Hapus..'];
		}else{
			$res = ['status'=>false,'msg'=>'Data Gagal Di Hapus..'];
		}
		$this->response($res);
	}
	
	public function getPilihan(){
		
		$query = $this->db->get_where('m_pilihan', ['jenis_pilihan'=>1])->result_array();
		$sel = '';
		$sel .="<select class='form-control' required id='select-form' onChange='jenis_pilihan()'><option value=''>--Pilih metode pembayaran--</option>";
		foreach($query as $a){
			$sel .="<option value='".$a['id_metode']."'>".$a['nama_pilihan']."</option>";
		}
		$sel .="</select>";
		//print_r($sel);exit;
		$this->response(array('data'=>$sel));
	}

	private function _config_upload($data = []){
		
		$config = array(
				//'file_name'		=> $name.','.$_FILES['userfile']['name'],
				'file_name'		=> md5($data['nama_donatur'].$data['telp']),
				'upload_path' 	=> './assets/files',
				'allowed_types'	=> 'jpeg|jpg|png',
				'max_size'		=> 1024,
				'encrypt_name'	=> false,
				'overwrite'		=> true,
				'remove_spaces'	=> true
		);
		
		$this->load->library('upload', $config);
	}

}
