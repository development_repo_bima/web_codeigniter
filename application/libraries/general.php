<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class general{
	
	private $obj = null;
	// function general(){
	// 	$this->obj =& get_instance();
	// }

	public function __construct(){
		$this->obj =& get_instance();
	}
	
	public function check_access($page, $access = null){
		
		$jabatan_id = $this->obj->session->userdata('jabatan_id');
		
		$sql = "SELECT * FROM m_user a, m_akses b 
					WHERE a.jabatan_id = b.jabatan_id 
					AND b.modul='{$page}' 
					AND b.{$access} = 1 
					AND a.jabatan_id={$jabatan_id}";
		
		$q = $this->obj->db->query($sql);
		
		if($q->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function check_role($jabatan_id, $modul, $action){

		if(is_array($action)){
			$res = $this->obj->db->query("SELECT `view`, `add`, `edit`, `remove` FROM m_akses WHERE jabatan_id = {$jabatan_id} AND modul = '{$modul}'")->row_array();
				
			
		}else{
			$res = $this->obj->db->query("SELECT `{$action}` FROM m_akses WHERE jabatan_id = {$jabatan_id} AND modul = '{$modul}' AND `{$action}`= 1")->row_array();
		}
		if($res){
			return $res;
		}else{
			return false;
		}
	}

	public function notFound(){
		$this->obj->load->view('error_layouts/404');
	}
	
	public function response($res=array()){
		
		return json_encode($res);
	}

	public function num_format($num){
        #tes sublim sftp
		/* number format has 4 params : 
		*	1	string number, 
		*	2	decimal you want, 
		*	3	separator for comma's, 
		*	4	separator for thousand number
		*/
        // note: change as you wish :D
        # bug fix for float type
		if(intval($num) == $num){
            $nums = number_format($num,0,',','.');
        }else{
            $nums = number_format($num,4,',','.');
            $nums = rtrim($nums,0);
        }
        //$nums = number_format((int)$num , 4, '.', ','); 
        return $nums;
	}
}