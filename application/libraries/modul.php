<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Modul{
	
	private $obj = null;
	// function modul(){
	// 	$this->obj =& get_instance();
	// }

	public function __construct(){
		$this->obj =& get_instance();
	}
	/* used in admin page*/
	public function generateModul($parent=0,$hasil=''){
		
		$q = $this->obj->db->query("SELECT * FROM m_menu WHERE parent = '".$parent."' and flag = 0");
		
		foreach($q->result() as $i){
			
			if($this->obj->general->check_access($i->const,'view')){
				$hasil .="<li>";
				$hasil .="<a><i class='fa ".$i->icon."'></i> ".$i->menu." <span class='fa fa-chevron-down'></span></a>";
				$hasil .="<ul class='nav child_menu' style='display: none'>";
				
				$hasil .= $this->child($i->id);
				$hasil .="</ul>";
				$hasil .="</li>";
			}
		}
		//print_r($hasil);exit;
		return $hasil;
		
	}
	/* used in admin page*/
	private function child($parent=null){
		
		$q = $this->obj->db->query("SELECT * FROM m_menu WHERE parent = '".$parent."'");
		$hasil ='';
		foreach($q->result() as $a){
			if($this->obj->general->check_access($a->const,'view')){
				$hasil .="<li><a href='".site_url().$a->url."' title='".$a->menu."'>".$a->menu."</a></li>";
			}
		}
		return $hasil;
	}

	/*
	 * function : get Top 5 articles put at slider
	 * return : string
	*/
	public function top_articles(){

		$q = $this->obj->db->query("select * from t_artikel a LEFT JOIN m_category b ON b.category_id = a.category where status = 1 order by counter desc limit 5;");
		$tops = '';

		foreach($q->result() as $v){

			$tops .= '<div class="ms-slide blog-slider">';
			$tops .= '<img src="<?php echo base_url(); ?>public/assets/plugins/master-slider/masterslider/style/blank.gif" data-src="'.base_url().'assets/files/'.$v->image.'" alt="'.$v->title.'"/>';
			$tops .= '<span class="blog-slider-badge">'.$v->name.'</span>';
			$tops .= '<div class="ms-info"></div>';
			$tops .= '<div class="blog-slider-title">';
			$tops .= '<span class="blog-slider-posted">'.date("F jS, Y", strtotime($v->created_at)).'</span>';
			$tops .= '<h2><a href="'.site_url().'homepage/post/'.$v->uid.'/'.urlencode($v->title).'">'.$v->title.'</a></h2></div></div>';
        }

        return $tops;		
	}

	public function index_articles(){

		$q = $this->obj->db->query("select * from t_artikel a LEFT JOIN m_category b ON b.category_id = a.category where status = 1 order by counter desc limit 6;");

		$tops = '';
		foreach($q->result() as $v){
		
			$tops .= '<div class="col-sm-4">';
			$tops .= '<div class="thumbnails-v1">';
			$tops .= '<div class="thumbnail-img">';
			$tops .= '<img class="img-responsive" width="973" height="615" src="'.base_url().'assets/files/'.$v->image.'" alt="'.$v->title.'" alt=""></div>';
			$tops .= '<div class="caption">';
			$tops .= '<h3><a href="'.site_url().'homepage/post/'.$v->uid.'/'.urlencode($v->title).'">'.$v->title.'</a></h3>';
			$tops .= '<p>'.$this->sub($v->content).'</p>';
			$tops .= '<p><a class="read-more" href="'.site_url().'homepage/post/'.$v->uid.'/'.urlencode($v->title).'">Selengkapnya</a></p>';
			$tops .= '</div></div></div>';
		}

		return $tops;
	}

	/* used in homepage*/
	public function get_home_menu($parent = 0, $hasil = ''){

		$q = $this->obj->db->query("SELECT * FROM m_menu WHERE parent = '".$parent."' and flag = 1");
		
		foreach($q->result() as $i){
			
			if($i->induk){
				$hasil .="<li class='dropdown active'>";
				$hasil .='<a href="'.(($i->url)?site_url($i->url):'javascript:void(0);').'" class="'.(($i->url)?'':'dropdown-toggle').'" data-toggle="'.(($i->url)?'':'dropdown').'">'.$i->menu.'</a>';
			}else{
				$hasil .='<li><a href="'.site_url($i->url).'">'.$i->menu.'</a></li>';
			}

			if($i->induk){
				if($child = $this->get_home_menu($i->id)){
					$hasil .='<ul class="dropdown-menu">';
					$hasil .= $child;
					$hasil .="</ul>";
				}
			}
			$hasil .="</li>";
		}
		//print_r($hasil);exit;
		return $hasil;
	}

	public function breadcrumbs(){

		$link = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';

		if($link){
			$subLink = explode('/', substr($link,1));

			if(isset($subLink[1]) && $subLink[1] != 'post'){
				$link = $this->obj->db->query("SELECT menu FROM m_menu WHERE url = '".substr($link,1)."';")->row_array();
			}
			
			$res ='';
			if(isset($link['menu']) && $link['menu'] != 'Beranda'){
				$res = '<div class="breadcrumbs breadcrumbs-light">
							<div class="container">
								<h1 class="pull-left">'.(isset($link['menu'])?$link['menu']:$subLink[1]).'</h1>
								<ul class="pull-right breadcrumb">
									<li><a href="'.site_url().'">Beranda</a></li>
									<li class="active">'.(isset($link['menu'])?$link['menu']:$subLink[1]).'</li>
								</ul>
							</div>
						</div>';
			}
			return $res;
		}
	}

	public function getPilihan($pilihan=0){
		
		$query = $this->obj->db->get_where('m_pilihan', ['jenis_pilihan'=>$pilihan])->result_array();
		$sel = '';
		$sel .="<select class='form-control' name='metode' required id='select-form' onChange='jenis_pilihan()'><option value=''>--Pilih--</option>";
		foreach($query as $a){
			$sel .="<option value='".$a['id_metode']."'>".$a['nama_pilihan']."</option>";
		}
		$sel .="</select>";

		return $sel;
	}

	public function sub($str){
		$str = strip_tags($str);
		$new_str = preg_replace('/\s+?(\S+)?$/', '', substr($str, 0, 201));
		return $new_str;
	}
}